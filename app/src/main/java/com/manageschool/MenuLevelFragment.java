package com.manageschool;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class MenuLevelFragment extends Fragment {


    RequestQueue queue;
    StringRequest request;
    RecyclerView recyclerView;
    String GETURL = "http://192.168.42.210/mysite/Rschool/getLevel.php";
    AppCompatImageButton refresh;
    FloatingActionButton add;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_level, container, false);
        queue = Volley.newRequestQueue(getContext());

        add = (FloatingActionButton) view.findViewById(R.id.floatingbtn);
        refresh = (AppCompatImageButton) view.findViewById(R.id.refresh);
        recyclerView = (RecyclerView) view.findViewById(R.id.levels);
        queue = Volley.newRequestQueue(getContext());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputLevelName dialog = new InputLevelName();
                dialog.show(getActivity().getSupportFragmentManager(),"level");
            }
        });


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
            }
        });
        Load();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Load();
    }

    private void Load(){

        request = new StringRequest(Request.Method.POST, GETURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Level level ;
                    ArrayList<Level> levels = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        level = new Level();
                        JSONObject object = jsonArray.getJSONObject(i);
                        level.setID(object.getString("ID"));
                        level.setName(object.getString("Name"));
                        level.setClassRoomName(object.getString("ClassRoom"));

                        levels.add(level);
                    }
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                    LevelAdapter adapter = new LevelAdapter(levels , getContext(), true);
                    recyclerView.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);

        queue.add(request);

    }

}
