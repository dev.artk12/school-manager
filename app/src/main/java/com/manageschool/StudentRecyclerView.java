package com.manageschool;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class StudentRecyclerView extends RecyclerView.Adapter<StudentRecyclerView.ViewHolder> {

    Context context;
    ArrayList<Student> students;
    ArrayList<String> color;
    int blue , red , green , yellow;


    StudentRecyclerView(Context context , ArrayList<Student> students , ArrayList<String> Color){
    this.context = context;

    if (students == null){
        students = new ArrayList<>();
    }
    this.students = students;
    this.color = Color;

        blue = ContextCompat.getColor(context , R.color.bluecheckbox);
        red = ContextCompat.getColor(context , R.color.redcheckbox);
        green = ContextCompat.getColor(context , R.color.greencheckbox);
        yellow = ContextCompat.getColor(context , R.color.yellowcheckbox);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.itemviewstudent,viewGroup,false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        if (students.size() != 0){

            viewHolder.fathername.setText(students.get(i).getFatherName());
            viewHolder.fullname.setText(students.get(i).getName() + " " + students.get(i).getLastName());
            viewHolder.nationalid.setText(students.get(i).getNationalID());
            viewHolder.parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, StudentDetaildActivity.class);
                    intent.putExtra("ID",students.get(i).getID());
                    context.startActivity(intent);
                }
            });
            int accepted = Integer.valueOf(students.get(i).getAccepted());
            if (accepted == 0){
                viewHolder.view.setBackgroundColor(red);
            }
        }
        viewHolder.Year.setText(students.get(i).getBirthYear());

        switch (Integer.valueOf(color.get(i))){
            case 0 : viewHolder.Year.setTextColor(blue); break;
            case 1 : viewHolder.Year.setTextColor(green); break;
            case 2 : viewHolder.Year.setTextColor(red); break;
            case 3 : viewHolder.Year.setTextColor(yellow); break;
        }

    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayoutCompat parent;
        AppCompatTextView fullname , fathername , nationalid , Year;
        View view;


        public ViewHolder(View itemView){
            super(itemView);
            parent = (LinearLayoutCompat) itemView.findViewById(R.id.parent);
            fullname = (AppCompatTextView) itemView.findViewById(R.id.fullname);
            fathername = (AppCompatTextView) itemView.findViewById(R.id.fathername);
            nationalid = (AppCompatTextView) itemView.findViewById(R.id.nationalid);
            Year = (AppCompatTextView) itemView.findViewById(R.id.birth);
            view = (View) itemView.findViewById(R.id.view);

        }



    }
}
