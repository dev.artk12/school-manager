package com.manageschool;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Internet {

    Context context;
    StringRequest request;
    RequestQueue queue;
    public Internet(Context context){
        this.context = context;
    }


    public void RequestClassRoom(String Url , final Map<String , String > map , final RecyclerView recyclerView  , int code , final ArrayList<MasterClassRoom> classRooms){
        queue = Volley.newRequestQueue(context);

        request = new StringRequest(Request.Method.POST, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    MasterClassRoom classRoom;
                    JSONObject object;

                    for (int i = 0; i < jsonArray.length() ; i++) {

                        object = jsonArray.getJSONObject(i);
                        classRoom = new MasterClassRoom();

                        classRoom.setID(object.getString("ID"));
                        classRoom.setLesson(object.getString("LessonID"));
                        classRoom.setMaster(object.getString("MasterID"));
                        classRoom.setStartHour(object.getString("StartHour"));
                        classRoom.setEndHour(object.getString("EndHour"));
                        classRoom.setWeekDay(object.getString("WeekDay"));
                        classRoom.setLevelName(object.getString("LevelName"));
                        classRoom.setClassRoomName(object.getString("LevelClassName"));
                        classRoom.setStartQuran(object.getString("StartQuran"));
                        classRoom.setEndQuran(object.getString("EndQuran"));



                        classRooms.add(classRoom);
                    }
                    recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL , false));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    WeeklyscheduleAdapter adapter = new WeeklyscheduleAdapter(1,true,classRooms , context);
                    recyclerView.setAdapter(adapter);

                } catch (JSONException e) {
                    Log.e("errrrrrrror",e.getLocalizedMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

}
