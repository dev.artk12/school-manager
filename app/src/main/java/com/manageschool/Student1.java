package com.manageschool;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.SwitchCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Student1 extends Fragment {


    private static String BIRTHKRY = "Birth";
    private static String IDKEY = "ID";
    private static String NAMEKEY = "Name";
    private static String LASTNAMEKEY = "lastName";
    private static String FATHERNAME = "FatherName";
    private static String NATIONALKEY = "nationalId";
    private static String PHONEKEY = "Phone";


    RequestQueue queue;
    StringRequest request , update , deleteRequest;
    AppCompatEditText NationalID , Name , LastName , FatherName , Birth , Phone;
    String ID , name , lastname , fathername , nationalid , birth , phone ;
    AppCompatImageButton register , delete;
    RelativeLayout parent;
    Animation in , out;
    SwitchCompat switchCompat;
    String updateURL = "http://192.168.42.210/mysite/Rschool/UpdateStudent.php";
    String DeleteURL = "http://192.168.42.210/mysite/Rschool/DeletStudent.php";


    public static Student1 newInstance(String ID , String Name , String LastName , String FatherName , String NationalID , String Birth , String Phone) {

        Bundle args = new Bundle();

        Student1 fragment = new Student1();
        args.putString(IDKEY , ID);
        args.putString(NAMEKEY,Name);
        args.putString(LASTNAMEKEY , LastName);
        args.putString(FATHERNAME , FatherName);
        args.putString(NATIONALKEY , NationalID);
        args.putString(BIRTHKRY , Birth);
        args.putString(PHONEKEY , Phone);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_student1, container, false);
        LastName = (AppCompatEditText) view.findViewById(R.id.LastName);
        Name = (AppCompatEditText) view.findViewById(R.id.Name);
        NationalID = (AppCompatEditText) view.findViewById(R.id.nationalid);
        FatherName = (AppCompatEditText) view.findViewById(R.id.FatherName);
        Birth = (AppCompatEditText) view.findViewById(R.id.birth);
        Phone = (AppCompatEditText) view.findViewById(R.id.phone);

        register = (AppCompatImageButton) view.findViewById(R.id.save);
        delete = (AppCompatImageButton) view.findViewById(R.id.delet);
        parent = (RelativeLayout) view.findViewById(R.id.parent);
        in = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryin);
        out = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryout);
        switchCompat = (SwitchCompat) view.findViewById(R.id.EditMode);


        ID = getArguments().getString(IDKEY);
        name = getArguments().getString(NAMEKEY);
        lastname = getArguments().getString(LASTNAMEKEY);
        fathername = getArguments().getString(FATHERNAME);
        nationalid = getArguments().getString(NATIONALKEY);
        birth = getArguments().getString(BIRTHKRY);
        phone = getArguments().getString(PHONEKEY);


        queue = Volley.newRequestQueue(getContext());

        switchCompat.setChecked(false);

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean check = checkresult(Name.getText().toString(),LastName.getText().toString(),FatherName.getText().toString(),NationalID.getText().toString() , Birth.getText().toString());
                if (check){
                    UpdateRequest(ID ,Name.getText().toString(),LastName.getText().toString(),FatherName.getText().toString(),NationalID.getText().toString(), Birth.getText().toString());
                }
            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteRequest(ID);

            }
        });


        Name.setText(name);
        LastName.setText(lastname);
        NationalID.setText(nationalid);
        FatherName.setText(fathername);
        Birth.setText(birth);
        Phone.setText(phone);
        Name.setEnabled(false);
        LastName.setEnabled(false);
        FatherName.setEnabled(false);
        NationalID.setEnabled(false);
        Phone.setEnabled(false);

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    out(true, View.VISIBLE, in);

                }else if (!isChecked){

                    out(false, View.INVISIBLE, out);
                }
            }
        });

        return view;
    }

    private void out(boolean b, int invisible, Animation out) {
        Name.setEnabled(b);
        LastName.setEnabled(b);
        FatherName.setEnabled(b);
        NationalID.setEnabled(b);
        Phone.setEnabled(b);
        Birth.setEnabled(b);
        if (switchCompat.isChecked()){
            switchCompat.setChecked(b);
        }
        register.setVisibility(invisible);
        register.startAnimation(out);
        delete.setVisibility(invisible);
        delete.startAnimation(out);
    }

    private void DeleteRequest(final String id) {

        deleteRequest = new StringRequest(Request.Method.POST, DeleteURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(response,response);
                getActivity().onBackPressed();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errrrrrrrrrrror",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("ID",id);
                return map;
            }
        };

        deleteRequest.setShouldCache(false);
        deleteRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(deleteRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
        SetUp();

    }

    private boolean checkresult(String Name , String LastName , String fatherName , String NationalID , String birth){

        if (Name.trim().length() < 3 ){
            Toast.makeText(getContext(), "اسم باید بیش از سه کلمه باشد.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (LastName.trim().length() < 3){
            Toast.makeText(getContext(), "نام خانوادگی باید بیش از سه کلمه باشد.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (fatherName.trim().length() < 3 ){
            Toast.makeText(getContext(), "نام پدر باید بیش از سه کلمه باشد.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (NationalID.trim().length() != 10){
            Toast.makeText(getContext(), "کد ملی باید 10 عدد باشد.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (birth.length() != 4){
            Toast.makeText(getContext(), "تاریخ تولد معتبر نمیباشد.", Toast.LENGTH_SHORT).show();
            return  false;
        }else {
            return true;
        }
    }
    private void UpdateRequest(final String ID , final String Name , final String LastName , final String FatherName , final String NationalID , final String Birth){

        update = new StringRequest(Request.Method.POST, updateURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.trim().equals("ثبت شد")){
                    out(false, View.INVISIBLE, out);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("dddddddddddd",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("ID",ID);
                map.put("FirstName",Name);
                map.put("LastName",LastName);
                map.put("FatherName",FatherName);
                map.put("NationalID",NationalID);
                map.put("Birth",Birth);

                return map;
            }
        };

        update.setShouldCache(false);
        update.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(update);

    }


    private void SetUp(){

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        Name.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        LastName.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        FatherName.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        NationalID.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);

    }

}
