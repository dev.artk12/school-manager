package com.manageschool;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class LevelAdapter extends RecyclerView.Adapter<LevelAdapter.ViewHolder> {


    ArrayList<Level> levels;
    Context context;
    boolean check;

    LevelAdapter(ArrayList<Level> levels , Context context , boolean check){

        if (levels == null){
            levels = new ArrayList<>();
        }
        this.check = check;
        this.levels = levels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.itemstudi , viewGroup , false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.Name.setText(levels.get(i).getName());
        viewHolder.ClassRoomName.setText(levels.get(i).getClassRoomName());
        viewHolder.grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check){
                    Intent intent = new Intent(context , LevelActivity.class);
                    intent.putExtra("ID",levels.get(i).getID());
                    intent.putExtra("Name",levels.get(i).getName());
                    intent.putExtra("classRoom",levels.get(i).getClassRoomName());
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return levels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView Name , ClassRoomName;
        GridLayout grid;


        public ViewHolder(View itemView){
            super(itemView);
            Name = (AppCompatTextView) itemView.findViewById(R.id.Code);
            ClassRoomName = (AppCompatTextView) itemView.findViewById(R.id.Name);
            grid = (GridLayout) itemView.findViewById(R.id.grid);

        }

    }


}
