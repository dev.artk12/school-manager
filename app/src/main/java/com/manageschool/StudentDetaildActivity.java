package com.manageschool;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class StudentDetaildActivity extends AppCompatActivity {

    ViewPager viewPager;
    AppCompatTextView title;
    AppCompatButton Accepted;
    String ID;
    StringRequest request;
    RequestQueue queue;
    ViewAdapter adapter;
    String url = "http://192.168.42.210/mysite/Rschool/getCurrentStudent.php";
    TabLayout tabLayout;
    String AccpetdURL = "http://192.168.42.210/mysite/Rschool/UpdateAccepted.php";
    ArrayList<Neat> neats;
    String getNeatURL = "http://192.168.42.210/mysite/Rschool/getNeat.php";
    String Year;
    String GroupName , GroupID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sudent_detail);
        title = (AppCompatTextView) findViewById(R.id.studentname);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        Bundle bundle = getIntent().getExtras();
        ID = bundle.getString("ID");
        queue = Volley.newRequestQueue(this);
        Accepted = (AppCompatButton) findViewById(R.id.accepted);
        Accepted.setVisibility(View.GONE);
        neats = new ArrayList<>();


        if (ID != null){

            LoadStudentDetail(ID, new StudentVolleyCallBack() {
                @Override
                public void callBack(final Student student) {

                    adapter = new ViewAdapter(getSupportFragmentManager() ,student);
                    viewPager.setAdapter(adapter);
                    Year = student.getBirthYear();
                    title.setText(student.getName()+" "+student.getLastName());
                    tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
                    viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
                    int accepted = Integer.parseInt(student.getAccepted());
                    if (accepted == 0){
                        Accepted.setVisibility(View.VISIBLE);
                    }

                    Accepted.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                                AcceptRequest(student.getID());

                        }
                    });


                }
            });

        }else{
            Toast.makeText(this, "Somthing Wrong", Toast.LENGTH_SHORT).show();
        }



    }

    private void AcceptRequest(final String StudentID) {

        StringRequest request = new StringRequest(Request.Method.POST, AccpetdURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("adsasda",response);
                Accepted.setVisibility(View.GONE);

                Load(StudentID);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("ID",ID);
                map.put("Accepted" , "1");
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);

        queue.add(request);


    }
    private void Load(final String StudentID){
        StringRequest request = new StringRequest(Request.Method.POST, getNeatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                Log.e("ddddddddd",response);
                if (response.equals("[]")){

                }if (response != null){
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        Neat neat;

                        for (int i = 0; i < jsonArray.length() ; i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            neat = new Neat();
                            neat.setGroupID(object.getString("GroupID"));
                            neat.setGroupName(object.getString("GroupName"));
                            neat.setLevelID(object.getString("LevelID"));
                            neat.setLevelName(object.getString("LevelName"));
                            neat.setStartYear(object.getString("Start"));
                            neat.setEndYear(object.getString("End"));

                            neats.add(neat);
                        }

                        if (neats.size() != 0){
                            String year = CompareCalensars(Year);
                            int Birth = Integer.valueOf(year) ;


                            for (int i = 0; i < neats.size() ; i++) {
                                int Start = Integer.parseInt(neats.get(i).getStartYear());
                                int End = Integer.parseInt(neats.get(i).getEndYear());

                                if (Birth < End && Birth >= Start){
                                    GroupName = neats.get(i).getGroupName();
                                    GroupID = neats.get(i).getGroupID();

                                }

                            }

                            LevelCheck check = LevelCheck.newInstance(GroupName,GroupID , StudentID);
                            check.setCancelable(true);
                            check.show(getSupportFragmentManager(),"level");

                        }

                    }catch (JSONException e){
                        Log.e("ddddddddddd",e.getLocalizedMessage());
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }
    private String CompareCalensars(String studentbirth ) {


        int start = Integer.valueOf(studentbirth);

        SimpleDateFormat Year = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        String yearString = Year.format(new Date());

        SimpleDateFormat month = new SimpleDateFormat("MM", Locale.ENGLISH);
        String monthString = month.format(new Date());


        SimpleDateFormat day = new SimpleDateFormat("dd", Locale.ENGLISH);
        String dayString = day.format(new Date());

        PersianDate pdate = new PersianDate();
        PersianDateFormat format = new PersianDateFormat();
        format.format(pdate);

        int[] jalili = pdate.toJalali(Integer.valueOf(yearString) , Integer.valueOf(monthString) , Integer.valueOf(dayString));

        int nowyear = jalili[0];


        int birth = nowyear - start;

        return String.valueOf(birth);

    }

    private void LoadStudentDetail(final String ID , final StudentVolleyCallBack callBack) {

        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ffffffffffff",response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject object = jsonArray.getJSONObject(0);
                    Student student = new Student();

                    student.setNationalID(object.getString("nationalID"));
                    student.setID(object.getString("ID"));
                    student.setLastName(object.getString("LastName"));
                    student.setName(object.getString("FirstName"));
                    student.setFatherName(object.getString("FatherName"));
                    student.setBirthYear(object.getString("Birth"));
                    student.setAccepted(object.getString("accepted"));
                    student.setPhone(object.getString("Phone"));





                    callBack.callBack(student);

                } catch (JSONException e) {
                    Log.e("ffffffffffff",e.getLocalizedMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ssssssssss",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("ID",ID);
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);
        queue.add(request);

    }
    @Override
    public void onBackPressed() {
        finish();
    }

    protected class ViewAdapter extends FragmentPagerAdapter{

        String ID , Name , LastName , FatherName , NationalID , BirthYear , Phone;
        public ViewAdapter(FragmentManager fm , Student student) {
            super(fm);
            this.ID = student.getID();
            this.Name = student.getName();
            this.LastName = student.getLastName();
            this.FatherName = student.getFatherName();
            this.NationalID = student.getNationalID();
            this.BirthYear = student.getBirthYear();
            this.Phone = student.getPhone();

        }

        @Override
        public Fragment getItem(int i) {
            switch (i){
                case 0 : Student1 st1 = Student1.newInstance(ID , Name , LastName , FatherName , NationalID , BirthYear , Phone); return st1;
                case 1 : Student2 st2 = Student2.newInstance(ID); return st2;
                case 2 : return new Student3();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
    interface StudentVolleyCallBack{
        void callBack(Student student);
    }

}
