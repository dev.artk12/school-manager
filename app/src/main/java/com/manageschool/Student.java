package com.manageschool;

import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

public class Student {

    private boolean checked;
    private String ID;
    private String nationalID;
    private String Name;
    private String LastName;
    private String FatherName;
    private String BirthYear;
    private String Accepted;

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    private String Phone;


    public String getAccepted() {
        return Accepted;
    }

    public void setAccepted(String accepted) {
        Accepted = accepted;
    }

    public String getBirthYear() {
        return BirthYear;
    }

    public void setBirthYear(String birthYear) {
        BirthYear = birthYear;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getNationalID() {
        return nationalID;
    }

    public void setNationalID(String nationalID) {
        this.nationalID = nationalID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public JSONObject getJsonObject(){

        JSONObject object = new JSONObject();

        try {

            object.put("ID" , ID);


            return object;

        }catch (Exception e){
            Log.e("dddddddddddd",e.getMessage());
        }


        return null;
    }
}
