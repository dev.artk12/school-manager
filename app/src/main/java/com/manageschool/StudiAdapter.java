package com.manageschool;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class StudiAdapter extends RecyclerView.Adapter<StudiAdapter.ViewHolder> {


    ArrayList<Studi> studis;
    Context context;
    boolean check;
    private String STUDIIDKEY ="StudiID";
    private String STUDINAMEKEY ="StudiNAME";
    private String STUDICODEKEY ="StudiCODE";
    private String STUDILEVELKEY ="StudiLEVELID";



    StudiAdapter(ArrayList<Studi> studis , Context context , boolean check){

        if (studis == null){
            studis = new ArrayList<>();
        }
        this.check = check;
        this.studis = studis;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.itemstudi , viewGroup , false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.Code.setText(studis.get(i).getCode());
        viewHolder.Name.setText(studis.get(i).getName());
        viewHolder.grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check){
                    Intent intent = new Intent(context , StudiDetailActivity.class);
                    intent.putExtra(STUDIIDKEY , studis.get(i).getID());
                    intent.putExtra(STUDINAMEKEY , studis.get(i).getName());
                    intent.putExtra(STUDICODEKEY , studis.get(i).getCode());
                    intent.putExtra(STUDILEVELKEY , studis.get(i).getLevelCode());
                    context.startActivity(intent);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return studis.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView Code , Name;
        GridLayout grid;


        public ViewHolder(View itemView){
            super(itemView);
            Code = (AppCompatTextView) itemView.findViewById(R.id.Code);
            Name = (AppCompatTextView) itemView.findViewById(R.id.Name);
            grid = (GridLayout) itemView.findViewById(R.id.grid);

        }

    }


}
