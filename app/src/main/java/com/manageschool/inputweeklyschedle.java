package com.manageschool;


import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class inputweeklyschedle extends Fragment implements TimePickerDialog.OnTimeSetListener {


    private static final String GROUPIDKEY = "GroupKey" ;
    AppCompatImageButton Done , Watch;
    AppCompatTextView StartTimeTXT , EndTimeTXT , MasterTXT , StudiTXT , LevelTXT , GroupTXT , StartQuran , EndQuran;
    AppCompatSpinner spinner;
    ArrayAdapter<CharSequence> adapter;
    String dayname = "شنبه";
    AppCompatImageButton Students , Record;
    StringRequest MasterRequest , StudiRequest , Groups, InsertRequest , levelRequest , QuranRequest;
    RequestQueue queue;
    String MasterUrl = "http://192.168.42.210/mysite/Rschool/getMaster.php";
    String GroupstUrl = "http://192.168.42.210/mysite/Rschool/getGroups.php";
    String StudiUrl = "http://192.168.42.210/mysite/Rschool/getStudiLevel.php";
    String InsertUrl = "http://192.168.42.210/mysite/Rschool/InsertMasterClassRoom.php";
    String UpdateUrl = "http://192.168.42.210/mysite/Rschool/UpdateMasterClassRoom.php";
    String LevelUrl = "http://192.168.42.210/mysite/Rschool/getLevel.php";
    String QuranUrl = "http://192.168.42.210/mysite/Rschool/getQuran.php";

    RelativeLayout parent;
    private static String IDCLASSKEY = "IDClass";
    SwitchCompat switchCompat;

    private static String D1ID = "ID";
    private static String D1STARTHOUR = "StartHour";
    private static String D1ENDHOUR = "EndHour";
    private static String D1WEEKDAY = "WeekDay";
    private static String D1MASTER = "Master";
    private static String D1LEVEL = "Level";
    private static String D1STUDI = "Studi";




    RecyclerView recyclerView;
    Animation in , out , fadein , fadeout;
    StudiAdapter studiadapter;
    ArrayList<Studi> studis;
    private int TAG;
    Map<String , String > result;

    private static String WEEKMASTERKEY = "MasterKey";
    private static String WEEKSTUDIKEY = "StudiKey";
    private static String WEEKDAYKEY = "WeekDayKey";
    private static String WEEKSTARTHOURKEY = "StartHour";
    private static String WEEKENDHOURKEY = "EnddHour";
    private static String WEEKLEVELKEY = "Level";
    private static String WEEKSTARTQURAN = "StartQuran";
    private static String WEEKENDQURAN = "EndQuran";
    private static String D1CHECK = "Check";
    boolean EditMode ;
    String ID , StartHour , EndHour , Weekday , Master , Level , Studi;
    ArrayList<Master> masters;
    ArrayList<Groups> list;
    ArrayList<Quran> qurans;

    private ArrayList<Level> Levels;
    int IDClass;


    public static inputweeklyschedle newInstance(boolean EditMode , String ClassID , String ID , String StartHour , String EndHour , String Weekday , String Master , String Level , String Studi) {

        Bundle args = new Bundle();

        inputweeklyschedle fragment = new inputweeklyschedle();
        if (ClassID == null){
            args.putInt(IDCLASSKEY , 0);
        }else {
            args.putInt(IDCLASSKEY , Integer.parseInt(ClassID));
        }

        args.putString(D1ID , ID);
        args.putString(D1STARTHOUR , StartHour);
        args.putString(D1ENDHOUR , EndHour);
        args.putString(D1WEEKDAY , Weekday);
        args.putString(D1MASTER , Master);
        args.putString(D1LEVEL , Level);
        args.putString(D1STUDI , Studi);
        args.putBoolean(D1CHECK , EditMode);


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        LoadQuran();
    }

    private void LoadQuran() {

        queue = Volley.newRequestQueue(getContext());


        qurans = new ArrayList<>();

        QuranRequest = new StringRequest(Request.Method.POST, QuranUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try{
                    JSONArray jsonArray = new JSONArray(response);

                    for (int i = 0; i < jsonArray.length() ; i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        Quran quran = new Quran();

                        quran.setSura(object.getString("sura"));
                        quran.setIndex(object.getString("index"));
                        quran.setSuraName(object.getString("suraName"));

                        qurans.add(quran);

                    }

                }catch (JSONException E){
                    Log.e("JsonException" , E.getLocalizedMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        QuranRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15), DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ));
        QuranRequest.setShouldCache(false);

        queue.add(QuranRequest);



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_inputweeklyschedle, container, false);

        Done = (AppCompatImageButton) view.findViewById(R.id.next);
        StartTimeTXT = (AppCompatTextView) view.findViewById(R.id.starttime);
        EndTimeTXT = (AppCompatTextView) view.findViewById(R.id.endtime);
        Students = (AppCompatImageButton) view.findViewById(R.id.students);
        MasterTXT = (AppCompatTextView) view.findViewById(R.id.master);
        StudiTXT = (AppCompatTextView) view.findViewById(R.id.studi);
        LevelTXT = (AppCompatTextView) view.findViewById(R.id.level);
        Record = (AppCompatImageButton) view.findViewById(R.id.done);
        GroupTXT = (AppCompatTextView) view.findViewById(R.id.groupName);
        parent = (RelativeLayout) view.findViewById(R.id.parent);
        Watch = (AppCompatImageButton) view.findViewById(R.id.watch);
        switchCompat = (SwitchCompat) view.findViewById(R.id.readingclass);
        StartQuran = (AppCompatTextView) view.findViewById(R.id.startQuran);
        EndQuran = (AppCompatTextView) view.findViewById(R.id.endQuran);
        fadein = AnimationUtils.loadAnimation(getContext() , android.R.anim.fade_in);
        fadeout = AnimationUtils.loadAnimation(getContext() , android.R.anim.fade_out);
        result = new HashMap<>();




        parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        in = AnimationUtils.loadAnimation(getContext() , R.anim.slide_left_week);
        out = AnimationUtils.loadAnimation(getContext() , R.anim.slide_right_week);
        Levels = new ArrayList<>();
        list = new ArrayList<>();
        masters = new ArrayList<>();


        Students.getDrawable().setColorFilter(getActivity().getResources().getColor(R.color.Base), PorterDuff.Mode.SRC_IN);

        IDClass = getArguments().getInt(IDCLASSKEY);
        ID = getArguments().getString(D1ID);
        StartHour = getArguments().getString(D1STARTHOUR);
        EndHour = getArguments().getString(D1ENDHOUR);
        Weekday = getArguments().getString(D1WEEKDAY);
        Master = getArguments().getString(D1MASTER);
        Level = getArguments().getString(D1LEVEL);
        Studi = getArguments().getString(D1STUDI);
        EditMode = getArguments().getBoolean(D1CHECK);
        spinner = (AppCompatSpinner) view.findViewById(R.id.days);

        switchCompat.setChecked(false);
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked){
                    StartQuran.setVisibility(View.VISIBLE);
                    StartQuran.startAnimation(fadein);
                    EndQuran.setVisibility(View.VISIBLE);
                    EndQuran.startAnimation(fadein);
                }else {
                    StartQuran.setVisibility(View.INVISIBLE);
                    StartQuran.startAnimation(fadeout);
                    EndQuran.setVisibility(View.INVISIBLE);
                    StartQuran.setText("شروع");
                    EndQuran.setText("پایان");
                    EndQuran.setVisibility(View.INVISIBLE);
                    EndQuran.startAnimation(fadeout);
                    result.put(WEEKENDQURAN,"0");
                    result.put(WEEKSTARTQURAN,"0");

                    if (TAG == 15 || TAG == 16){
                        recyclerView.setVisibility(View.INVISIBLE);
                        recyclerView.startAnimation(out);
                    }
                }

            }
        });
        StartQuran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TAG != 15){
                    TAG = 15;

                    StartQuranMethod(15, StartQuran, WEEKSTARTQURAN);
                }


            }
        });
        EndQuran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TAG != 16){
                    TAG = 16;

                    StartQuranMethod(16, EndQuran, WEEKENDQURAN);
                }

            }
        });

        if (EditMode){
            StartTimeTXT.setText(StartHour);
            EndTimeTXT.setText(EndHour);
            result.put(WEEKSTARTHOURKEY, StartHour);
            result.put(WEEKENDHOURKEY, EndHour);

            result.put(WEEKMASTERKEY , "");
            result.put(WEEKDAYKEY , "0");
            result.put(WEEKSTUDIKEY , "");

        }else {
            result.put(WEEKMASTERKEY , "");
            result.put(WEEKDAYKEY , "0");
            result.put(WEEKSTARTHOURKEY , "");
            result.put(WEEKSTUDIKEY , "");
            result.put(WEEKENDHOURKEY , "");
        }


        adapter = ArrayAdapter.createFromResource(getContext(),R.array.weekdays,R.layout.spinnerweekitem);
        spinner.setAdapter(adapter);


        studis = new ArrayList<>();


        Students.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TAG != 1){
                    TAG = 1;
                    LoadStudentGroups();
                }

            }
        });

        Record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (result.get(WEEKMASTERKEY) == null || result.get(WEEKMASTERKEY).length() == 0){
                    Toast.makeText(getContext(), "استاد انتخاب نشده است.", Toast.LENGTH_SHORT).show();
                }else if (result.get(WEEKSTARTHOURKEY) == null || result.get(WEEKSTARTHOURKEY).length() == 0){
                    Toast.makeText(getContext(), "ساعت شروع مشخص نشده است.", Toast.LENGTH_SHORT).show();
                }else if (result.get(WEEKENDHOURKEY) == null || result.get(WEEKENDHOURKEY).length() == 0){
                    Toast.makeText(getContext(), "ساعت پایان مشخص نشده است.", Toast.LENGTH_SHORT).show();
                } else if (result.get(WEEKSTUDIKEY) == null || result.get(WEEKSTUDIKEY).length() == 0) {
                    Toast.makeText(getContext(), "نام درس وارد نشده است", Toast.LENGTH_SHORT).show();
                }else if (result.get(GROUPIDKEY) == null || result.get(GROUPIDKEY).length() == 0){
                    Toast.makeText(getContext(), "دانش آموزی انتخاب نشده است.", Toast.LENGTH_SHORT).show();
                }else if (result.get(WEEKDAYKEY) == null || result.get(WEEKDAYKEY).length() == 0){
                    Toast.makeText(getContext(), "لطفا روز هفته را وارد کنید..", Toast.LENGTH_SHORT).show();
                }else if (result.get(WEEKLEVELKEY) == null || result.get(WEEKLEVELKEY).length() == 0){
                    Toast.makeText(getContext(), "سطح کلاس مشخص نشده است.", Toast.LENGTH_SHORT).show();
                }
                else {
                    String Master = result.get(WEEKMASTERKEY);
                    String StartHour = result.get(WEEKSTARTHOURKEY);
                    String EndHour = result.get(WEEKENDHOURKEY);
                    String Studi = result.get(WEEKSTUDIKEY);

                    String Day = result.get(WEEKDAYKEY);
                    String level = result.get(WEEKLEVELKEY);
                    String GroupsID = result.get(GROUPIDKEY);
                    String StartQuran;
                    String EndQuran;
                    if (result.get(WEEKSTARTQURAN) == null || result.get(WEEKENDQURAN) == null ){
                        StartQuran = "0";
                        EndQuran = "0";
                    }else {
                        StartQuran = result.get(WEEKSTARTQURAN);
                        EndQuran = result.get(WEEKENDQURAN);
                    }
                    InsertRequest(StartHour , EndHour , Studi , Master , Day ,level , IDClass+1 , GroupsID , StartQuran , EndQuran);
                }
            }
        });


        MasterTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TAG != 2) {
                    TAG = 2;

                    if (masters.size() == 0){
                        LoadMasters(new VolleyCallbackMater() {
                            @Override
                            public void callback(String response) {
                                try {
                                    Master master;
                                    masters = new ArrayList<>();
                                    JSONArray jsonArray = new JSONArray(response);
                                    for (int i = 0; i < jsonArray.length() ; i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        master = new Master();

                                        master.setID(object.getString("ID"));
                                        master.setFirstName(object.getString("FirstName"));
                                        master.setLastName(object.getString("LastName"));
                                        master.setProfileAddress(object.getString("Photo"));
                                        masters.add(master);
                                    }

                                    recyclerView.setVisibility(View.VISIBLE);
                                    recyclerView.startAnimation(in);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                                    Drawable drawable = getResources().getDrawable(R.drawable.camerabg );
                                    MasterItemAdapter adapter = new MasterItemAdapter(masters,getContext(),getResources() ,drawable , false);
                                    recyclerView.setAdapter(adapter);

                                    recyclerView.addOnItemTouchListener(
                                            new RecyclerItemClickListener(getActivity(), new   RecyclerItemClickListener.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(View view, int position) {
                                                    if (TAG == 2){
                                                        MasterTXT.setText(masters.get(position).getFirstName()+" "+masters.get(position).getLastName());
                                                        recyclerView.setVisibility(View.INVISIBLE);
                                                        recyclerView.startAnimation(out);
                                                        result.put(WEEKMASTERKEY , String.valueOf(masters.get(position).getID()));
                                                        TAG = 4;
                                                    }
                                                }
                                            })
                                    );
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }else {
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.startAnimation(in);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        Drawable drawable = getResources().getDrawable(R.drawable.camerabg );
                        MasterItemAdapter adapter = new MasterItemAdapter(masters,getContext(),getResources() ,drawable , false);
                        recyclerView.setAdapter(adapter);
                    }

                }

            }
        });

        StudiTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TAG != 3 ){
                    TAG = 3 ;

                        if (result.get(WEEKLEVELKEY) != null ) {

                            if (result.get(WEEKLEVELKEY).length() >= 1){


                                LoadStudi(result.get(WEEKLEVELKEY),new VolleyCallbackStudi() {
                                    @Override
                                    public void callback(String Response) {
                                        Studi studi ;
                                        if (Response != null){
                                            studis = new ArrayList<>();
                                            try {
                                                JSONArray jsonArray = new JSONArray(Response);
                                                for (int i = 0; i < jsonArray.length() ; i++) {
                                                    studi = new Studi();
                                                    JSONObject object = jsonArray.getJSONObject(i);

                                                    studi.setID(object.getString("ID"));
                                                    studi.setName(object.getString("Name"));
                                                    studi.setCode(object.getString("LessonCode"));

                                                    studis.add(studi);
                                                }
                                                recyclerView.setVisibility(View.VISIBLE);
                                                recyclerView.startAnimation(in);
                                                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                                studiadapter = new StudiAdapter(studis , getContext(),false );
                                                recyclerView.setAdapter(studiadapter);

                                                recyclerView.addOnItemTouchListener(
                                                        new RecyclerItemClickListener(getActivity(), new   RecyclerItemClickListener.OnItemClickListener() {
                                                            @Override
                                                            public void onItemClick(View view, int position) {
                                                                if (TAG == 3){
                                                                    StudiTXT.setText(studis.get(position).getName());
                                                                    recyclerView.setVisibility(View.INVISIBLE);
                                                                    recyclerView.startAnimation(out);
                                                                    result.put(WEEKSTUDIKEY , String.valueOf(studis.get(position).getID()));
                                                                    TAG = 4;
                                                                }
                                                            }
                                                        })
                                                );

                                            } catch (JSONException e) {

                                            }
                                        }
                                    }
                                });
                            }

                        }else {
                            Toast.makeText(getContext(), "سطحی انتخاب نشده است.", Toast.LENGTH_SHORT).show();
                        }
                }else {
                    Toast.makeText(getContext(), "لطفا سطح را مشخص کنید.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        LevelTXT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TAG != 5){
                    TAG = 5 ;
                    if (Levels.size() == 0){
                        LoadLevles(new VolleyCallbackStudi() {
                            @Override
                            public void callback(String Response) {
                                Level level ;
                                if (Response != null){
                                    Levels = new ArrayList<>();

                                    try {
                                        JSONArray jsonArray = new JSONArray(Response);
                                        for (int i = 0; i < jsonArray.length() ; i++) {
                                            level = new Level();
                                            JSONObject object = jsonArray.getJSONObject(i);

                                            level.setID(object.getString("ID"));
                                            level.setName(object.getString("Name"));
                                            level.setClassRoomName(object.getString("ClassRoom"));

                                            Levels.add(level);
                                        }
                                        recyclerView.setVisibility(View.VISIBLE);
                                        recyclerView.startAnimation(in);
                                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                                        LevelAdapter adapter = new LevelAdapter(Levels,getContext() , false);
                                        recyclerView.setAdapter(adapter);

                                        recyclerView.addOnItemTouchListener(
                                                new RecyclerItemClickListener(getActivity(), new   RecyclerItemClickListener.OnItemClickListener() {
                                                    @Override
                                                    public void onItemClick(View view, int position) {
                                                        if (TAG == 5){
                                                            LevelTXT.setText(Levels.get(position).getName());
                                                            recyclerView.setVisibility(View.INVISIBLE);
                                                            recyclerView.startAnimation(out);
                                                            result.put(WEEKLEVELKEY , String.valueOf(Levels.get(position).getID()));

                                                            TAG = 4;
                                                        }
                                                    }
                                                })
                                        );

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            }
                        });
                    }else {
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.startAnimation(in);
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        LevelAdapter adapter = new LevelAdapter(Levels,getContext() , false);
                        recyclerView.setAdapter(adapter);
                    }

                }
            }
        });


        Watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePicker();
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position){
                    case 0 : dayname = "شنبه"; break;
                    case 1 : dayname = "یکشنبه"; break;
                    case 2 : dayname = "دوشنبه"; break;
                    case 3 : dayname = "سه شنبه"; break;
                    case 4 : dayname = "چهارشنبه"; break;
                    case 5 : dayname = "پنج شنبه"; break;
                    case 6 : dayname = "جمعه"; break;
                }

                result.put(WEEKDAYKEY , String.valueOf(position));

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return view;
    }

    private void StartQuranMethod(final int i, final AppCompatTextView startQuran, final String weekstartquran) {
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.startAnimation(in);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        QuranAdapter adapter = new QuranAdapter(getContext(), qurans);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (TAG == i) {
                    startQuran.setText(qurans.get(position).getSuraName());
                    result.put(weekstartquran, qurans.get(position).getSura());
                }

            }
        }));
    }


    private void TimePickerCall() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                inputweeklyschedle.this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.show(getActivity().getFragmentManager(), "h");
    }

    private void TimePicker() {
        TimePickerCall();
    }

    private void InsertRequest(final String StartHour , final String EndHour , final String LessonID, final String MasterID ,
                              final String Weekday , final String Level , final int IDClass , final String GroupId , final String StartQuran , final String EndQuran ){


        String URL ;
        if (EditMode){
            URL = UpdateUrl;
        }else {
            URL = InsertUrl;
        }

        InsertRequest = new StringRequest(Request.Method.POST,URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                getActivity().onBackPressed();
                Log.e("eeeeeeeeeeeeeee",response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Errrror",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                if (EditMode){
                    map.put("ID",ID);
                }
                map.put("StartHour",StartHour);
                map.put("EndHour",EndHour);
                map.put("MasterID",MasterID);
                map.put("WeekDay",Weekday);
                map.put("LessonID",LessonID);
                map.put("Level",Level);
                map.put("GroupID",GroupId);
                map.put("IDClass",String.valueOf(IDClass));
                map.put("StartQuran",StartQuran);
                map.put("EndQuran",EndQuran);

                return map;
            }
        };


        InsertRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        InsertRequest.setShouldCache(false);

        queue.add(InsertRequest);

    }
    private void LoadMasters(final VolleyCallbackMater callback){
        MasterRequest = new StringRequest(Request.Method.POST, MasterUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.callback(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };

        MasterRequest.setShouldCache(false);
        MasterRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(MasterRequest);
    }
    private void LoadStudi(final String LevelID, final VolleyCallbackStudi callback){

        StudiRequest = new StringRequest(Request.Method.POST, StudiUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.callback(response);
                Log.e("dddddddd",response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Dsdvsdvsedvasedvas" , error.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("LevelID",LevelID);
                return map;
            }
        };

        StudiRequest.setShouldCache(false);

        StudiRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(StudiRequest);
    }

    private void LoadLevles(final VolleyCallbackStudi callback){

        levelRequest = new StringRequest(Request.Method.POST, LevelUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.callback(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Dsdvsdvsedvasedvas" , error.getMessage());
            }
        });

        levelRequest.setShouldCache(false);

        levelRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(levelRequest);
    }
    private void LoadStudentGroups(){

        if (list.size() == 0){

            Groups = new StringRequest(Request.Method.POST, GroupstUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("dddddddddd", response);


                    if (response != null){

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            Groups groups ;
                            for (int i = 0; i < jsonArray.length() ; i++) {
                                groups = new Groups();
                                JSONObject object = jsonArray.getJSONObject(i);
                                if (object.getString("GroupID").equals("0") || object.getString("GroupID").equals("1")  ){

                                }else {

                                    groups.setNames(object.getString("Name"));
                                    groups.setColor(object.getString("Color"));
                                    groups.setGroupsID(object.getString("GroupID"));


                                    list.add(groups);
                                }

                            }
                            recyclerView.setVisibility(View.VISIBLE);
                            recyclerView.startAnimation(in);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext() , LinearLayoutManager.VERTICAL , false));
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            GroupsRecyclerView adapter = new GroupsRecyclerView(false ,list,getContext(), (AppCompatActivity) getActivity());
                            recyclerView.setAdapter(adapter);
                            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    if (TAG == 1){
                                        GroupTXT.setText(list.get(position).getNames());
                                        result.put(GROUPIDKEY , list.get(position).getGroupsID());
                                    }
                                }
                            }));


                        } catch (JSONException e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }


                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("fdffffffffff",error.getLocalizedMessage());
                }
            });


            Groups.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Groups.setShouldCache(false);

            queue.add(Groups);

        }else {
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.startAnimation(in);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext() , LinearLayoutManager.VERTICAL , false));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            GroupsRecyclerView adapter = new GroupsRecyclerView(false ,list,getContext(), (AppCompatActivity) getActivity());
            recyclerView.setAdapter(adapter);

            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    if (TAG == 1){
                        GroupTXT.setText(list.get(position).getNames());
                    }
                }
            }));

        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {


        String smin = String.valueOf(minute);
        String emin = String.valueOf(minuteEnd);

        if (hourOfDay == 0 ){
            hourOfDay = 12;
        }if (hourOfDayEnd == 0 ){
            hourOfDayEnd = 12;
        }if (smin.equals("0")){
            smin = "00";
        }if (emin.equals("0")){
            emin = "00";
        }


        if (hourOfDay < hourOfDayEnd){
            StartTimeTXT.setText(hourOfDay+":"+smin);
            result.put(WEEKSTARTHOURKEY , StartTimeTXT.getText().toString());
            EndTimeTXT.setText(hourOfDayEnd+":"+emin);
            result.put(WEEKENDHOURKEY , EndTimeTXT.getText().toString());
        }else {
            Toast.makeText(getContext(), "ساعت شروع بزرگتر از ساعت پایان است.", Toast.LENGTH_SHORT).show();
        }


    }



    public interface VolleyCallbackMater{
        void callback(String response);
    }

    public interface VolleyCallbackStudi{
        void callback(String response);
    }

}
