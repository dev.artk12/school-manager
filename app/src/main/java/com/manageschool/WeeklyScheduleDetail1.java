package com.manageschool;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


public class WeeklyScheduleDetail1 extends Fragment {


    private static String D1ID = "ID";
    private static String D1STARTHOUR = "StartHour";
    private static String D1STARTQURAN = "EndQuran";
    private static String D1ENDQURAN = "StartQuran";
    private static String D1ENDHOUR = "EndHour";
    private static String D1WEEKDAY = "WeekDay";
    private static String D1MASTER = "Master";
    private static String D1LEVEL = "Level";
    private static String D1STUDI = "Studi";
    private static String IDCLASSKEY = "ClassID";
    private static String D1CHECK = "Check";

    private String DeleteURL = "http://192.168.42.210/mysite/Rschool/DeleteMasterClassRoom.php";


    AppCompatEditText weekDay , MasterEdt , LevelEdt , StudiEdt ;
    AppCompatTextView  startHour , endHour , EndQuranTXT , StartQuranTXT;

    SwitchCompat switchCompat;
    String ID , StartHour , EndHour , WeekDay , Master , Level , Studi , StartQuran , EndQuran ;
    int  ClassID;
    boolean check;
    AppCompatImageButton watch , edit, delete;
    Animation in , out;
    RequestQueue queue;
    Map<String , String > result;
    GridLayout quran;

    public static WeeklyScheduleDetail1 newInstance(boolean check , String ClassID ,String ID , String StartHour ,
                                                    String EndHour , String Weekday , String Master , String Level , String Studi , String StartQuran , String EndQuran) {

        Bundle args = new Bundle();

        WeeklyScheduleDetail1 fragment = new WeeklyScheduleDetail1();
        if (ClassID == null){
            args.putInt(IDCLASSKEY , 0);
        }else {
            args.putInt(IDCLASSKEY , Integer.parseInt(ClassID));
        }

        args.putString(D1ID , ID);
        args.putString(D1STARTHOUR , StartHour);
        args.putString(D1ENDHOUR , EndHour);
        args.putString(D1WEEKDAY , Weekday);
        args.putString(D1MASTER , Master);
        args.putString(D1LEVEL , Level);
        args.putString(D1STUDI , Studi);
        args.putString(D1STARTQURAN , StartQuran);
        args.putString(D1ENDQURAN , EndQuran);
        args.putBoolean(D1CHECK , check);



        fragment.setArguments(args);
        return fragment;
    }
    private void Timer() {
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.e("TestTimer","WORKING FINE");
            }
        };
        timer.schedule(timerTask,5000);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weekly_schedule_detail1, container, false);

        switchCompat = (SwitchCompat) view.findViewById(R.id.EditMode);
        startHour = (AppCompatTextView) view.findViewById(R.id.starthour);
        watch = (AppCompatImageButton) view.findViewById(R.id.watch);
        endHour = (AppCompatTextView) view.findViewById(R.id.endhour);
        weekDay = (AppCompatEditText) view.findViewById(R.id.weekday);
        MasterEdt = (AppCompatEditText) view.findViewById(R.id.master);
        LevelEdt = (AppCompatEditText) view.findViewById(R.id.level);
        StudiEdt = (AppCompatEditText) view.findViewById(R.id.studi);
        edit = (AppCompatImageButton) view.findViewById(R.id.save);
        StartQuranTXT = (AppCompatTextView) view.findViewById(R.id.startQuran);
        EndQuranTXT = (AppCompatTextView) view.findViewById(R.id.endQuran);

        delete = (AppCompatImageButton) view.findViewById(R.id.delete);
        quran = (GridLayout) view.findViewById(R.id.quran);


        ID = getArguments().getString(D1ID);
        StartHour = getArguments().getString(D1STARTHOUR);
        EndHour = getArguments().getString(D1ENDHOUR);
        WeekDay = getArguments().getString(D1WEEKDAY);
        Master = getArguments().getString(D1MASTER);
        Level = getArguments().getString(D1LEVEL);
        Studi = getArguments().getString(D1STUDI);
        check = getArguments().getBoolean(D1CHECK);
        ClassID = getArguments().getInt(IDCLASSKEY);
        StartQuran = getArguments().getString(D1STARTQURAN);
        EndQuran = getArguments().getString(D1ENDQURAN);


        if (!StartQuran.trim().equals("null") || !EndQuran.trim().equals("null") ){
            quran.setVisibility(View.VISIBLE);
            StartQuranTXT.setText(StartQuran);
            EndQuranTXT.setText(EndQuran);
        }



        result = new HashMap<>();



        queue = Volley.newRequestQueue(getContext());
        in = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryin);
        out = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryout);

        String day = null;
        switch (Integer.valueOf(WeekDay)){
                case 0 : day = "شنبه" ; break;
                case 1 : day = "یکشنبه" ; break;
                case 2 : day = "دوشنبه" ; break;
                case 3 : day = "سه شنبه" ; break;
                case 4 : day = "چهارشنبه" ; break;
                case 5 : day = "پنجشنبه" ; break;
                case 6 : day = "جمعه" ; break;
            }


        if (ID == null && StartHour == null && EndHour == null && WeekDay == null && Master == null && Level == null && Studi == null){
            Toast.makeText(getContext(), "Somthing wrong", Toast.LENGTH_SHORT).show();
        }else {

            startHour.setEnabled(false);
            endHour.setEnabled(false);
            weekDay.setEnabled(false);
            MasterEdt.setEnabled(false);
            LevelEdt.setEnabled(false);
            StudiEdt.setEnabled(false);
            watch.setEnabled(false);

            switchCompat.setChecked(false);

            startHour.setText(StartHour);
            endHour.setText(EndHour);
            weekDay.setText(day);
            MasterEdt.setText(Master);
            LevelEdt.setText(Level);
            StudiEdt.setText(Studi);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteRequest(ID);
                }
            });

            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    inputweeklyschedle fragment = inputweeklyschedle.newInstance(true ,result.get(IDCLASSKEY) ,ID,StartHour,EndHour,WeekDay,Master,Level,Studi);
                    transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    transaction.replace(android.R.id.content, fragment);
                    transaction.commit();
                }
            });




            /*watch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TimePickerCall();
                }
            });*/

            if (!check){
                switchCompat.setVisibility(View.GONE);
            }
            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    if (isChecked){
                        edit.setVisibility(View.VISIBLE);
                        delete.setVisibility(View.VISIBLE);
                        edit.startAnimation(in);
                        delete.startAnimation(in);
                    }else {
                        edit.setVisibility(View.INVISIBLE);
                        delete.setVisibility(View.INVISIBLE);
                        edit.startAnimation(out);
                        delete.startAnimation(out);
                    }

                }
            });


        }

        return view;
    }

    private void deleteRequest(final String id) {
        StringRequest delete = new StringRequest(Request.Method.POST, DeleteURL , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            getActivity().onBackPressed();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("ID",id);
                return map;
            }
        };

        delete.setShouldCache(false);
        delete.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(delete);
    }

   /* private void TimePickerCall() {
        java.util.Calendar now = java.util.Calendar.getInstance();
        TimePickerDialog tpd = TimePickerDialog.newInstance(
                WeeklyScheduleDetail1.this,
                now.get(java.util.Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false
        );
        tpd.show(getActivity().getFragmentManager(), "h");
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {


        String smin = String.valueOf(minute);
        String emin = String.valueOf(minuteEnd);

        if (hourOfDay == 0 ){
            hourOfDay = 12;
        }if (hourOfDayEnd == 0 ){
            hourOfDayEnd = 12;
        }if (smin.equals("0")){
            smin = "00";
        }if (emin.equals("0")){
            emin = "00";
        }


        if (hourOfDay < hourOfDayEnd){
            startHour.setText(hourOfDay+":"+smin);
            result.put(WEEKSTARTHOURKEY , startHour.getText().toString());
            endHour.setText(hourOfDayEnd+":"+emin);
            result.put(WEEKENDHOURKEY , endHour.getText().toString());
        }else {
            Toast.makeText(getContext(), "ساعت شروع بزرگتر از ساعت پایان است.", Toast.LENGTH_SHORT).show();
        }


    }*/
}
