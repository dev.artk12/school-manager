package com.manageschool;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.file.attribute.GroupPrincipal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class LevelCheck extends DialogFragment {


    AppCompatTextView Discrip;
    String one = "آیا میخواهید این دانش آموز بر اساس سن به";
    String two = "    ";
    String three = "<font color='#8B0000'>"+"<b>"+ two +"</b>"+"</font>";
    String four = "انتقال یابد.";
    private static String NAMEKEY = "NameKey";
    private static String GROUPIDKEY = "GRoupID";
    private static String STUDENTKEY = "StudentKey";

    String GroupID , Name , StudentID;
    AppCompatButton Yes , No;
    StringRequest request;
    RequestQueue queue;
    String YESURL = "http://192.168.42.210/mysite/Rschool/NewStudentLevelCheck.php";


    public static LevelCheck newInstance(String Name , String GroupID , String StudentID) {
        
        Bundle args = new Bundle();
        
        LevelCheck fragment = new LevelCheck();
        args.putString(NAMEKEY,Name);
        args.putString(GROUPIDKEY , GroupID);
        args.putString(STUDENTKEY , StudentID);
        fragment.setArguments(args);

        return fragment;
    }


    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_level_check, container, false);
       Yes = (AppCompatButton) view.findViewById(R.id.positive);
       No = (AppCompatButton) view.findViewById(R.id.nagetive);
       Discrip = (AppCompatTextView) view.findViewById(R.id.descrip);

        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        queue = Volley.newRequestQueue(getContext());
       
        Name = getArguments().getString(NAMEKEY);
        GroupID = getArguments().getString(GROUPIDKEY);
        StudentID = getArguments().getString(STUDENTKEY);

        if (Name != null && GroupID != null) {
            two = " " + Name + " ";
            String three = "<font color='#8B0000'>" + "<b>" + two + "</b>" + "</font>";
            String four = "انتقال یابد؟";
            String five = one + three + four;
            Discrip.setText(Html.fromHtml(five));

            Yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    YesRequest();
                }
            });

            No.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LevelCheck.this.dismiss();
                }
            });

        }

        return view;
    }

    private void YesRequest() {

        request = new StringRequest(Request.Method.POST, YESURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("LevelCheckResponse" , response);

                LevelCheck.this.dismiss();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LevelCheck" , error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("StudentID" , StudentID);
                map.put("GroupID" , GroupID);

                return map;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);
    }


}
