package com.manageschool;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;

public class TypeWriter extends android.support.v7.widget.AppCompatTextView {

    private CharSequence mText;
    private int mIndex;
    private long mDelay = 150;//Default 150ms delay
    private ArrayList<String> message;
    int count = 0;

    public void setReaped(boolean reaped) {
        isReaped = reaped;
    }

    boolean isReaped;




    public TypeWriter(Context context) {
        super(context);
    }

    public TypeWriter(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private Handler mHandler = new Handler();
    private Runnable characterAdder = new Runnable() {
        @Override
        public void run() {


            if (mText != null){
                setText(mText.subSequence(0, mIndex++));
                if(mIndex <= mText.length()) {
                    mHandler.postDelayed(characterAdder, mDelay);

                }if (mIndex == mText.length() ){
                    if (isReaped){
                        mIndex = 0 ;
                    }
                }
            }else if (message != null){


                    setText(message.get(count).subSequence(0, mIndex++));

                    if(mIndex <= message.get(count).length()) {
                        mHandler.postDelayed(characterAdder, mDelay);

                    }if (mIndex == message.get(count).length() ){
                        if (count == 7){


                        } else {
                                count++;
                                mIndex = 0 ;
                        }
                    }
            }
        }
    };

    public void animateText(CharSequence text) {
        mText = text;
        mIndex = 0;


            setText("");
            mHandler.removeCallbacks(characterAdder);
            mHandler.postDelayed(characterAdder, mDelay);

    }

    public void animateText(ArrayList<String> list) {
        message = list;
        mIndex = 0;


        setText("");
        mHandler.removeCallbacks(characterAdder);
        mHandler.postDelayed(characterAdder, mDelay);

    }

    public void setCharacterDelay(long millis) {
        mDelay = millis;
    }

}
