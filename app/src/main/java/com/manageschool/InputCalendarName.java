package com.manageschool;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class InputCalendarName extends DialogFragment implements TextWatcher {




    AppCompatEditText Start, End;
    AppCompatButton done;
    RequestQueue queue ;
    StringRequest request;
    String url = "http://192.168.42.210/mysite/Rschool/checkCalendar.php";


    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.inputcalendarnamedialog, container , false);

        Start = (AppCompatEditText) view.findViewById(R.id.start);
        End = (AppCompatEditText) view.findViewById(R.id.end);
        done = (AppCompatButton) view.findViewById(R.id.done);

        queue = Volley.newRequestQueue(getContext());



        Start.addTextChangedListener(this);
        End.addTextChangedListener(this);


        return view;
    }


    private void Upload(final String start , final String end){

        String Insert = "http://192.168.42.210/mysite/Rschool/InsertStartEndCalendar.php";

        request = new StringRequest(Request.Method.POST, Insert, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("dddddddddd",response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("dddddddddd", error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("start" , start);
                map.put("end", end);
                return map;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!Start.getText().toString().isEmpty() && !End.getText().toString().isEmpty() && Start.getText().toString().length() == 10 && End.getText().toString().length() == 10  ){

                    String datestart = Start.getText().toString();
                    datestart = datestart.replace("/","");
                    datestart = datestart.replace("-","");
                    datestart = datestart.replace(".","");

                    SimpleDateFormat Year = new SimpleDateFormat("yyyy", Locale.ENGLISH);
                    String yearString = Year.format(new Date());

                    SimpleDateFormat month = new SimpleDateFormat("MM", Locale.ENGLISH);
                    String monthString = month.format(new Date());


                    SimpleDateFormat day = new SimpleDateFormat("dd", Locale.ENGLISH);
                    String dayString = day.format(new Date());

                    PersianDate pdate = new PersianDate();
                    PersianDateFormat format = new PersianDateFormat();
                    format.format(pdate);

                    int[] jalili = pdate.toJalali(Integer.valueOf(yearString) , Integer.valueOf(monthString) , Integer.valueOf(dayString));

                    int yearint = jalili[0];
                    int monthint = jalili[1];
                    int dayint = jalili[2];

                    String monthstr = String.valueOf(monthint);
                    String daystr = String.valueOf(dayint);
                    String yearstr = String.valueOf(yearint);
                    if (monthstr.length() == 1){
                        monthstr = "0"+monthstr;
                    }if (daystr.length() == 1){
                        daystr = "0"+daystr;
                    }if (yearstr.length() == 2){
                        yearstr = "13"+yearstr;
                    }

                    int today = Integer.valueOf(yearstr+monthstr+daystr);

                    int start = Integer.valueOf(datestart);

                    if (today > start){
                        Toast.makeText(getContext(), "تاریخ شروع ترم باید بزرگتر از تاریخ امروز باشد.", Toast.LENGTH_SHORT).show();
                    }else  {
                        Upload(Start.getText().toString() , End.getText().toString());
                        InputCalendarName.this.dismiss();
                    }
                }else {
                    Toast.makeText(getContext(), "لطفا تمامی فیلدها را پر کنید.", Toast.LENGTH_SHORT).show();


                }
            }
        });

    }



}
