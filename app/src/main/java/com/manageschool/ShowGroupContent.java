package com.manageschool;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class ShowGroupContent extends Fragment {

    private static String LOCKKRY = "LockKey";
    private static String TITLEKEY = "Title";
    private static String GROUPIDEKEY = "GroupID";

    RelativeLayout groupsrview;
    AppCompatImageButton add ,refresh ;
    AppCompatTextView title;
    StringRequest request;
    RecyclerView students;
    RequestQueue queue;
    String url = "http://192.168.42.210/mysite/Rschool/getGroupsContent.php";
    private String getNeatURL = "http://192.168.42.210/mysite/Rschool/getNeat.php";

    ArrayList<Student> list;
    String name , GroupID;
    FloatingActionButton floatbtn;
    boolean newGroup;
    JSONArray StudentIds;
    boolean lock;
    FragmentCallBack callBack;
    ArrayList<Neat> neats;
    ArrayList<String> GroupIDs;
    ArrayList<String> Groupname;
    ArrayList<String> End;
    ArrayList<String> Start;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (FragmentCallBack) context;
    }

    public static ShowGroupContent newInstance(String title , String GroupID , boolean Lock) {

        Bundle args = new Bundle();

        ShowGroupContent fragment = new ShowGroupContent();
        args.putString(TITLEKEY , title);
        args.putString(GROUPIDEKEY , GroupID);
        args.putBoolean(LOCKKRY , Lock);
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.student_fragemnt,container , false);
        groupsrview = (RelativeLayout) view.findViewById(R.id.groupparent);
        add = (AppCompatImageButton) view.findViewById(R.id.newstudent);
        add.setVisibility(View.GONE);
        add = (AppCompatImageButton) view.findViewById(R.id.add);
        add.setVisibility(View.VISIBLE);
        refresh = (AppCompatImageButton) view.findViewById(R.id.refresh);
        title = (AppCompatTextView) view.findViewById(R.id.title);
        students = (RecyclerView) view.findViewById(R.id.students);
        floatbtn = (FloatingActionButton) view.findViewById(R.id.floatingbtn);

        queue = Volley.newRequestQueue(getContext());
        StudentIds = new JSONArray();
        lock = getArguments().getBoolean(LOCKKRY);






        GroupID = getArguments().getString(GROUPIDEKEY);
        if (GroupID.equals("0")){
            GroupIDs = new ArrayList<>();
            Groupname = new ArrayList<>();
            neats = new ArrayList<>();
            Start = new ArrayList<>();
            End = new ArrayList<>();
            floatbtn.hide();

            Load();
        }
        callBack.NewStudentBTN(GroupID ,1,add);
        floatbtn.setImageResource(R.drawable.ic_mode_edit_black_24dp);
        floatbtn.getDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        name = getArguments().getString(TITLEKEY);
        floatbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.addToBackStack("myscreen");
                    transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    InputStudentGroup fragment = InputStudentGroup.newInstance(true , StudentIds.toString(), GroupID,name , lock);
                    transaction.replace(android.R.id.content, fragment );
                    transaction.commit();

            }
        });
        groupsrview.setVisibility(View.GONE);
        title.setText(name);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupstudents();
    }

    private void Load(){
        StringRequest request = new StringRequest(Request.Method.POST, getNeatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.equals("[]")){

                }if (response != null){
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        Neat neat;

                        for (int i = 0; i < jsonArray.length() ; i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            neat = new Neat();
                            neat.setGroupID(object.getString("GroupID"));
                            neat.setGroupName(object.getString("GroupName"));
                            neat.setLevelID(object.getString("LevelID"));
                            neat.setLevelName(object.getString("LevelName"));
                            neat.setStartYear(object.getString("Start"));
                            neat.setEndYear(object.getString("End"));
                            GroupIDs.add(object.getString("GroupID"));
                            Groupname.add(object.getString("GroupName"));
                            Start.add(object.getString("Start"));
                            End.add(object.getString("End"));

                            neats.add(neat);

                        }


                    }catch (JSONException e){
                        Log.e("ddddddddddd",e.getLocalizedMessage());
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void setupstudents() {
        final ArrayList<String> color = new ArrayList<>();
        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("Ddddddd",response);

                if (response != null){

                    Student student;

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        list = new ArrayList<>();

                        for (int i = 0; i < jsonArray.length() ; i++) {
                            student = new Student();
                            JSONObject IDs = new JSONObject();
                            JSONObject object = jsonArray.getJSONObject(i);

                            student.setID(object.getString("ID"));
                            IDs.put("ID",student.getID());
                            student.setNationalID(object.getString("nationalID"));
                            student.setName(object.getString("FirstName"));
                            student.setLastName(object.getString("LastName"));
                            student.setFatherName(object.getString("FatherName"));
                            student.setAccepted(object.getString("accepted"));

                            StudentIds.put(IDs);
                            String birth = CompareCalensars(object.getString("Birth"));
                            student.setBirthYear(birth);
                            color.add(object.getString("color"));

                            list.add(student);
                        }

                        students.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        students.setItemAnimator(new DefaultItemAnimator());
                        StudentRecyclerView adapter = new StudentRecyclerView(getContext(),list,color);
                        students.setAdapter(adapter);


                    } catch (JSONException e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        Log.e("ddddddddddddd",e.getMessage());
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("GroupID",GroupID);
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);

        queue.add(request);
    }
    private String CompareCalensars(String studentbirth ) {


        int start = Integer.valueOf(studentbirth);

        SimpleDateFormat Year = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        String yearString = Year.format(new Date());

        SimpleDateFormat month = new SimpleDateFormat("MM", Locale.ENGLISH);
        String monthString = month.format(new Date());


        SimpleDateFormat day = new SimpleDateFormat("dd", Locale.ENGLISH);
        String dayString = day.format(new Date());

        PersianDate pdate = new PersianDate();
        PersianDateFormat format = new PersianDateFormat();
        format.format(pdate);

        int[] jalili = pdate.toJalali(Integer.valueOf(yearString) , Integer.valueOf(monthString) , Integer.valueOf(dayString));

        int nowyear = jalili[0];


        int birth = nowyear - start;

        return String.valueOf(birth);

    }
    interface VollayCallBack{
        void Respons(String response);
    }
}
