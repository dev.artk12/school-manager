package com.manageschool;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class StudiDetailActivity extends AppCompatActivity {


    AppCompatEditText EducationCode, EducationName;
    RequestQueue queue ;
    StringRequest request , levelrequest  , onelevelrequest  , upload , deleteRequest;
    RecyclerView recyclerView;
    AppCompatImageButton done , delete;
    AppCompatTextView leveltxt;
    Animation in , out;
    String GETURL = "http://192.168.42.210/mysite/Rschool/getCurrnetLevel.php";
    Map<String , String> map;
    private String IDKEY = "ID";
    private String STUDIIDKEY ="StudiID";
    private String STUDINAMEKEY ="StudiNAME";
    private String STUDICODEKEY ="StudiCODE";
    private String STUDILEVELKEY ="StudiLEVELID";
    String ID , Name , Code , Level ;
    ArrayList<MasterClassRoom> classRooms;
    SwitchCompat switchCompat;
    ArrayList<Level> levels;
    private String ClassRoomURL = "http://192.168.42.210/mysite/Rschool/getCurrentStudiClassRoom.php";
    private String GETAllURL = "http://192.168.42.210/mysite/Rschool/getLevel.php";
    private String UpLoadURL = "http://192.168.42.210/mysite/Rschool/UpdateStudi.php";
    private String DELETEURL = "http://192.168.42.210/mysite/Rschool/DeletStudi.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studi_detail);

        EducationCode = (AppCompatEditText) findViewById(R.id.studiCode);
        EducationName = (AppCompatEditText) findViewById(R.id.studiname);
        done = (AppCompatImageButton) findViewById(R.id.done);
        delete = (AppCompatImageButton) findViewById(R.id.delet);
        leveltxt = (AppCompatTextView) findViewById(R.id.leveltxt);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        in = AnimationUtils.loadAnimation(this , R.anim.cameragalleryin);
        out = AnimationUtils.loadAnimation(this , R.anim.cameragalleryout);
        switchCompat = (SwitchCompat) findViewById(R.id.EditMode);
        map = new HashMap<>();

        Bundle bundle = getIntent().getExtras();

        ID = bundle.getString(STUDIIDKEY);
        Name = bundle.getString(STUDINAMEKEY);
        Code = bundle.getString(STUDICODEKEY);
        Level = bundle.getString(STUDILEVELKEY);

        if (ID != null || Name != null || Code != null || Level != null) {

            switchCompat.setVisibility(View.VISIBLE);
            done.setVisibility(View.INVISIBLE);
            delete.setVisibility(View.INVISIBLE);

            EducationCode.setEnabled(false);
            EducationName.setEnabled(false);
            leveltxt.setEnabled(false);
            queue = Volley.newRequestQueue(this);

            EducationName.setText(Name);
            EducationCode.setText(Code);
            LoadLevel(Level);
            LoadClassRooms(ID);

            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String LevelID = Level;

                    if (map.get(IDKEY) != null){
                        if (map.get(IDKEY).length() >= 1){
                            LevelID = map.get(IDKEY);
                        }
                    }else {
                        LevelID = Level;
                    }

                    if (EducationCode.getText().toString().length() < 2){
                        Toast.makeText(StudiDetailActivity.this, "کد درس معتبر نمیباشد.", Toast.LENGTH_SHORT).show();
                    }else if (EducationName.getText().toString().length() < 2){
                        Toast.makeText(StudiDetailActivity.this, "نام درس معتبر نمیباشد.", Toast.LENGTH_SHORT).show();
                    }else {
                        Upload(ID , EducationName.getText().toString() , EducationCode.getText().toString() , LevelID);

                    }
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteRQ(ID);
                }
            });

            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        LoadLevels(Level);
                        done.setVisibility(View.VISIBLE);
                        done.startAnimation(in);
                        delete.setVisibility(View.VISIBLE);
                        delete.startAnimation(in);
                        EducationCode.setEnabled(true);
                        EducationName.setEnabled(true);
                        leveltxt.setEnabled(true);
                    }else {
                        done.setVisibility(View.INVISIBLE);
                        done.startAnimation(out);
                        delete.setVisibility(View.INVISIBLE);
                        delete.startAnimation(out);
                        EducationCode.setEnabled(false);
                        EducationName.setEnabled(false);
                        leveltxt.setEnabled(false);
                        LoadClassRooms(ID);
                    }
                }
            });


        }else {
            Toast.makeText(this, "Somthing Wrong", Toast.LENGTH_SHORT).show();
        }
    }

    private void Upload(final String ID,final String name, final String code, final String levelID) {

        upload = new StringRequest(Request.Method.POST, UpLoadURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                done.setVisibility(View.INVISIBLE);
                done.startAnimation(out);
                delete.setVisibility(View.INVISIBLE);
                delete.startAnimation(out);
                EducationCode.setEnabled(false);
                EducationName.setEnabled(false);
                leveltxt.setEnabled(false);
                switchCompat.setChecked(false);
                classRooms = null;
                LoadClassRooms(ID);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("sssssssss",error.getLocalizedMessage());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("ID" , ID);
                map.put("Name" , name);
                map.put("Code" , code);
                map.put("LevelID" , levelID);

                return map;
            }
        };

        upload.setShouldCache(false);
        upload.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(upload);

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void deleteRQ(final String ID){

        deleteRequest = new StringRequest(Request.Method.POST, DELETEURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("resssponse",response);
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("resssponse",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("ID",ID);
                return map;
            }
        };

        deleteRequest.setShouldCache(false);
        deleteRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(deleteRequest);

    }

    private void LoadLevels(final String ID) {

        recyclerView.setVisibility(View.INVISIBLE);
        recyclerView.startAnimation(out);

        if (levels == null){
            levelrequest = new StringRequest(Request.Method.POST, GETAllURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Level level ;
                        levels = new ArrayList<>();
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            level = new Level();
                            JSONObject object = jsonArray.getJSONObject(i);
                            level.setID(object.getString("ID"));
                            level.setName(object.getString("Name"));
                            level.setClassRoomName(object.getString("ClassRoom"));

                            levels.add(level);
                        }
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.startAnimation(in);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setLayoutManager(new LinearLayoutManager(StudiDetailActivity.this,LinearLayoutManager.VERTICAL,false));
                        LevelAdapter adapter = new LevelAdapter(levels , StudiDetailActivity.this, false);
                        recyclerView.setAdapter(adapter);
                        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(StudiDetailActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                map.put(IDKEY,levels.get(position).getID());
                                leveltxt.setText(levels.get(position).getName());
                            }
                        }));

                    }catch (JSONException e){
                        Log.e("eeeeee",e.getLocalizedMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String , String > map = new HashMap<>();
                    map.put("LevelID",ID);
                    return map;
                }
            };

            levelrequest.setShouldCache(false);
            levelrequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(levelrequest);
        }else {
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.startAnimation(in);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setLayoutManager(new LinearLayoutManager(StudiDetailActivity.this,LinearLayoutManager.VERTICAL,false));
            LevelAdapter adapter = new LevelAdapter(levels , StudiDetailActivity.this, true);
            recyclerView.setAdapter(adapter);
            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(StudiDetailActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    map.put(IDKEY,levels.get(position).getID());
                    leveltxt.setText(levels.get(position).getName());
                }
            }));
        }
    }
    private void LoadLevel(final String ID) {

        Log.e("ffffffffdddddd",ID);
           onelevelrequest = new StringRequest(Request.Method.POST, GETURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {



                    try {
                        Level level;

                        JSONArray jsonArray = new JSONArray(response);

                        level = new Level();
                        JSONObject object = jsonArray.getJSONObject(0);
                        level.setID(object.getString("ID"));
                        level.setName(object.getString("Name"));
                        level.setClassRoomName(object.getString("ClassRoom"));

                        leveltxt.setText(level.getName());


                    } catch (JSONException e) {
                        Log.e("eeeeee", e.getLocalizedMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                        Log.e("Eeeee",error.getLocalizedMessage());
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> map = new HashMap<>();
                    map.put("LevelID", ID);
                    return map;
                }
            };

            onelevelrequest.setShouldCache(false);
            onelevelrequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(onelevelrequest);


    }
    private void LoadClassRooms(final String ID){


        recyclerView.setVisibility(View.INVISIBLE);
        recyclerView.startAnimation(out);

        if (classRooms == null){
            request = new StringRequest(Request.Method.POST, ClassRoomURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        MasterClassRoom classRoom;
                        JSONObject object;
                        classRooms = new ArrayList<>();

                        for (int i = 0; i < jsonArray.length() ; i++) {

                            object = jsonArray.getJSONObject(i);
                            classRoom = new MasterClassRoom();

                            classRoom.setID(object.getString("ID"));
                            classRoom.setLesson(object.getString("LessonID"));
                            classRoom.setMaster(object.getString("MasterID"));
                            classRoom.setStartHour(object.getString("StartHour"));
                            classRoom.setEndHour(object.getString("EndHour"));
                            classRoom.setWeekDay(object.getString("WeekDay"));
                            classRoom.setLevelName(object.getString("LevelName"));
                            classRoom.setClassRoomName(object.getString("LevelClassName"));
                            classRoom.setStartQuran(object.getString("StartQuran"));
                            classRoom.setEndQuran(object.getString("EndQuran"));


                            classRooms.add(classRoom);
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(StudiDetailActivity.this,LinearLayoutManager.VERTICAL , false));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        WeeklyscheduleAdapter adapter = new WeeklyscheduleAdapter(0,false,classRooms , StudiDetailActivity.this);
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.startAnimation(in);
                        recyclerView.setAdapter(adapter);

                    } catch (JSONException e) {
                        Log.e("errrrrrrror",e.getLocalizedMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ddddddddddd",error.getLocalizedMessage());
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map = new HashMap<>();
                    map.put("LessonID",ID);
                    return map;
                }
            };

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(request);
        }else {
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.startAnimation(in);
            recyclerView.setLayoutManager(new LinearLayoutManager(StudiDetailActivity.this,LinearLayoutManager.VERTICAL , false));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            WeeklyscheduleAdapter adapter = new WeeklyscheduleAdapter(0,false,classRooms , StudiDetailActivity.this);
            recyclerView.setAdapter(adapter);
        }
    }
}
