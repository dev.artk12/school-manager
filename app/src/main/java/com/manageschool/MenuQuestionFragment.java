package com.manageschool;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.ReferenceQueue;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class MenuQuestionFragment extends Fragment {


    FloatingActionButton floatingActionButton ;
    RecyclerView recyclerView;
    ArrayList<Question> questions;
    StringRequest request;
    RequestQueue queue;
    String QURL = "http://192.168.42.210/mysite/Rschool/getAllQuestion.php";
    AppCompatImageButton refresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question, container, false);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.floatingbtn);
        refresh = (AppCompatImageButton) view.findViewById(R.id.refresh);

        recyclerView = (RecyclerView) view.findViewById(R.id.questions);
        queue = Volley.newRequestQueue(getContext());
        questions = new ArrayList<>();

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                InputQuestion question = InputQuestion.newInstance("0",false , "0","0","0","0" , "0","0","0");
                transaction.replace(android.R.id.content, question);
                transaction.commit();
            }
        });



        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Load();
    }

    private void Load() {
        request = new StringRequest(Request.Method.POST, QURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("response",response);
                questions = new ArrayList<>();

                try {
                    JSONArray array = new JSONArray(response);

                    for (int i = 0; i < array.length() ; i++) {
                        JSONObject object = array.getJSONObject(i);
                        Question question = new Question();

                        question.setID(object.getString("ID"));
                        question.setQuestion(object.getString("Question"));
                        question.setFirst(object.getString("First"));
                        question.setSecend(object.getString("Secend"));
                        question.setThird(object.getString("Third"));
                        question.setFour(object.getString("Four"));
                        question.setLevel(object.getString("GroupName"));
                        question.setLevelID(object.getString("GroupID"));


                        questions.add(question);
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext() , LinearLayoutManager.VERTICAL , false));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    QuestionAdapter adapter = new QuestionAdapter(true , getContext() , questions , (AppCompatActivity) getActivity());
                    recyclerView.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errrrrrrrror",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);



    }


}
