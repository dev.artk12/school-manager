package com.manageschool;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class MenuWeeklyScheduleFragment extends Fragment {

    FloatingActionButton add;
    RequestQueue queue;
    StringRequest load;
    ArrayList<MasterClassRoom> classRooms;
    AppCompatImageButton refresh;
    RecyclerView recyclerView;
    String LadClassroomsURL = "http://192.168.42.210/mysite/Rschool/getMasterClassRoom.php";
    Map<String , String > result;
    private String IDCLASSKEY = "IDClass";
    Internet internet;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weekly_schedule, container, false);
        add = (FloatingActionButton) view.findViewById(R.id.floatingbtn);
        refresh = (AppCompatImageButton) view.findViewById(R.id.refresh);
        recyclerView = (RecyclerView) view.findViewById(R.id.weelklyschedule);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
            }
        });
        internet = new Internet(getContext());

        classRooms = new ArrayList<>();

        queue = Volley.newRequestQueue(getContext());
        result = new HashMap<>();


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                String s = " ";
                inputweeklyschedle fragment = inputweeklyschedle.newInstance(false ,result.get(IDCLASSKEY) , s , s , s , s ,s , s , s);
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                transaction.replace(android.R.id.content, fragment);
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        classRooms = new ArrayList<>();
        Load();
    }

    private void Load(){

        load = new StringRequest(Request.Method.POST, LadClassroomsURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                Log.e("ccccccc",response);

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    MasterClassRoom classRoom;
                    JSONObject object;


                    for (int i = 0; i < jsonArray.length() ; i++) {

                        object = jsonArray.getJSONObject(i);
                        classRoom = new MasterClassRoom();

                        classRoom.setID(object.getString("ID"));
                        classRoom.setLesson(object.getString("LessonID"));
                        classRoom.setMaster(object.getString("MasterID"));
                        classRoom.setStartHour(object.getString("StartHour"));
                        classRoom.setEndHour(object.getString("EndHour"));
                        classRoom.setWeekDay(object.getString("WeekDay"));
                        classRoom.setLevelName(object.getString("LevelName"));
                        classRoom.setClassRoomName(object.getString("LevelClassName"));
                        classRoom.setIDClass(object.getString("IDClass"));
                        classRoom.setStartQuran(object.getString("StartQuran"));
                        classRoom.setEndQuran(object.getString("EndQuran"));


                        classRooms.add(classRoom);
                    }
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    WeeklyscheduleAdapter adapter = new WeeklyscheduleAdapter(0,true,classRooms , getContext());
                    recyclerView.setAdapter(adapter);
                    if (classRooms.size() != 0){
                        result.put(IDCLASSKEY , classRooms.get(classRooms.size() - 1).getIDClass());
                        if (result.get(IDCLASSKEY) == null){
                            result.put(IDCLASSKEY , "0");
                        }
                    }else {
                        result.put(IDCLASSKEY , "0");
                    }


                } catch (JSONException e) {
                    Log.e("errrrrrrror",e.getLocalizedMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errrrrror" , error.getLocalizedMessage());
            }
        });

        load.setShouldCache(false);
        load.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(load);


    }

}
