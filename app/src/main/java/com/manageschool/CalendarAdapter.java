package com.manageschool;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.ViewHolderCalendar> {


    ArrayList<Calendar> calendars;
    Context context;

    CalendarAdapter(Context context , ArrayList<Calendar> calendars ){

        if (calendars == null){
            calendars = new ArrayList<>();
        }
        this.calendars = calendars;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolderCalendar onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.calendaritem , viewGroup , false);
        ViewHolderCalendar viewholder = new ViewHolderCalendar(view);


        return viewholder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderCalendar viewHolder, int i) {

        viewHolder.start.setText(calendars.get(i).getStart());
        viewHolder.end.setText(calendars.get(i).getEnd());

    }

    @Override
    public int getItemCount() {
        return calendars.size();
    }

    public class ViewHolderCalendar extends RecyclerView.ViewHolder{

        AppCompatTextView start , end;


        public ViewHolderCalendar(View itemView){
            super(itemView);
            start = (AppCompatTextView) itemView.findViewById(R.id.start);
            end = (AppCompatTextView) itemView.findViewById(R.id.end);
        }



    }


}
