package com.manageschool;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class MenuStudiFragemnt extends Fragment {

    FloatingActionButton add;
    RecyclerView recyclerView;
    RequestQueue queue;
    StringRequest request;
    String url = "http://192.168.42.210/mysite/Rschool/getStudi.php";
    StudiAdapter adapter;
    ArrayList<Studi> studis;
    AppCompatImageButton refresh;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_studi_fragemnt , container , false);
        add = (FloatingActionButton) view.findViewById(R.id.floatingbtn);
        recyclerView = (RecyclerView) view.findViewById(R.id.studis);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext() , LinearLayoutManager.VERTICAL , false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        refresh = (AppCompatImageButton) view.findViewById(R.id.refresh);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
            }
        });

        queue = Volley.newRequestQueue(getContext());

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                transaction.replace(android.R.id.content, new InputStudiName());
                transaction.commit();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Download(new VolleyResultCallBack() {
            @Override
            public void CallBack(String Respons) {

                Studi studi ;
                if (Respons != null){
                    studis = new ArrayList<>();

                    try {
                        JSONArray jsonArray = new JSONArray(Respons);
                        for (int i = 0; i < jsonArray.length() ; i++) {
                            studi = new Studi();
                            JSONObject object = jsonArray.getJSONObject(i);

                            studi.setID(object.getString("ID"));
                            studi.setName(object.getString("Name"));
                            studi.setCode(object.getString("LessonCode"));
                            studi.setLevelCode(object.getString("LevelID"));


                            studis.add(studi);
                        }

                        adapter = new StudiAdapter(studis , getContext(),true );
                        recyclerView.setAdapter(adapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

            }
        });
    }

    private void Download (final VolleyResultCallBack callBack){

        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callBack.CallBack(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Dsdvsdvsedvasedvas" , error.getMessage());
            }
        });

        request.setShouldCache(false);

        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    interface VolleyResultCallBack{
        void CallBack(String Respons);
    }
}
