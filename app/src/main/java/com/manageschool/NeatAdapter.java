package com.manageschool;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class NeatAdapter extends RecyclerView.Adapter<NeatAdapter.ViewHolder>{

    ArrayList<Neat> neats;
    Context context;
    AppCompatActivity activity;

    public NeatAdapter(Context context , ArrayList<Neat> neats , AppCompatActivity activity){

        if (neats == null){
            neats = new ArrayList<>();
        }
        this.neats = neats;
        this.context = context;
        this.activity = activity;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.neatitem , viewGroup , false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.name.setText(neats.get(i).getLevelName());

        if (i == neats.size()-1){
            viewHolder.arrow.setVisibility(View.GONE);
            viewHolder.line.setVisibility(View.GONE);
        }

        viewHolder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(R.anim.slidedown, R.anim.slideup);
                NeatDetail_fragment fragment = NeatDetail_fragment.newInstance(neats.get(i).getLevelID() , neats.get(i).getGroupID());
                transaction.replace(android.R.id.content,fragment);
                transaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return neats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView name;
        View line;
        AppCompatImageButton arrow;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = (AppCompatTextView) itemView.findViewById(R.id.name);
            line = (View) itemView.findViewById(R.id.line);
            arrow = (AppCompatImageButton) itemView.findViewById(R.id.arrow);
        }
    }
}
