package com.manageschool;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


public class NeatStudent extends Fragment implements InputStudentAdapterCheckCallBack {


    private static String STATYEARKEY = "StartYear";
    private static String ENDYEARKEY = "EndYear";
    private static String NAMEGROUPID = "Groupname" ;
    private static String GROUPIDKEY = "GroupID1";
    private static String STUDENT = "ST1";

    String url = "http://192.168.42.210/mysite/Rschool/getGroupsContent.php";
    String Update = "http://192.168.42.210/mysite/Rschool/UpdateGroupNeat.php";



    StringRequest request1 , request2  ;
    AppCompatTextView title;
    RequestQueue queue;
    AppCompatImageButton done;
    RecyclerView recyclerView ;
    ArrayList<Student> list;
    ArrayList<String> GroupID , Names , Start , End;
    private int COUNTER = 0;
    Map<String , String > st;

    public static NeatStudent newInstance(ArrayList<String> neatsID , ArrayList<String> name , ArrayList<String> StartYear ,ArrayList<String> EndYear) {

        Bundle args = new Bundle();

        NeatStudent fragment = new NeatStudent();

        args.putStringArrayList(GROUPIDKEY , neatsID);
        args.putStringArrayList(NAMEGROUPID , name);
        args.putStringArrayList(STATYEARKEY,StartYear);
        args.putStringArrayList(ENDYEARKEY,EndYear);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_neat_student, container, false) ;
        recyclerView = (RecyclerView) view.findViewById(R.id.student);
        title = (AppCompatTextView) view.findViewById(R.id.title);
        done = (AppCompatImageButton) view.findViewById(R.id.done);
        Names = getArguments().getStringArrayList(NAMEGROUPID);
        st = new HashMap<>();

        GroupID = getArguments().getStringArrayList(GROUPIDKEY);
        queue = Volley.newRequestQueue(getContext());
        list = new ArrayList<>();
        Start = getArguments().getStringArrayList(STATYEARKEY);
        End = getArguments().getStringArrayList(ENDYEARKEY);

        if (Names != null){
            if (Names.size() != 0){
                title.setText("انتقال از ");
                title.append(Names.get(COUNTER));
                title.append(" به ");
                title.append(Names.get(COUNTER+1));
                LoadGroup(GroupID.get(COUNTER));
            }else {
                title.setText("گروه حذف شده یا مشکلی پیش اومده");
            }
        }else {
            title.setText("گروه حذف شده یا مشکلی پیش اومده");
        }

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                COUNTER++;

                if (COUNTER == GroupID.size()){
                    Toast.makeText(getContext(), "Done", Toast.LENGTH_SHORT).show();
                    UPLoadGroup( "1", st.get(STUDENT) , GroupID.get(COUNTER -1) );
                }else {
                            title.setText("انتقال از ");
                            title.append(Names.get(COUNTER));
                            title.append(" به ");
                            if (COUNTER == GroupID.size() - 1){
                                title.append("فارغ التحصیل");
                                UPLoadGroup( GroupID.get(COUNTER), st.get(STUDENT) , GroupID.get(COUNTER)  );


                            }else {
                                title.append(Names.get(COUNTER+1));
                                UPLoadGroup( GroupID.get(COUNTER), st.get(STUDENT) , GroupID.get(COUNTER)  );
                                Log.e("dddddddd",st.get(STUDENT));
                            }
                }
            }
        });

        return view;
    }

    private void LoadGroup(final String GroupID1){

        request1 = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null){
                    list = new ArrayList<>();

                    Student student = null;

                    try {
                        JSONArray jsonArray = new JSONArray(response);

                        for (int i = 0; i < jsonArray.length() ; i++) {
                            student = new Student();

                            JSONObject object = jsonArray.getJSONObject(i);

                            student.setID(object.getString("ID"));
                            student.setNationalID(object.getString("nationalID"));
                            student.setName(object.getString("FirstName"));
                            student.setLastName(object.getString("LastName"));
                            student.setFatherName(object.getString("FatherName"));
                            String birth =CompareCalensars(object.getString("Birth"));
                            student.setBirthYear(birth);
                            int end = Integer.valueOf(End.get(COUNTER));
                            int x = Integer.valueOf(birth);
                            if (x == end  ){
                                student.setChecked(true);
                            }else {
                                student.setChecked(false);
                            }

                            list.add(student);
                        }
                        ArrayList<Student> arrayList = new ArrayList<>();
                        for (int i = 0; i < list.size() ; i++) {
                            if (list.get(i).isChecked()){
                                arrayList.add(list.get(i));
                            }

                        }
                        st.put(STUDENT,ArrayListToJsonString(arrayList));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        InputStudentAdapter adapter = new InputStudentAdapter(getContext(),list,NeatStudent.this , 1);
                        recyclerView.setAdapter(adapter);


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("GroupID" , GroupID1);
                return map;
            }
        };

        request1.setShouldCache(false);
        request1.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request1);

    }

    private void UPLoadGroup(final String GroupID ,final String students , final String getstudentgroup ){

        request2 = new StringRequest(Request.Method.POST, Update, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (COUNTER == Names.size()){
                    getActivity().onBackPressed();

                }

                if (response != null ){
                    list = new ArrayList<>();

                    Student student = null;

                    try {
                        JSONArray jsonArray = new JSONArray(response);

                        for (int i = 0; i < jsonArray.length() ; i++) {
                            student = new Student();

                            JSONObject object = jsonArray.getJSONObject(i);

                            student.setID(object.getString("ID"));
                            student.setNationalID(object.getString("nationalID"));
                            student.setName(object.getString("FirstName"));
                            student.setLastName(object.getString("LastName"));
                            student.setFatherName(object.getString("FatherName"));
                            String birth = CompareCalensars(object.getString("Birth"));
                            student.setBirthYear(birth);


                            if (COUNTER <= End.size() -1) {
                                int end = Integer.valueOf(End.get(COUNTER));

                                int x = Integer.valueOf(birth);
                                if (x == end) {
                                    student.setChecked(true);
                                } else {
                                    student.setChecked(false);
                                }
                            }

                            list.add(student);
                        }

                        ArrayList<Student> arrayList = new ArrayList<>();
                        for (int i = 0; i < list.size() ; i++) {
                            if (list.get(i).isChecked()){
                                arrayList.add(list.get(i));
                            }

                        }
                        st.put(STUDENT,ArrayListToJsonString(arrayList));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    InputStudentAdapter adapter = new InputStudentAdapter(getContext(),list,NeatStudent.this , 1);
                    recyclerView.setAdapter(adapter);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("GroupID" , GroupID);
                map.put("students" , students);
                map.put("getstudentgroup" , getstudentgroup);

                return map;
            }
        };

        request2.setShouldCache(false);
        request2.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request2);

    }
    private String CompareCalensars(String studentbirth ) {


        int start = Integer.valueOf(studentbirth);

        SimpleDateFormat Year = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        String yearString = Year.format(new Date());

        SimpleDateFormat month = new SimpleDateFormat("MM", Locale.ENGLISH);
        String monthString = month.format(new Date());


        SimpleDateFormat day = new SimpleDateFormat("dd", Locale.ENGLISH);
        String dayString = day.format(new Date());

        PersianDate pdate = new PersianDate();
        PersianDateFormat format = new PersianDateFormat();
        format.format(pdate);

        int[] jalili = pdate.toJalali(Integer.valueOf(yearString) , Integer.valueOf(monthString) , Integer.valueOf(dayString));

        int nowyear = jalili[0];


        int birth = nowyear - start;

        return String.valueOf(birth);

    }


    @Override
    public void CallBackStudents(ArrayList<Student> students) {
        if (students.size() == 0){
            Toast.makeText(getContext(), "دانش آموزی انتخاب نشده است. ", Toast.LENGTH_SHORT).show();
            students = new ArrayList<>();
            st.put(STUDENT,"[]");
        }else {
            st.put(STUDENT,ArrayListToJsonString(students));
        }
    }
    private String ArrayListToJsonString(ArrayList<Student> student){

        if (student.size() == 0){
            return "[]";
        }else {

                JSONArray jsonArray = new JSONArray();
                try {
                for (int i = 0; i < student.size() ; i++) {
                    JSONObject object = new JSONObject();
                        object.put("ID",student.get(i).getID());
                        jsonArray.put(object);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return jsonArray.toString();
        }
    }
}
