package com.manageschool;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.JsonReader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

public class InputStudentGroup extends Fragment implements CompoundButton.OnCheckedChangeListener , InputStudentAdapterCheckCallBack{

    private static String LOCKKEY = "LockKey";
    RequestQueue queue;
    StringRequest request , insert , update;
    AppCompatImageButton done , delete;
    String url = "http://192.168.42.210/mysite/Rschool/getStudent.php";
    String inserturl = "http://192.168.42.210/mysite/Rschool/InsertGroup.php";
    String updateurl = "http://192.168.42.210/mysite/Rschool/UpdateGroup.php";


    private static String GROUPNEWKEY = "check";


    ArrayList<Student> list;
    RecyclerView recyclerView;
    AppCompatCheckBox blue , green , red , yellow;
    InputStudentAdapter adapter;
    Map<Integer , View> viewMap;
    Map<Integer , ArrayList<Student>> offlinestudent;
    int color = 0;
    AppCompatEditText search , Name;
    private static String GROUPSID = "GroupID";
    private static String STUDENTID = "StudentID";
    private static String NAMEKEY = "NameGroups";
    String StudentIDs , NameString;
    ArrayList<String> IDs;
    String GroupID;
    boolean EditMode , lock;

    public static InputStudentGroup newInstance(boolean newGroup , String StudentIDs, String GroupsID, String Name , boolean Lock) {

        Bundle args = new Bundle();

        InputStudentGroup fragment = new InputStudentGroup();
        if (GroupsID == null){
            args.putString(GROUPSID , "0");
        }else {
            args.putString(GROUPSID , GroupsID);
        }

        args.putBoolean(GROUPNEWKEY , newGroup);
        args.putString(STUDENTID , StudentIDs);
        args.putString(NAMEKEY,Name);
        args.putBoolean(LOCKKEY,Lock);
        fragment.setArguments(args);


        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.inputstudentsgroup, container , false);
        recyclerView = (RecyclerView) view.findViewById(R.id.students);
        done = (AppCompatImageButton) view.findViewById(R.id.done);
        delete = (AppCompatImageButton) view.findViewById(R.id.delete);

        blue = (AppCompatCheckBox) view.findViewById(R.id.blue);
        red = (AppCompatCheckBox) view.findViewById(R.id.red);
        green = (AppCompatCheckBox) view.findViewById(R.id.green);
        yellow = (AppCompatCheckBox) view.findViewById(R.id.yellow);
        search = (AppCompatEditText) view.findViewById(R.id.search);
        Name = (AppCompatEditText) view.findViewById(R.id.name);
        EditMode = getArguments().getBoolean(GROUPNEWKEY);
        StudentIDs = getArguments().getString(STUDENTID);
        NameString = getArguments().getString(NAMEKEY);

        blue.setOnCheckedChangeListener(this);
        red.setOnCheckedChangeListener(this);
        green.setOnCheckedChangeListener(this);
        yellow.setOnCheckedChangeListener(this);
        IDs = new ArrayList<>();

        GroupID = getArguments().getString(GROUPSID);


        if (NameString.equals("فارغ التحصیل") || NameString.equals("ثبت نام")){
            Name.setEnabled(false);
        }




        red.setChecked(false);
        green.setChecked(false);
        yellow.setChecked(false);
        lock = getArguments().getBoolean(LOCKKEY);
        Log.e("dddddddddd",""+lock);

        offlinestudent = new HashMap<>();

        viewMap = new HashMap<>();

        done.setEnabled(true);
        if (EditMode){
            if (lock){
                delete.setVisibility(View.GONE);
            }else {
                delete.setVisibility(View.VISIBLE);
            }
            ArrayList<Student> list1 = new ArrayList<>();
            Student student ;
            try {
                JSONArray array = new JSONArray(StudentIDs);
                for (int i = 0; i < array.length() ; i++) {
                    JSONObject object = array.getJSONObject(i);
                    student = new Student();
                    student.setID(object.getString("ID"));

                    list1.add(student);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            offlinestudent.put(2,list1);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeleltGroup(GroupID);
                }
            });

            Name.setText(NameString);
            try {
                JSONArray array = new JSONArray(StudentIDs);
                for (int i = 0; i < array.length() ; i++) {
                    JSONObject object = array.getJSONObject(i);
                    String ID = object.getString("ID");

                    IDs.add(ID);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        viewMap.put(0,blue);
        viewMap.put(1,green);
        viewMap.put(2,red);
        viewMap.put(3,yellow);


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<Student> requeststudentarratlist = offlinestudent.get(2);


                JSONArray jsonArray = new JSONArray();

                if (!Name.getText().toString().isEmpty()){
                    if (requeststudentarratlist != null){

                        for (int i = 0; i < requeststudentarratlist.size(); i++) {
                            jsonArray.put(requeststudentarratlist.get(i).getJsonObject());
                        }
                        if (EditMode){
                            Updategroup(color , jsonArray ,Name.getText().toString() , GroupID);
                        }else {
                            insertgroup(color , jsonArray ,Name.getText().toString() , GroupID);
                        }

                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.remove(InputStudentGroup.this);
                        transaction.commit();


                    }else if (requeststudentarratlist == null){
                        Toast.makeText(getContext(), "لطفا دانش اموزان مورد نظر را وارد نمایید.", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "لظفا اسم گروه را وارد نمایید.", Toast.LENGTH_SHORT).show();
                }


            }
        });


        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0){
                    ArrayList<Student> students = offlinestudent.get(1);
                    ArrayList<Student> curentstudent = new ArrayList<>();

                    for (int i = 0; i < students.size() ; i++) {

                        if(students.get(i).getName().contains(s.toString())) {
                            curentstudent.add(students.get(i));
                        }

                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        InputStudentAdapter adapter = new InputStudentAdapter(getContext(),curentstudent,InputStudentGroup.this,0 );
                        recyclerView.setAdapter(adapter);

                    }
                    if (curentstudent != null){

                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        InputStudentAdapter adapter = new InputStudentAdapter(getContext(),curentstudent,InputStudentGroup.this,0);
                        recyclerView.setAdapter(adapter);

                    }



                }else {
                    ArrayList<Student> students = offlinestudent.get(1);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    InputStudentAdapter adapter = new InputStudentAdapter(getContext(),students, InputStudentGroup.this,0);
                    recyclerView.setAdapter(adapter);
                }

            }
        });


        list = new ArrayList<>();
        queue = Volley.newRequestQueue(getContext());


        setupstudents(new VolleyCallBack() {
            @Override
            public void contentCallBack(ArrayList<Student> students) {
                offlinestudent.put(1,students);
            }
        });



        return view;
    }

    private void DeleltGroup(final String GRoupID) {

        StringRequest request = new StringRequest(Request.Method.POST, "http://192.168.42.210/mysite/Rschool/DeletGroup.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            Log.e("ddddddddd",response);
            getActivity().onBackPressed();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ddddddddddddd",error.getLocalizedMessage());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("GroupID",GRoupID);
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);

        queue.add(request);

    }


    private void insertgroup(final int color , final JSONArray IDs , final String Name , String GroupID){

        int Id = Integer.valueOf(GroupID);


        final String finalGroupID = String.valueOf(Id + 1);



        insert = new StringRequest(Request.Method.POST, inserturl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                getActivity().onBackPressed();
                Log.e("mmmmmmmmm",response);
                Log.e("Fffffffffff",response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();

                map.put("students",IDs.toString());
                map.put("color",String.valueOf(color));
                map.put("name",Name);
                map.put("GroupID" , finalGroupID);

                return map;
            }
        };


        insert.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        insert.setShouldCache(false);

        queue.add(insert);

    }


    private void Updategroup(final int color , final JSONArray IDs , final String Name , final String GroupID){



        update = new StringRequest(Request.Method.POST, updateurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                getActivity().onBackPressed();
                Log.e("mmmmmmmmm",response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();

                map.put("students",IDs.toString());
                map.put("color",String.valueOf(color));
                map.put("name",Name);
                map.put("GroupID" , GroupID);

                return map;
            }
        };


        update.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        update.setShouldCache(false);

        queue.add(update);

    }


    private void setupstudents(final VolleyCallBack callBack) {
        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response != null){

                    Student student;

                    try {
                        JSONArray jsonArray = new JSONArray(response);

                        for (int i = 0; i < jsonArray.length() ; i++) {
                            student = new Student();

                            JSONObject object = jsonArray.getJSONObject(i);

                            student.setID(object.getString("ID"));
                            student.setNationalID(object.getString("nationalID"));
                            student.setName(object.getString("FirstName"));
                            student.setLastName(object.getString("LastName"));
                            student.setFatherName(object.getString("FatherName"));

                            for (int j = 0; j <IDs.size() ; j++) {
                                if (IDs.get(j).equals(student.getID())){
                                    student.setChecked(true);
                                }
                            }
                            list.add(student);
                        }
                        callBack.contentCallBack(list);

                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        InputStudentAdapter adapter = new InputStudentAdapter(getContext(),list,InputStudentGroup.this,0);
                        recyclerView.setAdapter(adapter);


                    } catch (JSONException e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("fdffffffffff",error.getLocalizedMessage());
            }
        });


        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);

        queue.add(request);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked){
            int id = buttonView.getId();

            for (int i = 0; i < viewMap.size() ; i++) {
                if (id != viewMap.get(i).getId()){
                    ((AppCompatCheckBox) viewMap.get(i)).setChecked(false);
                }else if (id == viewMap.get(i).getId()){{
                    switch (i){
                        case 0 : color = 0 ; break;
                        case 1 : color = 1 ; break;
                        case 2 : color = 2; break;
                        case 3 : color = 3 ; break;
                    }

                }

                }

            }
            done.setEnabled(true);

        }else {
            done.setEnabled(false);
        }


    }

    @Override
    public void CallBackStudents(ArrayList<Student> students) {

        if (students.size() == 0){
            done.setEnabled(false);
        }else {
            offlinestudent.put(2,students);
            done.setEnabled(true);

        }

    }

    interface VolleyCallBack{
        void contentCallBack(ArrayList<Student> students);
    }
}
