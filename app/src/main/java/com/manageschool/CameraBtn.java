package com.manageschool;


import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutCompat;

import de.hdodenhof.circleimageview.CircleImageView;

public interface CameraBtn {

    void Camera(int Code , AppCompatImageButton request, CircleImageView profile, LinearLayoutCompat btnparent, AppCompatImageButton camera, AppCompatImageButton gallery , Fragment fragment);
    void ContentCallBack(String Name, String nationalId, String LastName, String education);
    void ProfilContent(String Name , String LastName);


}
