package com.manageschool;


import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class MenuStudentFragment extends Fragment {

    FloatingActionButton floatingBTN ;
    AppCompatImageButton close , add , refresh;
    FragmentCallBack callBack;
    String url = "http://192.168.42.210/mysite/Rschool/getStudent.php";
    String urlgroups = "http://192.168.42.210/mysite/Rschool/getGroups.php";
    RecyclerView studentsview , groupsview;
    StringRequest request , grouprequest;
    RequestQueue queue;
    ArrayList<Student> list;
    RelativeLayout groupparent;
    Animation closeanim , Openanim;
    Map<String , String > map;
    private String GROUPIDKEY = "GroupID";
    int TAG = 0;



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (FragmentCallBack) context;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.student_fragemnt, container , false);
        floatingBTN = (FloatingActionButton) view.findViewById(R.id.floatingbtn);


        studentsview = (RecyclerView) view.findViewById(R.id.students);
        groupsview = (RecyclerView) view.findViewById(R.id.groups);
        add = (AppCompatImageButton) view.findViewById(R.id.newstudent);
        groupparent = (RelativeLayout) view.findViewById(R.id.groupparent);
        refresh = (AppCompatImageButton) view.findViewById(R.id.refresh);
        close =(AppCompatImageButton) view.findViewById(R.id.closegroups);
        close.getDrawable().setColorFilter(Color.parseColor("#00bcd4") , PorterDuff.Mode.SRC_IN);
        closeanim = AnimationUtils.loadAnimation(getContext(),R.anim.slideup);
        Openanim = AnimationUtils.loadAnimation(getContext(),R.anim.slidedown);


        map = new HashMap<>();
        TAG = 2 ;
        callBack.NewStudentBTN(" ",0,add);
        list = new ArrayList<>();

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             groupparent.setVisibility(View.GONE);
             groupparent.startAnimation(closeanim);
             studentsview.startAnimation(Openanim);

            }
        });

        floatingBTN.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                groupparent.setVisibility(View.VISIBLE);
                groupparent.startAnimation(Openanim);
                studentsview.startAnimation(Openanim);

                return false;
            }
        });

        refresh.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                onResume();
            }
        });


        floatingBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TAG == 2){
                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                    transaction.addToBackStack("myscreen");
                    transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_in);
                    InputStudentGroup fragment = InputStudentGroup.newInstance(false , " ", map.get(GROUPIDKEY)," ",true);
                    transaction.replace(android.R.id.content, fragment );
                    transaction.commit();
                }
            }
        });


        queue = Volley.newRequestQueue(getContext());


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        setupstudents();
        setupgroups();

    }

    private void setupstudents() {
        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                ArrayList<String> color = new ArrayList<>();

                if (response != null){

                    Student student;

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        list = new ArrayList<>();

                        for (int i = 0; i < jsonArray.length() ; i++) {
                            student = new Student();

                            JSONObject object = jsonArray.getJSONObject(i);

                            student.setID(object.getString("ID"));
                            student.setNationalID(object.getString("nationalID"));
                            student.setName(object.getString("FirstName"));
                            student.setLastName(object.getString("LastName"));
                            student.setFatherName(object.getString("FatherName"));
                            student.setAccepted(object.getString("accepted"));
                            String birth = CompareCalensars(object.getString("Birth"));
                            student.setBirthYear(birth);


                            color.add(object.getString("color"));
                            list.add(student);


                        }

                        studentsview.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        studentsview.setItemAnimator(new DefaultItemAnimator());
                        StudentRecyclerView adapter = new StudentRecyclerView(getContext(),list,color);
                        studentsview.setAdapter(adapter);


                    } catch (JSONException e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });


        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);

        queue.add(request);
    }
    private String CompareCalensars(String studentbirth ) {


        int start = Integer.valueOf(studentbirth);

        SimpleDateFormat Year = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        String yearString = Year.format(new Date());

        SimpleDateFormat month = new SimpleDateFormat("MM", Locale.ENGLISH);
        String monthString = month.format(new Date());


        SimpleDateFormat day = new SimpleDateFormat("dd", Locale.ENGLISH);
        String dayString = day.format(new Date());

        PersianDate pdate = new PersianDate();
        PersianDateFormat format = new PersianDateFormat();
        format.format(pdate);

        int[] jalili = pdate.toJalali(Integer.valueOf(yearString) , Integer.valueOf(monthString) , Integer.valueOf(dayString));

        int nowyear = jalili[0];


        int birth = nowyear - start;

        return String.valueOf(birth);

    }

    private void setupgroups() {

        final ArrayList<Groups> names = new ArrayList<>();



        grouprequest = new StringRequest(Request.Method.POST, urlgroups , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("DFfffffffff",response);


                if (response != null){

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        Groups groups ;
                        for (int i = 0; i < jsonArray.length() ; i++) {
                            groups = new Groups();
                            JSONObject object = jsonArray.getJSONObject(i);
                            groups.setNames(object.getString("Name"));
                            groups.setColor(object.getString("Color"));
                            groups.setGroupsID(object.getString("GroupID"));
                            boolean lock ;
                            if (object.getString("Lock").equals("0")){
                                lock = false;
                            }else {
                                lock = true;
                            }if (groups.getGroupsID().equals("0") || groups.getGroupsID().equals("1")){
                                lock = true;
                            }

                            groups.setCheck(lock);


                            names.add(groups);

                        }

                        groupsview.setLayoutManager(new GridLayoutManager(getContext(),2));
                        groupsview.setItemAnimator(new DefaultItemAnimator());
                        GroupsRecyclerView adapter = new GroupsRecyclerView(true ,names,getContext(), (AppCompatActivity) getActivity());
                        groupsview.setAdapter(adapter);

                        if (names.size() == 0){
                            map.put(GROUPIDKEY , "0");
                        }else {
                            map.put(GROUPIDKEY , names.get(names.size() - 1).getGroupsID());
                        }



                    } catch (JSONException e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ggggggggg" , error.getLocalizedMessage());
            }
        });

        grouprequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        grouprequest.setShouldCache(false);

        queue.add(grouprequest);
    }

}
