package com.manageschool;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidadvance.topsnackbar.TSnackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LoginActivity extends AppCompatActivity {


    AppCompatEditText NationalID;
    AppCompatButton done;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    RequestQueue queue;
    StringRequest Check;
    String URL = "http://192.168.42.210/mysite/Rschool/checkmanage.php";
    Output_files files;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        boolean b = preferences.getBoolean("Login",false);
        if (b){
            finishA();
        }

        setContentView(R.layout.activity_login);
        NationalID = (AppCompatEditText) findViewById(R.id.nationalID);
        done = (AppCompatButton) findViewById(R.id.done);
        queue = Volley.newRequestQueue(this);
        files = new Output_files(this);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckRequest();
            }
        });


    }

    private void finishA() {
        finish();
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }

    private void CheckRequest(){

        Check = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (!response.trim().equals("null")){
                    editor.putBoolean("Login",true).apply();


                    try{
                        JSONObject object = new JSONObject(response);
                        files.write("N" , object.getString("nationalID"));
                        files.write("FirstName",object.getString("Name") );
                        files.write("LastName" , object.getString("LastName"));
                        files.write("IMG" , object.getString("ProfileAddRess"));

                    }catch (JSONException e){
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    File file = new File("School");
                    if (!file.mkdir()){
                        file.mkdir();
                    }

                    File file1 = new File(file.getAbsoluteFile() + "/Image");
                    if (!file1.mkdir()){
                        file1.mkdir();
                    }

                    finishA();
                }else {
                    TSnackbar snackbar = TSnackbar.make(findViewById(android.R.id.content), "متاسفانه این کد ملی اجازه دسترسی به برنامه را ندارد.", TSnackbar.LENGTH_LONG);
                    View snackbarView = snackbar.getView();
                    snackbarView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                    TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                    textView.setTextColor(Color.parseColor("#00BCD4"));
                    snackbar.show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("N",NationalID.getText().toString());
                return map;
            }
        };

        Check.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Check.setShouldCache(false);

        queue.add(Check);


    }

}
