package com.manageschool;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;

public class NeatDetailAdapter extends  RecyclerView.Adapter<NeatDetailAdapter.ViewHolder>{


    ArrayList<NeatDetail> neatDetails;
    Context context;
    Animation anim;

    public NeatDetailAdapter(Context context , ArrayList<NeatDetail> neatDetails){

        if (neatDetails == null){
            neatDetails = new ArrayList<>();
        }
        this.neatDetails = neatDetails;
        this.context = context;
        anim = AnimationUtils.loadAnimation(context , R.anim.neatitems);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.neatdetailitem , viewGroup , false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.cardView.startAnimation(anim);

        String weekDay = "شنبه";

        switch (Integer.valueOf(neatDetails.get(i).getWeekDay())){
            case 0 : weekDay = "شنبه" ; break;
            case 1 : weekDay = "یکشنبه" ; break;
            case 2 : weekDay = "دوشنبه" ; break;
            case 3 : weekDay = "سه شنبه" ; break;
            case 4 : weekDay = "چهارشنبه" ; break;
            case 5 : weekDay = "پنجشنبه" ; break;
            case 6 : weekDay = "جمعه" ; break;
        }

        viewHolder.weekDay.setText(weekDay);
        viewHolder.Hour.setText(neatDetails.get(i).getHour());
        viewHolder.master.setText(neatDetails.get(i).getMaster());
        viewHolder.examcount.setText(neatDetails.get(i).getExamcount());
        viewHolder.examavrage.setText(neatDetails.get(i).getAvrage());


    }

    @Override
    public int getItemCount() {
        return neatDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{


        AppCompatTextView weekDay , Hour , master , examcount , examavrage;
        CardView cardView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            weekDay = (AppCompatTextView) itemView.findViewById(R.id.weekday);
            Hour = (AppCompatTextView) itemView.findViewById(R.id.hour);
            master = (AppCompatTextView) itemView.findViewById(R.id.master);
            examcount = (AppCompatTextView) itemView.findViewById(R.id.exam);
            examavrage = (AppCompatTextView) itemView.findViewById(R.id.avrage);
            cardView = (CardView) itemView.findViewById(R.id.card);
        }
    }
}
