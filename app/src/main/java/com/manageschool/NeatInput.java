package com.manageschool;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class NeatInput extends Fragment {

    private String GROUPIDKEY = "GroupKey";
    private String LEVELKEY = "LevelKey";
    TypeWriter writer;
    AppCompatImageButton bot;
    String first = "السلام علیکم...";
    String secend = "بزار خودمو برات معرفی کنم  ";
    String third = "اوممم من neat هستم   ";
    String four = "و توسط تیم برنامه نویسی طراحی شدم   ";
    String five = "امیدوارم برای کارا بودن بیشتر برنامه   ";
    String six = "بتونم کمکتون کنم   ";
    String seven = "والسلام علیکم و رحمه الله...";
    String eight = "تیم برنامه نویسی leno";
    String levelE = "لطفا سطح شروع مقطع رو انتخاب کن.  ";
    String GroupE = "گروهی که میخوای به ای سطح وصل کنی رو انتخاب کن .   ";


    ArrayList<String> message;
    AppCompatTextView Level, Group;
    RecyclerView recyclerView;
    StringRequest Groups, levelRequest , Insert;
    RequestQueue queue;
    ArrayList<Groups> list;
    ArrayList<Level> Levels;
    AppCompatEditText Start , End;
    int TAG;
    Animation in, out;
    Map<String, String> result;
    String wait = "لطفا صبر کنید..\nیَا أَیُّهَا الَّذِینَ آمَنُواْ اسْتَعِینُواْ بِالصَّبْرِ وَالصَّلاَةِ إِنَّ اللّهَ مَعَ الصَّابِرِینَ...";
    AppCompatImageButton done;
    AppCompatButton next;
    private static int COUNTER = 0;

    String GroupstUrl = "http://192.168.42.210/mysite/Rschool/getGroups.php";
    String LevelUrl = "http://192.168.42.210/mysite/Rschool/getLevel.php";
    String InsertURL = "http://192.168.42.210/mysite/Rschool/Insertneat.php";




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_neat_input, container, false);
        writer = (TypeWriter) view.findViewById(R.id.message);
        Level = (AppCompatTextView) view.findViewById(R.id.level);
        Group = (AppCompatTextView) view.findViewById(R.id.group);
        in = AnimationUtils.loadAnimation(getContext(),R.anim.slide_left_week);
        out = AnimationUtils.loadAnimation(getContext(),R.anim.slide_right_week);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        done = (AppCompatImageButton) view.findViewById(R.id.done);
        next = (AppCompatButton) view.findViewById(R.id.next);
        Start =(AppCompatEditText) view.findViewById(R.id.Start);
        End =(AppCompatEditText) view.findViewById(R.id.end);






        message = new ArrayList<>();
        message.add(first);
        message.add(secend);
        message.add(third);
        message.add(four);
        message.add(five);
        message.add(six);
        message.add(seven);
        message.add(eight);
        writer.setReaped(false);
        result = new HashMap<>();


        bot = (AppCompatImageButton) view.findViewById(R.id.bot);

        writer.setCharacterDelay(150);
        writer.animateText(message);
        queue = Volley.newRequestQueue(getContext());

        bot.getDrawable().setColorFilter(getResources().getColor(R.color.Base), PorterDuff.Mode.SRC_IN);

        Level.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writer.animateText(wait);
                if (TAG != 5){
                    TAG = 5;
                    LoadLevles();
                }
            }
        });

        Group.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writer.animateText(wait);

                        TAG = 2 ;
                        LoadStudentGroups();

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (COUNTER < 2){
                    writer.animateText("حتما حتما باید دستکم 2 مقطع ثبت شه.");
                }else if (result.get(LEVELKEY) == null){
                    writer.animateText("سطحی انتخاب نکردی");
                }else if (result.get(GROUPIDKEY) == null){
                    writer.animateText("گروهی انتخاب نکردی");
                }else if (Start.getText().toString().length() < 1){
                    writer.animateText("سن شروعی وارد نکردی یا سنی که وارد کردی معتبر نیست. ");
                }else if (End.getText().toString().length() < 1){
                    writer.animateText("سن پایانی وارد نکردی یا سنی که وارد کردی معتبر نیست. ");
                }else {
                    InsertRequest(result.get(LEVELKEY),result.get(GROUPIDKEY) , Start.getText().toString() , End.getText().toString());
                    getActivity().onBackPressed();
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                if (result.get(LEVELKEY) == null){
                    writer.animateText("سطحی انتخاب نکردی");
                }else if (result.get(GROUPIDKEY) == null){
                    writer.animateText("گروهی انتخاب نکردی");
                }else if (Start.getText().toString().length() < 1){
                    writer.animateText("سن شروعی وارد نکردی یا سنی که وارد کردی معتبر نیست. ");
                }else if (End.getText().toString().length() < 1){
                    writer.animateText("سن پایانی وارد نکردی یا سنی که وارد کردی معتبر نیست. ");
                }else {
                    Level.setText("سطح");
                    Group.setText("گروه");
                    writer.animateText(wait);
                    recyclerView.setVisibility(View.INVISIBLE);
                    recyclerView.startAnimation(out);
                    writer.animateText("در حال ذخیره و آماده سازی برای مقطع بعدی.");
                    InsertRequest(result.get(LEVELKEY),result.get(GROUPIDKEY) , Start.getText().toString() , End.getText().toString());
                    Start.getText().clear();
                    End.getText().clear();

                    int levelID = Integer.valueOf(result.get(LEVELKEY));
                    if (COUNTER == Levels.size() -1 || COUNTER == list.size() -1 ){
                        next.setVisibility(View.INVISIBLE);
                    }
                    COUNTER++;

                    for (int i = 0; i < Levels.size(); i++) {
                        if (Integer.valueOf(Levels.get(i).getID()) == levelID){
                            Levels.remove(i);
                            Log.e("Fffff",Levels.get(i).getName());
                        }

                    }
                    String GroupID = result.get(GROUPIDKEY);

                    for (int i = 0; i < list.size() ; i++) {
                        if (list.get(i).getGroupsID().equals(GroupID)){
                            Log.e("Fffff",list.get(i).getNames());
                            list.remove(i);
                        }
                    }
                    result.put(GROUPIDKEY , null);
                    result.put(LEVELKEY , null);
                }
            }
        });

        return view;
    }

    private void LoadLevles() {

        levelRequest = new StringRequest(Request.Method.POST, LevelUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Level level;
                if (Levels == null){
                    Levels = new ArrayList<>();
                    if (response != null) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            for (int i = 0; i < jsonArray.length() ; i++) {
                                level = new Level();
                                JSONObject object = jsonArray.getJSONObject(i);

                                level.setID(object.getString("ID"));
                                level.setName(object.getString("Name"));
                                level.setClassRoomName(object.getString("ClassRoom"));

                                Levels.add(level);
                            }
                            recyclerView.setVisibility(View.VISIBLE);
                            recyclerView.startAnimation(in);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            LevelAdapter adapter = new LevelAdapter(Levels,getContext() , false);
                            recyclerView.setAdapter(adapter);
                            writer.animateText("لطفا سطح رو انتخاب کن.");


                            recyclerView.addOnItemTouchListener(
                                    new RecyclerItemClickListener(getActivity(), new   RecyclerItemClickListener.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(View view, int position) {

                                            if (TAG == 5){
                                                Level.setText(Levels.get(position).getName());
                                                recyclerView.setVisibility(View.INVISIBLE);
                                                recyclerView.startAnimation(out);
                                                result.put(LEVELKEY ,Levels.get(position).getID());
                                                writer.animateText("سطح به نام "+"("+Levels.get(position).getName()+")"+"انتخاب شد.");
                                                TAG = 4;
                                            }
                                        }
                                    })
                            );


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }if (Levels.size() == 0){
                    writer.animateText("سطحی برای نمایش پیدا نشد.");
                    TAG = 8;
                } else {
                    writer.animateText("لطفا سطح رو انتخاب کن.");
                    recyclerView.setVisibility(View.VISIBLE);
                    recyclerView.startAnimation(in);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    LevelAdapter adapter = new LevelAdapter(Levels, getContext(), false);
                    recyclerView.setAdapter(adapter);
                    recyclerView.addOnItemTouchListener(
                            new RecyclerItemClickListener(getActivity(), new   RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {

                                    if (TAG == 5){
                                        Level.setText(Levels.get(position).getName());
                                        recyclerView.setVisibility(View.INVISIBLE);
                                        recyclerView.startAnimation(out);
                                        result.put(LEVELKEY , Levels.get(position).getID());
                                        writer.animateText("سطح به نام "+"("+Levels.get(position).getName()+")"+"انتخاب شد.");
                                        TAG = 4;
                                    }
                                }
                            })
                    );
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Dsdvsdvsedvasedvas" , error.getMessage());
            }
        });

        levelRequest.setShouldCache(false);

        levelRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(levelRequest);
    }
    private void LoadStudentGroups(){

        TAG = 2;

        if (list == null){
            list = new ArrayList<>();

            Groups = new StringRequest(Request.Method.POST, GroupstUrl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.e("dddddddddd", response);


                    if (response != null){

                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            Groups groups ;
                            for (int i = 0; i < jsonArray.length() ; i++) {
                                groups = new Groups();
                                JSONObject object = jsonArray.getJSONObject(i);
                                groups.setNames(object.getString("Name"));
                                groups.setColor(object.getString("Color"));
                                groups.setGroupsID(object.getString("GroupID"));

                                if (groups.getGroupsID().trim().equals("0")|| groups.getGroupsID().trim().equals("1")){

                                }else {
                                    list.add(groups);
                                }

                            }
                            recyclerView.setVisibility(View.VISIBLE);
                            recyclerView.startAnimation(in);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext() , LinearLayoutManager.VERTICAL , false));
                            recyclerView.setItemAnimator(new DefaultItemAnimator());
                            GroupsRecyclerView adapter = new GroupsRecyclerView(false ,list,getContext(), (AppCompatActivity) getActivity());
                            recyclerView.setAdapter(adapter);
                            writer.animateText("لطفا گروه دانش آموزان رو انتخاب کن.");

                            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    if (TAG == 2){
                                        Group.setText(list.get(position).getNames());
                                        result.put(GROUPIDKEY , list.get(position).getGroupsID());
                                        writer.animateText("گروه به نام "+"("+list.get(position).getNames()+")"+"انتخاب شد.");
                                    }
                                }
                            }));


                        } catch (JSONException e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("fdffffffffff",error.getLocalizedMessage());
                }
            });


            Groups.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Groups.setShouldCache(false);

            queue.add(Groups);

        }if (list.size() == 0){
            writer.animateText("گروهی برای نمایش پیدا نشد.");
            TAG = 8;
        }else {
            writer.animateText("لطفا گروه دانش آموزان رو انتخاب کن.");
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.startAnimation(in);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext() , LinearLayoutManager.VERTICAL , false));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            GroupsRecyclerView adapter = new GroupsRecyclerView(false ,list,getContext(), (AppCompatActivity) getActivity());
            recyclerView.setAdapter(adapter);

            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    if (TAG == 2){
                        Group.setText(list.get(position).getNames());
                        result.put(GROUPIDKEY , list.get(position).getGroupsID());
                        writer.animateText("گروه به نام "+"("+list.get(position).getNames()+")"+"انتخاب شد.");
                    }
                }
            }));

        }
    }


    private void InsertRequest(final String LevelID , final String GroupID , final String Start , final String End){

        Insert = new StringRequest(Request.Method.POST, InsertURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
            writer.animateText("سطح جدید");
                result.put(GROUPIDKEY , null);
                result.put(LEVELKEY , null);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            Log.e("Dddd",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("groupID",GroupID);
                map.put("LevelID",LevelID);
                map.put("Start" , Start);
                map.put("End",End);
                return map;
            }
        };
        Insert.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Insert.setShouldCache(false);

        queue.add(Insert);
    }


}
