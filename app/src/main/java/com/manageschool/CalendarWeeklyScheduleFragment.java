package com.manageschool;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class CalendarWeeklyScheduleFragment extends Fragment {

    FloatingActionButton add;
    RequestQueue queue;
    StringRequest load;
    ArrayList<MasterClassRoom> classRooms;
    AppCompatImageButton refresh;
    RecyclerView recyclerView;
    String LadClassroomsURL = "http://192.168.42.210/mysite/Rschool/CalendargetMasterClassRoom.php";
    private static String CalendarIDKEY = "CalendarID";
    String ID ;

    public static CalendarWeeklyScheduleFragment newInstance(String CalendarID) {

        Bundle args = new Bundle();

        CalendarWeeklyScheduleFragment fragment = new CalendarWeeklyScheduleFragment();
        fragment.setArguments(args);
        args.putString(CalendarIDKEY , CalendarID);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weekly_schedule, container, false);
        add = (FloatingActionButton) view.findViewById(R.id.floatingbtn);
        refresh = (AppCompatImageButton) view.findViewById(R.id.refresh);
        recyclerView = (RecyclerView) view.findViewById(R.id.weelklyschedule);
        refresh.setVisibility(View.GONE);
        add.hide();

        classRooms = new ArrayList<>();

        queue = Volley.newRequestQueue(getContext());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        classRooms = new ArrayList<>();
        ID = getArguments().getString(CalendarIDKEY);

        Load(ID);
    }

    private void Load(final String ID){

        load = new StringRequest(Request.Method.POST, LadClassroomsURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    MasterClassRoom classRoom;
                    JSONObject object;

                    for (int i = 0; i < jsonArray.length() ; i++) {

                        object = jsonArray.getJSONObject(i);
                        classRoom = new MasterClassRoom();

                        classRoom.setID(object.getString("ID"));
                        classRoom.setLesson(object.getString("LessonID"));
                        classRoom.setMaster(object.getString("MasterID"));
                        classRoom.setStartHour(object.getString("StartHour"));
                        classRoom.setEndHour(object.getString("EndHour"));
                        classRoom.setWeekDay(object.getString("WeekDay"));
                        classRoom.setLevelName(object.getString("LevelName"));
                        classRoom.setClassRoomName(object.getString("LevelClassName"));
                        classRoom.setStartQuran(object.getString("StartQuran"));
                        classRoom.setEndQuran(object.getString("EndQuran"));



                        classRooms.add(classRoom);
                    }
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    WeeklyscheduleAdapter adapter = new WeeklyscheduleAdapter(1,true,classRooms , getContext());
                    recyclerView.setAdapter(adapter);

                } catch (JSONException e) {
                    Log.e("errrrrrrror",e.getLocalizedMessage());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errrrrror" , error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("ID",ID);
                return map;
            }
        };

        load.setShouldCache(false);
        load.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(load);


    }

}
