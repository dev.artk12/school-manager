package com.manageschool;

public class Question {

    private String Question;
    private String First;
    private String Secend;
    private String Third;
    private String Four;
    private String Level;
    private String ID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getLevelID() {
        return LevelID;
    }

    public void setLevelID(String levelID) {
        LevelID = levelID;
    }

    private String LevelID;


    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getFirst() {
        return First;
    }

    public void setFirst(String first) {
        First = first;
    }

    public String getSecend() {
        return Secend;
    }

    public void setSecend(String secend) {
        Secend = secend;
    }

    public String getThird() {
        return Third;
    }

    public void setThird(String third) {
        Third = third;
    }

    public String getFour() {
        return Four;
    }

    public void setFour(String four) {
        Four = four;
    }

    public String getLevel() {
        return Level;
    }

    public void setLevel(String level) {
        Level = level;
    }
}
