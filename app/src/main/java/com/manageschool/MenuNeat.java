package com.manageschool;


import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class MenuNeat extends Fragment {


    AppCompatImageButton  bot , delete ;
    FloatingActionButton add ;
    RecyclerView recyclerView;
    TypeWriter typeWriter;
    ArrayList<String> list;
    StringRequest request , Delete ,  checkC;
    RequestQueue queue;
    ArrayList<Neat> neats;
    ArrayList<String> GroupIDs;
    ArrayList<String> Groupname;
    ArrayList<String> End;
    ArrayList<String> Start;

    private String getNeatURL = "http://192.168.42.210/mysite/Rschool/getNeat.php";
    private String DeleteNeatURL = "http://192.168.42.210/mysite/Rschool/DeletNeat.php";



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu_neat, container, false);
        bot = (AppCompatImageButton) view.findViewById(R.id.bot);
        recyclerView = (RecyclerView) view.findViewById(R.id.neats);
        add = (FloatingActionButton) view.findViewById(R.id.floatingbtn);
        delete = (AppCompatImageButton) view.findViewById(R.id.delelte);

        bot.getDrawable().setColorFilter(getResources().getColor(R.color.Base), PorterDuff.Mode.SRC_IN);
        typeWriter = (TypeWriter) view.findViewById(R.id.message);
        typeWriter.setCharacterDelay(20);
        typeWriter.setReaped(false);
        list = new ArrayList<>();
        list.add("میتونی رو هر مقطع کلیک کنی تا اطلاعاتشو برات نشون پدم.  ");
        list.add("");
        typeWriter.animateText(list);
        add.hide();
        GroupIDs = new ArrayList<>();
        Groupname = new ArrayList<>();
        neats = new ArrayList<>();
        Start = new ArrayList<>();
        End = new ArrayList<>();

        queue = Volley.newRequestQueue(getContext());
        Load();

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Delete();
            }
        });


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(R.anim.slidedown, R.anim.slideup);
                NeatInput fragment = new NeatInput();
                transaction.replace(android.R.id.content,fragment);
                transaction.commit();
            }
        });


        return view;
    }

    private void Load(){
        request = new StringRequest(Request.Method.POST, getNeatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.equals("[]")){
                   add.show();
                   typeWriter.animateText("چیزی ثبت نشده میتونی از دکمه کناری\n برای ثبت استفاده کنی. ");
                   delete.setVisibility(View.GONE);
                }if (response != null){
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        Neat neat;

                        for (int i = 0; i < jsonArray.length() ; i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            neat = new Neat();
                            neat.setGroupID(object.getString("GroupID"));
                            neat.setGroupName(object.getString("GroupName"));
                            neat.setLevelID(object.getString("LevelID"));
                            neat.setLevelName(object.getString("LevelName"));
                            neat.setStartYear(object.getString("Start"));
                            neat.setEndYear(object.getString("End"));
                            GroupIDs.add(object.getString("GroupID"));
                            Groupname.add(object.getString("GroupName"));
                            Start.add(object.getString("Start"));
                            End.add(object.getString("End"));



                            neats.add(neat);

                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        NeatAdapter adapter = new NeatAdapter(getContext() , neats , (AppCompatActivity) getActivity());
                        recyclerView.setAdapter(adapter);


                    }catch (JSONException e){
                        Log.e("ddddddddddd",e.getLocalizedMessage());
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }
    private void Delete(){
        Delete = new StringRequest(Request.Method.POST, DeleteNeatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                getActivity().onBackPressed();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        Delete.setShouldCache(false);
        Delete.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(Delete);
    }

}
