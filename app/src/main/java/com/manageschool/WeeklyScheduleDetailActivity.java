package com.manageschool;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Toast;

public class WeeklyScheduleDetailActivity extends AppCompatActivity {


    ViewPager viewPager;
    TabLayout tabLayout;
    MasterClassRoom classRoom;
    boolean eboo;
    WeekPagerAdapetr adapetr;
    String ID , StartHour , EndHour , WeekDay , Master , Level , Studi , ClassID , StartQuran , EndQuran;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weekly_schedule_detail);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        classRoom = getIntent().getExtras().getParcelable("classroom");
        eboo = getIntent().getExtras().getBoolean("Editmode");


        Bundle bundle = getIntent().getExtras();
        ID = bundle.getString("ID");
        StartHour = bundle.getString("StartHour");
        EndHour = bundle.getString("EndHour");
        Master = bundle.getString("Master");
        WeekDay = bundle.getString("WeekDay");
        Level = bundle.getString("Level");
        Studi = bundle.getString("Studi");
        ClassID = bundle.getString("ClassId");
        StartQuran = bundle.getString("StartQuran");
        EndQuran = bundle.getString("EndQuran");






        if (ID == null || StartHour == null ||EndHour == null ||Master == null ||WeekDay == null ||Level == null ||Studi == null ){
            Toast.makeText(this, "Somthing wrong", Toast.LENGTH_SHORT).show();
        }else {
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
            adapetr = new WeekPagerAdapetr(getSupportFragmentManager() , ClassID ,ID , StartHour , EndHour , WeekDay , Master , Level , Studi ,eboo , StartQuran , EndQuran);
            viewPager.setAdapter(adapetr);
        }


    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private class WeekPagerAdapetr extends FragmentPagerAdapter{

        String ID , StartHour , EndHour , WeekDay , Master , Level , Studi , ClassID , StartQuran , EndQuran;
        boolean eboo;

        public WeekPagerAdapetr(FragmentManager fm ,String ClassID , String ID , String  StartHour ,
                                String  EndHour , String  WeekDay , String  Master ,String  Level ,String  Studi , boolean eboo , String StartQuran , String EndQuran) {
            super(fm);
            this.ID = ID;
            this.EndHour = StartHour;
            this.StartHour = EndHour;
            this.Level = Level;
            this.Master = Master;
            this.WeekDay = WeekDay;
            this.Studi = Studi;
            this.eboo = eboo;
            this.ClassID = ClassID;
            this.StartQuran = StartQuran;
            this.EndQuran = EndQuran;

        }

        @Override
        public Fragment getItem(int i) {

            switch (i){
                case 0 : WeeklyScheduleDetail1 w1 = WeeklyScheduleDetail1.newInstance(eboo,ClassID,ID,StartHour,EndHour,WeekDay,Master,Level,Studi, StartQuran , EndQuran); return w1;
                case 1 : return new WeeklyScheduleDetail2();
                case 2 : return  new WeeklyScheduleDetail3();
            }


            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
