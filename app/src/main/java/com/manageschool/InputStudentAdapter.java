package com.manageschool;

import android.content.ClipData;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.util.ArrayList;

public class InputStudentAdapter extends RecyclerView.Adapter<InputStudentAdapter.ViewHolder> {

    ArrayList<Student> students , callbackstudents;
    Context context;
    int CODE = 0;
    InputStudentAdapterCheckCallBack callBack;


    InputStudentAdapter(Context context , ArrayList<Student> students  , InputStudentAdapterCheckCallBack callBack , int CODE ){

        if (students.size() == 0){
            students = new ArrayList<>();
        }
        this.students = students;
        this.context = context;
        callbackstudents = new ArrayList<>();
        this.callBack = callBack;
        this.CODE = CODE;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.inputsudentgroupitem , viewGroup , false);

        ViewHolder viewHolder = new ViewHolder(view);


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        viewHolder.checkBox.setChecked(false);


        viewHolder.nationalid.setText(students.get(i).getNationalID());
        viewHolder.FullName.setText(students.get(i).getName() + " " + students.get(i).getLastName() );

        if (CODE == 0){
            viewHolder.Year.setVisibility(View.GONE);
        }else {
            viewHolder.Year.setVisibility(View.VISIBLE);
            viewHolder.Year.setText(students.get(i).getBirthYear());
        }


        viewHolder.checkBox.setChecked(students.get(i).isChecked());


        viewHolder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                students.get(i).setChecked(isChecked);

                if (students.size() > 0 ){

                    callbackresult();
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return students.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatCheckBox checkBox;
        AppCompatTextView FullName , nationalid;
        AppCompatTextView Year;

        public ViewHolder (View ItemView){
            super(ItemView);
            checkBox = (AppCompatCheckBox) ItemView.findViewById(R.id.studentcheck);
            FullName = (AppCompatTextView) ItemView.findViewById(R.id.name);
            nationalid = (AppCompatTextView) ItemView.findViewById(R.id.nationalid);
            Year = (AppCompatTextView) ItemView.findViewById(R.id.year);
        }
    }

    private void callbackresult(){

        callbackstudents = new ArrayList<>();
        for (int i = 0; i < students.size() ; i++) {

            if (students.get(i).isChecked() == true){
                callbackstudents.add(students.get(i));

            }

        }if (callbackstudents.size() == 0){
            Toast.makeText(context, "دانش آموزای انتخاب نشده است", Toast.LENGTH_SHORT).show();
            callBack.CallBackStudents(callbackstudents);

        }else {
            callBack.CallBackStudents(callbackstudents);
        }
    }


}
