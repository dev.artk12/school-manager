package com.manageschool;

public class Groups {

    private String Names;
    private String Color;
    private String GroupsID;
    private boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }

    public String getGroupsID() {
        return GroupsID;
    }

    public void setGroupsID(String groupsID) {
        GroupsID = groupsID;
    }

    public String getNames() {
        return Names;
    }

    public void setNames(String names) {
        Names = names;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }
}
