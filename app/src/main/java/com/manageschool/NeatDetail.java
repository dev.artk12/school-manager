package com.manageschool;

public class NeatDetail {


    private String  ID;



    private String WeekDay;
    private String Hour;
    private String master;
    private String examcount;
    private String avrage;

    public String getID() {
        return ID;
    }
    public void setID(String ID) {
        this.ID = ID;
    }

    public String getWeekDay() {
        return WeekDay;
    }

    public void setWeekDay(String weekDay) {
        WeekDay = weekDay;
    }

    public String getHour() {
        return Hour;
    }

    public void setHour(String hour) {
        Hour = hour;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getExamcount() {
        return examcount;
    }

    public void setExamcount(String examcount) {
        this.examcount = examcount;
    }

    public String getAvrage() {
        return avrage;
    }

    public void setAvrage(String avrage) {
        this.avrage = avrage;
    }
}
