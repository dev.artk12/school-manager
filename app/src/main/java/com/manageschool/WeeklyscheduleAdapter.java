package com.manageschool;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;

public class WeeklyscheduleAdapter extends RecyclerView.Adapter<WeeklyscheduleAdapter.ViewHolder> {


    Context context;
    ArrayList<MasterClassRoom> classRooms;
    boolean check;
    int TAG ;

    public WeeklyscheduleAdapter (int TAG , boolean check,ArrayList<MasterClassRoom> classRooms , Context context){

        this.classRooms = classRooms;
        this.context = context;
        this.check = check;
        this.TAG = TAG;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.weeklyscheduleadapter , viewGroup , false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.Lesson.setText(classRooms.get(i).getLesson());
        viewHolder.Master.setText(classRooms.get(i).getMaster());
        viewHolder.StartHour.setText(classRooms.get(i).getStartHour());
        viewHolder.EndHour.setText(classRooms.get(i).getEndHour());
        viewHolder.Level.setText(classRooms.get(i).getLevelName());
        viewHolder.classroomname.setText(classRooms.get(i).getClassRoomName());
        if (classRooms.get(i).getStartQuran().trim().equals("0") || classRooms.get(i).getEndQuran().trim().equals("0") || classRooms.get(i).getEndQuran().trim().equals("null")){
            viewHolder.Quran.setVisibility(View.GONE);
        }else {
            viewHolder.StartQuran.setText(classRooms.get(i).getStartQuran());
            viewHolder.EndQuran.setText(classRooms.get(i).getEndQuran());
            Log.e("StartClassRoom",classRooms.get(i).getStartQuran());
            Log.e("EndClassRoom",classRooms.get(i).getEndQuran());

        }



        String weekDay = "شنبه";

        switch (Integer.valueOf(classRooms.get(i).getWeekDay())){
            case 0 : weekDay = "شنبه" ; break;
            case 1 : weekDay = "یکشنبه" ; break;
            case 2 : weekDay = "دوشنبه" ; break;
            case 3 : weekDay = "سه شنبه" ; break;
            case 4 : weekDay = "چهارشنبه" ; break;
            case 5 : weekDay = "پنجشنبه" ; break;
            case 6 : weekDay = "جمعه" ; break;
        }

        viewHolder.Weekday.setText(weekDay);

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check){
                    MasterClassRoom classRoom = classRooms.get(i);
                    Intent intent = new Intent(context , WeeklyScheduleDetailActivity.class);
                    if (TAG == 0){
                        intent.putExtra("Editmode",true);
                    }else if (TAG == 1){
                        intent.putExtra("Editmode",false);
                    }
                    intent.putExtra("ID",classRoom.getID());
                    intent.putExtra("StartHour",classRoom.getStartHour());
                    intent.putExtra("EndHour",classRoom.getEndHour());
                    intent.putExtra("Master",classRoom.getMaster());
                    intent.putExtra("WeekDay",classRoom.getWeekDay());
                    intent.putExtra("Level",classRoom.getLevelName());
                    intent.putExtra("Studi",classRoom.getLesson());
                    intent.putExtra("ClassID",classRoom.getIDClass());
                    intent.putExtra("StartQuran",classRoom.getStartQuran());
                    intent.putExtra("EndQuran",classRoom.getEndQuran());


                    context.startActivity(intent);
                }
            }
        });




    }

    @Override
    public int getItemCount() {
        return classRooms.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView Weekday , StartHour , EndHour , Master , Lesson , Level , classroomname, StartQuran , EndQuran;
        CardView cardView;
        GridLayout Quran;

        public ViewHolder (View itemView){
            super(itemView);

            Weekday = (AppCompatTextView) itemView.findViewById(R.id.weekday);
            StartHour = (AppCompatTextView) itemView.findViewById(R.id.start);
            EndHour = (AppCompatTextView) itemView.findViewById(R.id.end);
            Master = (AppCompatTextView) itemView.findViewById(R.id.master);
            Lesson = (AppCompatTextView) itemView.findViewById(R.id.lesson);
            Level = (AppCompatTextView) itemView.findViewById(R.id.level);
            classroomname = (AppCompatTextView) itemView.findViewById(R.id.classname);
            cardView = (CardView) itemView.findViewById(R.id.cardview);
            EndQuran = (AppCompatTextView) itemView.findViewById(R.id.EndQuran);
            StartQuran = (AppCompatTextView) itemView.findViewById(R.id.StartQuran);
            Quran = (GridLayout) itemView.findViewById(R.id.Quran);



        }

    }
}
