package com.manageschool;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class LetterFragement extends Fragment {


    private static String CODEKEY = "Code";
    String Code;
    RecyclerView recyclerView;
    AppCompatButton send;
    String URL;
    String MasterURL = "http://192.168.42.210/mysite/Rschool/getMaster.php";
    String StudentGroupURL = "http://192.168.42.210/mysite/Rschool/getGroups.php";
    StringRequest request;
    RequestQueue queue;
    AppCompatTextView name;
    int TAG = 4;
    ArrayList<Groups> list;
    String GroupID ;
    String MasterID;
    ArrayList<Master> masters;


    public static LetterFragement newInstance(String Code) {

        Bundle args = new Bundle();

        LetterFragement fragment = new LetterFragement();
        args.putString(CODEKEY , Code);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_letter, container, false);
       recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
       send = (AppCompatButton) view.findViewById(R.id.send);
       name = (AppCompatTextView) view.findViewById(R.id.name);
       list = new ArrayList<>();
        Code = getArguments().getString(CODEKEY);
        queue = Volley.newRequestQueue(getContext());

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Code.equals("0")){

                    if (GroupID != null){

                    }else {
                        Toast.makeText(getContext(), "لطفا گروه را انتخاب کنید.", Toast.LENGTH_SHORT).show();
                    }
                }else if (Code.equals("1")){

                    if (MasterID != null){

                    }else {
                        Toast.makeText(getContext(), "لطفا استاد را انتخاب کنید.", Toast.LENGTH_SHORT).show();
                    }
                }



            }
        });

        if (Code.equals("0")){
            //Student
            URL = StudentGroupURL;
        }else if (Code.equals("1")){
            //master
            URL = MasterURL;
        }

        Load(Integer.valueOf(Code));


        return view;
    }


    private void Load(final int code){

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("LetterResponse",response);
            Parser(code , response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void Parser(int code , String response){
        TAG = code;
        if (code == 0) {
            if (response != null) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Groups groups;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        groups = new Groups();
                        JSONObject object = jsonArray.getJSONObject(i);
                        if (object.getString("GroupID").equals("0") || object.getString("GroupID").equals("1")) {

                        } else {

                            groups.setNames(object.getString("Name"));
                            groups.setColor(object.getString("Color"));
                            groups.setGroupsID(object.getString("GroupID"));


                            list.add(groups);
                        }

                    }
                    recyclerView.setVisibility(View.VISIBLE);

                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    GroupsRecyclerView adapter = new GroupsRecyclerView(false, list, getContext(), (AppCompatActivity) getActivity());
                    recyclerView.setAdapter(adapter);
                    recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            if (TAG == 0) {
                                name.setText(list.get(position).getNames());
                                GroupID = list.get(position).getGroupsID();
                            }
                        }
                    }));


                } catch (JSONException e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } else if(code == 1) {

                try {
                    masters = new ArrayList<>();
                    Master master;
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length() ; i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    master = new Master();

                    master.setID(object.getString("ID"));
                    master.setFirstName(object.getString("FirstName"));
                    master.setLastName(object.getString("LastName"));
                    master.setProfileAddress(object.getString("Photo"));
                    masters.add(master);
                }

                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                Drawable drawable = getResources().getDrawable(R.drawable.camerabg );
                MasterItemAdapter adapter = new MasterItemAdapter(masters,getContext(),getResources() ,drawable , false);
                recyclerView.setAdapter(adapter);

                recyclerView.addOnItemTouchListener(
                        new RecyclerItemClickListener(getActivity(), new   RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                if (TAG == 1){
                                    name.setText(masters.get(position).getFirstName()+" "+masters.get(position).getLastName());
                                    MasterID = masters.get(position).getID();

                                }
                            }
                        })
                );
            } catch (JSONException e) {
                e.printStackTrace();
            }

            }


        }
    }

