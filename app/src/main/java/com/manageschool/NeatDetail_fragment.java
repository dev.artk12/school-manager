package com.manageschool;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class NeatDetail_fragment extends Fragment {


    private static final String GROUPID = "GRoupKey";
    private static final String LEVELID = "LevelKey";
    private String DetailURL = "http://192.168.42.210/mysite/Rschool/getNeatDetail.php";
    private String DetailItemURL = "http://192.168.42.210/mysite/Rschool/getNeatDetailItem.php";

    String level , group;
    AppCompatTextView ClassName , GroupName , StudentCount , MasterCount , ClassRoomCount ;
    StringRequest detail , classroom;
    RequestQueue queue;
    RelativeLayout relativeLayout;
    RecyclerView recyclerView ;

    public static NeatDetail_fragment newInstance(String LevelID , String GroupID ) {

        Bundle args = new Bundle();

        NeatDetail_fragment fragment = new NeatDetail_fragment();
        args.putString(LEVELID , LevelID);
        args.putString(GROUPID , GroupID);

        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.neatdetail , container , false);
        ClassName = (AppCompatTextView) view.findViewById(R.id.LevelName);
        GroupName = (AppCompatTextView) view.findViewById(R.id.GroupName);
        StudentCount = (AppCompatTextView) view.findViewById(R.id.studentcount);
        MasterCount = (AppCompatTextView) view.findViewById(R.id.mastercount);
        ClassRoomCount = (AppCompatTextView) view.findViewById(R.id.classroomcount);
        recyclerView = (RecyclerView) view.findViewById(R.id.neats);
        queue = Volley.newRequestQueue(getContext());
        relativeLayout = (RelativeLayout) view.findViewById(R.id.parent);
        level = getArguments().getString(LEVELID);
        group = getArguments().getString(GROUPID);

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        Detail(level , group);
        ClassDetail(level,group);

        return view;
    }

    private void Detail(final String level, final String group) {

        detail = new StringRequest(Request.Method.POST, DetailURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("Fffffff",response);
                try {
                    JSONArray array = new JSONArray(response);
                    JSONObject object = array.getJSONObject(0);

                    ClassName.setText(object.getString("LevelName"));
                    GroupName.setText(object.getString("GroupName"));
                    StudentCount.setText(object.getString("StudentCount"));
                    MasterCount.setText(object.getString("mastercount"));
                    ClassRoomCount.setText(object.getString("ClassRoomcount"));

                } catch (JSONException e) {
                   Log.e("Ddddddddddd",e.getLocalizedMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            Log.e("cccccccccc",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("LevelID" , level);
                map.put("GroupID" , group);

                return map;
            }
        };


        detail.setShouldCache(false);
        detail.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(detail);

    }
    private void ClassDetail(final String Level , final String Group){

        classroom = new StringRequest(Request.Method.POST, DetailItemURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray array = new JSONArray(response);
                    ArrayList<NeatDetail> neatDetails = new ArrayList<>();
                    NeatDetail neatDetail;
                    for (int i = 0; i < array.length() ; i++) {
                        JSONObject object = array.getJSONObject(i);
                        neatDetail = new NeatDetail();
                        neatDetail.setID(object.getString("ID"));
                        neatDetail.setWeekDay(object.getString("WeekDay"));
                        neatDetail.setMaster(object.getString("mastername"));
                        neatDetail.setHour(object.getString("StartHour"));
                        neatDetail.setExamcount(object.getString("exams"));
                        neatDetail.setAvrage(object.getString("examsavrage"));

                        neatDetails.add(neatDetail);
                    }
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                    NeatDetailAdapter adapter = new NeatDetailAdapter(getContext() , neatDetails);
                    recyclerView.setAdapter(adapter);



                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("LevelID",Level);
                map.put("GroupID",Group);
                return map;
            }
        };

        classroom.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        classroom.setShouldCache(false);

        queue.add(classroom);


    }
}
