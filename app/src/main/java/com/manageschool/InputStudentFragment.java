package com.manageschool;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class InputStudentFragment extends Fragment {

    private static final String CODEKEY = "Code";
    private static final String STUDICOUNTERKEY = "StudentCounter" ;
    private static final String GROUPKEY = "GroupKey" ;

    private int STUDENTCOUNTER = 1 ;

    AppCompatEditText KodeMelli , FirstName , LastName ,FatherName , birth , Phone;
    AppCompatButton next;
    AppCompatTextView counter;
    AppCompatImageButton done , finish;
    Animation slidup , slidedown;
    RequestQueue queue;
    StringRequest request;
    int code;
    String url = "http://192.168.42.210/mysite/Rschool/InsertStudent.php";
    String groupurl = "http://192.168.42.210/mysite/Rschool/InsertStudentGroup.php";

    String GroupID;

    public static InputStudentFragment newInstance(int code , String GroupID ,int StudentCounter) {

        Bundle args = new Bundle();

        InputStudentFragment fragment = new InputStudentFragment();
        fragment.setArguments(args);
        args.putInt(STUDICOUNTERKEY , StudentCounter );
        args.putInt(CODEKEY , code);
        args.putString(GROUPKEY , GroupID);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view  = inflater.inflate(R.layout.inputstudentsfragment , container , false);

        KodeMelli = (AppCompatEditText) view.findViewById(R.id.KodeMelli);
        FirstName = (AppCompatEditText) view.findViewById(R.id.firstname);
        LastName  = (AppCompatEditText) view.findViewById(R.id.lastname);
        FatherName = (AppCompatEditText) view.findViewById(R.id.fatherName);
        birth = (AppCompatEditText) view.findViewById(R.id.birth);
        next = (AppCompatButton) view.findViewById(R.id.next);
        counter = (AppCompatTextView) view.findViewById(R.id.studentcounter);
        done = (AppCompatImageButton) view.findViewById(R.id.newstudent);
        finish = (AppCompatImageButton) view.findViewById(R.id.done);
        slidup = AnimationUtils.loadAnimation(getContext(),R.anim.slideup);
        Phone = (AppCompatEditText) view.findViewById(R.id.phone);
        slidedown = AnimationUtils.loadAnimation(getContext(),R.anim.slidedown);
        code = getArguments().getInt(CODEKEY);
        GroupID = getArguments().getString(GROUPKEY);

        counter.setText(String.valueOf(STUDENTCOUNTER));

        queue = Volley.newRequestQueue(getContext());


        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean check = checkfilds();

                if (code == 0){
                    if (check){
                        request(1);
                    }
                }else if (code == 1){
                    if (check){
                        request(1);
                    }
                }

            }
        });


        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               boolean check = checkfilds();

               if (code == 0){
                   if (check){
                       request(0);
                   }
               }else if (code == 1){
                   request(0);
                }



            }
        });


        return view;
    }

    private boolean checkfilds() {
        if (KodeMelli.getText().toString().isEmpty() || KodeMelli.getText().toString().length() != 10){
            Toast.makeText(getContext(), "کد ملی معتبر نمیباشد.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (FirstName.getText().toString().isEmpty()){
            Toast.makeText(getContext(), "نام وارد نشده است.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (LastName.getText().toString().isEmpty()){
            Toast.makeText(getContext(), "نام خانوادگی وارد نشده است.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (FatherName.getText().toString().isEmpty()){
            Toast.makeText(getContext(), "نام پدر وارد نشده است.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (birth.getText().toString().isEmpty() || birth.getText().toString().length() != 4){
            Toast.makeText(getContext(), "سال تولد معتبر نمیباشد.", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            return true;
        }
    }

    private void request(final int CODE) {
        String URL = "";
        if (code == 0){
            URL = url;
        }else if (code == 1){
            URL = groupurl;
        }

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ggggggggg",response);
                switch (CODE){
                    case 0 : Clear(); break;
                    case 1 : getActivity().onBackPressed(); break;
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getContext(), ""+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String , String> students = new HashMap<>();
                students.put("NationalID",KodeMelli.getText().toString());
                students.put("FirstName",FirstName.getText().toString());
                students.put("LastName",LastName.getText().toString());
                students.put("FatherName",FatherName.getText().toString());
                students.put("BirthYear",birth.getText().toString());
                if (Phone.getText().toString().length() != 11){
                    students.put("phone","0");
                }else {
                    students.put("phone",Phone.getText().toString());
                }
                students.put("accept","0");


                if (code == 1){
                    students.put("GroupID" , GroupID);
                }
                return students;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis((15)),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);
    }

    private void Clear() {
        KodeMelli.getText().clear();
        FirstName.getText().clear();
        LastName.getText().clear();
        FatherName.getText().clear();
        birth.getText().clear();
        counter.startAnimation(slidup);
        STUDENTCOUNTER++;
        counter.setText(String.valueOf(STUDENTCOUNTER));
        counter.startAnimation(slidedown);
    }
}
