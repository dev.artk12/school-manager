package com.manageschool;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.ViewHolder> {


    ArrayList<Question> questions;
    Context context;
    boolean b;
    AppCompatActivity activity;
    public QuestionAdapter(boolean b , Context context , ArrayList<Question> questions , AppCompatActivity activity){

        if (questions == null){
            questions = new ArrayList<>();
        }
        this.questions = questions;
        this.context = context;
        this.b = b ;
        this.activity = activity;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.questionitem , viewGroup , false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.Question.setText(questions.get(i).getQuestion());;
        viewHolder.Question.append("؟");
        viewHolder.First.setText(questions.get(i).getFirst());
        viewHolder.Secend.setText(questions.get(i).getSecend());
        viewHolder.Third.setText(questions.get(i).getThird());
        viewHolder.Four.setText(questions.get(i).getFour());
        viewHolder.Level.setText(questions.get(i).getLevel());
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (b){

                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.addToBackStack("myscreen");
                    transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    InputQuestion question = InputQuestion.newInstance(questions.get(i).getID(),true , questions.get(i).getQuestion(),questions.get(i).getFirst()
                            ,questions.get(i).getSecend(),questions.get(i).getThird() , questions.get(i).getFour() ,
                            questions.get(i).getLevel() ,questions.get(i).getLevelID() );
                    transaction.replace(android.R.id.content, question);
                    transaction.commit();

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView Question , First , Secend , Third , Four , Level;
        CardView cardView;

        public ViewHolder(View itemView){
            super(itemView);
            Question = (AppCompatTextView) itemView.findViewById(R.id.question);
            First = (AppCompatTextView) itemView.findViewById(R.id.first);
            Secend = (AppCompatTextView) itemView.findViewById(R.id.secend);
            Third = (AppCompatTextView) itemView.findViewById(R.id.third);
            Four = (AppCompatTextView) itemView.findViewById(R.id.four);
            Level = (AppCompatTextView) itemView.findViewById(R.id.level);
            cardView = (CardView) itemView.findViewById(R.id.card);
        }

    }
}
