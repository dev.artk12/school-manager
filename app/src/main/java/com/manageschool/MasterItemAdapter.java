package com.manageschool;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class MasterItemAdapter extends RecyclerView.Adapter<MasterItemAdapter.ViewHolder> {


    ArrayList<Master> masters;
    Context context;

    String url = "";
    Picasso picasso;
    LruCache<Integer,Bitmap> imagecach;
    ImageRequest imgRequest;
    RequestQueue queue;
    Resources resources;
    Drawable drawable;
    boolean check ;

    MasterItemAdapter(ArrayList<Master> masters , Context context , Resources resources , Drawable drawable , boolean check){

        if (masters == null){
            masters = new ArrayList<>();
        }
        this.masters = masters;
        this.context = context;
        url = "http://192.168.42.210/mysite/Rschool/";
        this.check = check;
        picasso = Picasso.get();
        int maxsize = (int) Runtime.getRuntime().maxMemory();
        imagecach = new LruCache<>(maxsize / 8);
        this.resources = resources;
        this.drawable = drawable;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.masteritem, viewGroup , false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {

        viewHolder.name.setText(masters.get(i).getFirstName() + " " + masters.get(i).getLastName());
        if (!masters.get(i).getProfileAddress().trim().equals("0")){
            LoadProfile(viewHolder , masters.get(i).getProfileAddress());

        }else {
            viewHolder.circleImageView.setImageDrawable(drawable);
        }

        viewHolder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check){
                    Intent intent = new Intent(context , MasterDetail.class);
                    intent.putExtra("ID",masters.get(i).getID());
                    context.startActivity(intent);
                }
            }
        });


    }

    private void LoadProfile(final ViewHolder holder , final String address){

        picasso.load(url+address)//
                .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(holder.circleImageView, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                imgRequest = new ImageRequest(url+address, new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        holder.circleImageView.setImageBitmap(response);
                    }
                }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.ARGB_8888,
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                imgRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                queue.add(imgRequest);
                            }
                        });
                queue = Volley.newRequestQueue(context);
                imgRequest.setRetryPolicy(new DefaultRetryPolicy((int)TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(imgRequest);

            }
        });



    }

    @Override
    public int getItemCount() {
        return masters.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView circleImageView;
        AppCompatTextView name;
        GridLayout parent;
        public ViewHolder(View itemView){
            super(itemView);
            circleImageView = (CircleImageView) itemView.findViewById(R.id.profile);
            name = (AppCompatTextView) itemView.findViewById(R.id.name);
            parent = (GridLayout) itemView.findViewById(R.id.parent);


        }
    }
}
