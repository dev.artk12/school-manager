package com.manageschool;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class LevelActivity extends AppCompatActivity {


    String ID , LevelName , LevelClassRoom;
    AppCompatEditText LevelNameEdt , LevelClassRoomEdt;
    SwitchCompat switchCompat;
    RecyclerView recyclerView;
    RequestQueue queue;
    StringRequest request , upload , DeleteRQ;
    AppCompatImageButton done , delete;
    Animation in , out;
    ArrayList<MasterClassRoom> classRooms;
    private String ClassRoomURL = "http://192.168.42.210/mysite/Rschool/getcurrentlevelMasterClassRoom.php";
    private String UpLoadURL = "http://192.168.42.210/mysite/Rschool/UpdateLevel.php";
    private String DeleteURL = "http://192.168.42.210/mysite/Rschool/DeletLevel.php";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        LevelNameEdt = (AppCompatEditText) findViewById(R.id.level);
        LevelClassRoomEdt = (AppCompatEditText) findViewById(R.id.levelclassroom);
        switchCompat = (SwitchCompat) findViewById(R.id.EditMode);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        done = (AppCompatImageButton) findViewById(R.id.done);
        delete = (AppCompatImageButton) findViewById(R.id.delet);
        in = AnimationUtils.loadAnimation(this , R.anim.cameragalleryin);
        out = AnimationUtils.loadAnimation(this,R.anim.cameragalleryout);


        Bundle bundle = getIntent().getExtras();
        ID = bundle.getString("ID");
        LevelName = bundle.getString("Name");
        LevelClassRoom = bundle.getString("classRoom");
        queue = Volley.newRequestQueue(this);


        if (ID != null && LevelName != null && LevelClassRoom != null){

            LevelNameEdt.setText(LevelName);
            LevelClassRoomEdt.setText(LevelClassRoom);
            LevelClassRoomEdt.setEnabled(false);
            LevelNameEdt.setEnabled(false);
            done.setVisibility(View.INVISIBLE);
            delete.setVisibility(View.INVISIBLE);
            switchCompat.setChecked(false);

            LoadClassRooms(ID);

            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (LevelClassRoomEdt.getText().toString().length() < 2){
                        Toast.makeText(LevelActivity.this, "کلاس معتبر نمیباشد.", Toast.LENGTH_SHORT).show();
                    }else if (LevelNameEdt.getText().toString().length() < 2){
                        Toast.makeText(LevelActivity.this, "اسم سطح معتبر نمیباشد.", Toast.LENGTH_SHORT).show();
                    }else {
                        Upload(ID,LevelNameEdt.getText().toString() , LevelClassRoomEdt.getText().toString());
                    }
                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Delete(ID);
                }
            });

            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked){
                        LevelClassRoomEdt.setEnabled(true);
                        LevelNameEdt.setEnabled(true);
                        done.setVisibility(View.VISIBLE);
                        delete.setVisibility(View.VISIBLE);
                        done.startAnimation(in);
                        delete.startAnimation(in);
                    }else {
                        LevelClassRoomEdt.setEnabled(false);
                        LevelNameEdt.setEnabled(false);
                        done.setVisibility(View.INVISIBLE);
                        delete.setVisibility(View.INVISIBLE);
                        done.startAnimation(out);
                        delete.startAnimation(out);
                    }

                }
            });





        }else {
            Toast.makeText(this, "Wrong", Toast.LENGTH_SHORT).show();
        }

    }
    private void LoadClassRooms(final String ID){


        recyclerView.setVisibility(View.INVISIBLE);
        recyclerView.startAnimation(out);

        if (classRooms == null){
            request = new StringRequest(Request.Method.POST, ClassRoomURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        MasterClassRoom classRoom;
                        JSONObject object;
                        classRooms = new ArrayList<>();

                        for (int i = 0; i < jsonArray.length() ; i++) {

                            object = jsonArray.getJSONObject(i);
                            classRoom = new MasterClassRoom();

                            classRoom.setID(object.getString("ID"));
                            classRoom.setLesson(object.getString("LessonID"));
                            classRoom.setMaster(object.getString("MasterID"));
                            classRoom.setStartHour(object.getString("StartHour"));
                            classRoom.setEndHour(object.getString("EndHour"));
                            classRoom.setWeekDay(object.getString("WeekDay"));
                            classRoom.setLevelName(object.getString("LevelName"));
                            classRoom.setClassRoomName(object.getString("LevelClassName"));
                            classRoom.setStartQuran(object.getString("StartQuran"));
                            classRoom.setEndQuran(object.getString("EndQuran"));


                            classRooms.add(classRoom);
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(LevelActivity.this,LinearLayoutManager.VERTICAL , false));
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        WeeklyscheduleAdapter adapter = new WeeklyscheduleAdapter(0,false,classRooms , LevelActivity.this);
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.startAnimation(in);
                        recyclerView.setAdapter(adapter);

                    } catch (JSONException e) {
                        Log.e("errrrrrrror",e.getLocalizedMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ddddddddddd",error.getLocalizedMessage());
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map = new HashMap<>();
                    map.put("LevelID",ID);
                    return map;
                }
            };

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(request);
        }else {
            recyclerView.setVisibility(View.VISIBLE);
            recyclerView.startAnimation(in);
            recyclerView.setLayoutManager(new LinearLayoutManager(LevelActivity.this,LinearLayoutManager.VERTICAL , false));
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            WeeklyscheduleAdapter adapter = new WeeklyscheduleAdapter(0,false,classRooms , LevelActivity.this);
            recyclerView.setAdapter(adapter);
        }
    }
    private void Upload(final String ID,final String name, final String ClassName) {


        upload = new StringRequest(Request.Method.POST, UpLoadURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                done.setVisibility(View.INVISIBLE);
                done.startAnimation(out);
                delete.setVisibility(View.INVISIBLE);
                delete.startAnimation(out);
                LevelNameEdt.setEnabled(false);
                LevelClassRoomEdt.setEnabled(false);
                switchCompat.setChecked(false);
                classRooms = null;
                LoadClassRooms(ID);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("sssssssss",error.getLocalizedMessage());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("ID" , ID);
                map.put("Name" , name);
                map.put("ClassRoomName" , ClassName);

                return map;
            }
        };

        upload.setShouldCache(false);
        upload.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(upload);

    }

    private void Delete(final String ID) {


        DeleteRQ = new StringRequest(Request.Method.POST, DeleteURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("sssssssss",error.getLocalizedMessage());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("ID" , ID);
                return map;
            }
        };

        DeleteRQ.setShouldCache(false);
        DeleteRQ.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(DeleteRQ);

    }

}
