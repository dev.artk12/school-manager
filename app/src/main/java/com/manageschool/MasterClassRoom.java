package com.manageschool;

public class MasterClassRoom {

    private String ID;



    private String Lesson ;
    private String Master ;
    private String WeekDay ;
    private String StartHour;
    private String EndHour;
    private String LevelName;
    private String ClassRoomName;
    private String StartQuran;
    private String EndQuran;


    public String getStartQuran() {
        return StartQuran;
    }

    public void setStartQuran(String startQuran) {
        StartQuran = startQuran;
    }

    public String getEndQuran() {
        return EndQuran;
    }

    public void setEndQuran(String endQuran) {
        EndQuran = endQuran;
    }

    public String getIDClass() {
        return IDClass;
    }

    public void setIDClass(String IDClass) {
        this.IDClass = IDClass;
    }

    private String IDClass;

    public String getLevelName() {
        return LevelName;
    }

    public void setLevelName(String levelName) {
        LevelName = levelName;
    }

    public String getClassRoomName() {
        return ClassRoomName;
    }

    public void setClassRoomName(String classRoomName) {
        ClassRoomName = classRoomName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getLesson() {
        return Lesson;
    }

    public void setLesson(String lesson) {
        Lesson = lesson;
    }

    public String getMaster() {
        return Master;
    }

    public void setMaster(String master) {
        Master = master;
    }

    public String getWeekDay() {
        return WeekDay;
    }

    public void setWeekDay(String weekDay) {
        WeekDay = weekDay;
    }

    public String getStartHour() {
        return StartHour;
    }

    public void setStartHour(String startHour) {
        StartHour = startHour;
    }

    public String getEndHour() {
        return EndHour;
    }

    public void setEndHour(String endHour) {
        EndHour = endHour;
    }
}
