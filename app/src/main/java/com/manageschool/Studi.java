package com.manageschool;

public class Studi {


    private String name;
    private String Code;
    private String ID;
    private String LevelCode;

    public String getLevelCode() {
        return LevelCode;
    }

    public void setLevelCode(String levelCode) {
        LevelCode = levelCode;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }
}
