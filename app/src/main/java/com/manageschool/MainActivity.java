package com.manageschool;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidadvance.topsnackbar.TSnackbar;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,FragmentCallBack , CameraBtn , WeeklyScheduleCallBack{



    private static final int REQUEST_OPEN_GALLETY_MASTER = 105 ;
    private static final int REQUEST_IMAGE_MASTER = 106 ;

    private static final int REQUEST_OPEN_GALLETY_PROFILE = 107 ;
    private static final int REQUEST_IMAGE_PROFILE = 108;
    private static final String PROFILE_IMAGEVIEW = "prof";
    private static final String CAMERABUTTONS = "btns";
    private static final String NAMEKEY = "NameKey" ;
    private static final String KODEKEY = "KodeKey";
    private static final String LASTNAMEKEY = "LastNameKey" ;
    private static final String EDUCATIONKEY = "EducationKey";
    private static final String STOCKKEY = "StockKey";
    private static final String EDUCCATIONNAMEKEY = "EducationName";
    private static final String STUDENTSSTOCKKEY = "StudentStock";
    private static final String MANAGELASTNAME = "MasterLastName";
    private static final String MANAGENAME = "MasterName";
    private final String BITMAP = "Bitmap";

    private String DAYSKEY = "Dayskey";
    private String TIMEKEY = "TimeKey";
    private String MASTERKEY = "MasterKey";
    private String STUDIKEY = "StudiKey";
    StringRequest request;

    private int STUDENTCOUNTER = 0;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    PagerAdapter adapter;
    NavigationView navigationView;
    Fragment fragment;
    Class fragmentclass;
    RequestQueue queue;
    StringRequest done , checkC;
    private String CheckNeatURL = "http://192.168.42.210/mysite/Rschool/getEndTerm.php";
    String [] permission = new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE , Manifest.permission.INTERNET};
    String camraPermisson = Manifest.permission.CAMERA;
    Uri uri;
    int color;
    Map<String , View> viewMap ;
    Map<String,String> result;
    AppCompatImageView circle , person  , message;
    Animation animation;
    private String CheckURL = "http://192.168.42.210/mysite/Rschool/getStudentNotAccepted";
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Output_files files;
    AppCompatTextView nameHeader;
    CircleImageView profileHeader;
    String url = "";
    Picasso picasso;
    LruCache<Integer,Bitmap> imagecach;
    ImageRequest imgRequest;
    String Address;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        circle = (AppCompatImageView) findViewById(R.id.circle);
        person = (AppCompatImageView) findViewById(R.id.newperson);
        message = (AppCompatImageView) findViewById(R.id.message);

        nameHeader = (AppCompatTextView) findViewById(R.id.title);
        profileHeader = (CircleImageView) findViewById(R.id.profile_image);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        nameHeader = (AppCompatTextView) header.findViewById(R.id.title);
        profileHeader = (CircleImageView) header.findViewById(R.id.profile_image);
        navigationView.setNavigationItemSelectedListener(this);

        animation = AnimationUtils.loadAnimation(this , R.anim.fade);
        queue = Volley.newRequestQueue(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        editor = preferences.edit();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        files = new Output_files(this);

        String name = files.read("FirstName");
        String lname = files.read("LastName");

        nameHeader.setText(name + " " +lname);
        Address = files.read("IMG");


        message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDilaog dilaog = new MessageDilaog();
                dilaog.setCancelable(true);
                dilaog.show(getSupportFragmentManager(),"Message");


            }
        });
        url = "http://192.168.42.210/mysite/Rschool/";
        picasso = Picasso.get();
        int maxsize = (int) Runtime.getRuntime().maxMemory();
        imagecach = new LruCache<>(maxsize / 8);

        if (Address.equals("0")){
            profileHeader.setImageResource(R.drawable.ic_person_24dp);
        }else {
            LoadProfile(profileHeader, Address);
        }


        if (ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED &&
                ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED &&
                ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.INTERNET) == PackageManager.PERMISSION_DENIED &&
                ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED)

        {
            ActivityCompat.requestPermissions(this , permission , 103);
        }



        getSupportActionBar().setDisplayShowTitleEnabled(false);


        person.setEnabled(false);
        circle.setVisibility(View.GONE);
        viewMap = new HashMap<>();
        result = new HashMap<>();
        CheckHaveNewStudent();

        String path = Environment.getExternalStorageDirectory().getAbsolutePath();
        File file = new File(path,"school");
        if (!file.mkdir()){
            file.mkdir();
        }
        File ImageFolder = new File(file,"Image");
        if (!ImageFolder.mkdir()){
            ImageFolder.mkdir();
        }
        person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        Check();

        person.getDrawable().setColorFilter(Color.WHITE , PorterDuff.Mode.SRC_IN);


        person.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_in);
                ShowGroupContent fragment = ShowGroupContent.newInstance("ثبت نام","0" , false);
                transaction.replace(android.R.id.content, fragment);
                transaction.commit();
            }
        });

        adapter = new AdapterPager(getSupportFragmentManager());

        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));



    }

    private void LoadProfile(final CircleImageView imageView , final String address){

        picasso.load(url+address)//
                .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(imageView , new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                imgRequest = new ImageRequest(url+address, new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        imageView.setImageBitmap(response);
                    }
                }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.ARGB_8888,
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                imgRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                queue.add(imgRequest);
                            }
                        });
                queue = Volley.newRequestQueue(MainActivity.this);
                imgRequest.setRetryPolicy(new DefaultRetryPolicy((int)TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(imgRequest);

            }
        });



    }
    private void Check(){

        checkC = new StringRequest(Request.Method.POST, CheckNeatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                response = response.trim();
                response = response.replace(".","");
                response = response.replace("-","");
                response = response.replace("/","");
                Log.e("Ffffffffff",response);
                CompareCalensars(response);



            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        checkC.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        checkC.setShouldCache(false);

        queue.add(checkC);
    }

    private void Load(){
        String getNeatURL = "http://192.168.42.210/mysite/Rschool/getNeat.php";

        final ArrayList<Neat> neats = new ArrayList<>();
        StringRequest request = new StringRequest(Request.Method.POST, getNeatURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.equals("[]")){
                   
                }if (response != null){
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        Neat neat;

                        for (int i = 0; i < jsonArray.length() ; i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            neat = new Neat();
                            neat.setGroupID(object.getString("GroupID"));
                            neat.setGroupName(object.getString("GroupName"));
                            neat.setLevelID(object.getString("LevelID"));
                            neat.setLevelName(object.getString("LevelName"));
                            neat.setStartYear(object.getString("Start"));
                            neat.setEndYear(object.getString("End"));
                            
                            
                            neats.add(neat);

                        }
                        if (neats.size() != 0){
                            ArrayList<String> GroupIDs = new ArrayList<>();
                            ArrayList<String> Groupname = new ArrayList<>();
                            ArrayList<String> Start = new ArrayList<>();
                            ArrayList<String> End = new ArrayList<>();


                            for (int i = 0; i < neats.size() ; i++) {
                                GroupIDs.add(neats.get(i).getGroupID());
                                End.add(neats.get(i).getEndYear());
                                Groupname.add(neats.get(i).getGroupName());
                                Start.add(neats.get(i).getStartYear());
                            }


                            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                            transaction.addToBackStack("myscreen");
                            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_in);
                            NeatStudent fragment = NeatStudent.newInstance(GroupIDs , Groupname ,Start , End);
                            transaction.replace(android.R.id.content, fragment );
                            transaction.commit();
                        }

                    }catch (JSONException e){
                        
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }
    
    private void CompareCalensars(String lastcalendar ) {
        lastcalendar = lastcalendar.replace("-" , "");
        lastcalendar = lastcalendar.replace("/" , "");
        lastcalendar = lastcalendar.replace("." , "");

        int start = Integer.valueOf(lastcalendar);

        SimpleDateFormat Year = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        String yearString = Year.format(new Date());

        SimpleDateFormat month = new SimpleDateFormat("MM", Locale.ENGLISH);
        String monthString = month.format(new Date());


        SimpleDateFormat day = new SimpleDateFormat("dd", Locale.ENGLISH);
        String dayString = day.format(new Date());

        PersianDate pdate = new PersianDate();
        PersianDateFormat format = new PersianDateFormat();
        format.format(pdate);

        int[] jalili = pdate.toJalali(Integer.valueOf(yearString) , Integer.valueOf(monthString) , Integer.valueOf(dayString));

        int yearint = jalili[0];
        int monthint = jalili[1];
        int dayint = jalili[2];

        String monthstr = String.valueOf(monthint);
        String daystr = String.valueOf(dayint);
        String yearstr = String.valueOf(yearint);
        if (monthstr.length() == 1){
            monthstr = "0"+monthstr;
        }if (daystr.length() == 1){
            daystr = "0"+daystr;
        }if (yearstr.length() == 2){
            yearstr = "13"+yearstr;
        }


        String index = yearstr+"/"+monthstr+"/"+daystr;
        index = index.replace("/","");

        int today = Integer.valueOf(index);
        Log.e("fffffffffffffffffff" , start +"                   "+today);

        if (start < today){
            FragmentManager fm = getSupportFragmentManager();

            InputCalendarName dialog = new InputCalendarName();
            dialog.show(fm,"MyDialog");

            fm.executePendingTransactions();
            dialog.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                        Load();
                }
            });



        }else {
            Log.e("fffffffffff","Everything IS OK");
        }

    }

    private void CheckHaveNewStudent() {

        request = new StringRequest(Request.Method.POST, CheckURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Dddddddd",response);
                if (response.trim().equals("0")){
                    person.setEnabled(false);
                    if (circle.getVisibility() == View.VISIBLE){
                        circle.startAnimation(AnimationUtils.loadAnimation(MainActivity.this,R.anim.fadeout));
                        circle.setVisibility(View.INVISIBLE);
                    }
                }else {
                    circle.setVisibility(View.VISIBLE);
                    circle.startAnimation(animation);
                    person.setEnabled(true);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            CheckHaveNewStudent();
        }
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        fragment = null;
        fragmentclass = null;

        if (id == R.id.student) {
            fragmentclass = MenuStudentFragment.class;
        }else if (id == R.id.masters) {
            fragmentclass = MenuMasterFragment.class;
        }else if (id == R.id.studi) {
            fragmentclass = MenuStudiFragemnt.class;
        }else if (id == R.id.calendar) {
            fragmentclass = MenuCalendarFragment.class;
        }else if (id == R.id.weeks){
            fragmentclass = MenuWeeklyScheduleFragment.class;
        }else if (id == R.id.level){
            fragmentclass = MenuLevelFragment.class;
        }else if (id == R.id.neat){
            fragmentclass = MenuNeat.class;
        }else if (id == R.id.question){
            fragmentclass = MenuQuestionFragment.class;
        }else if (id == R.id.profile){
            fragmentclass = MenuProfile.class;
        }else if (id == R.id.LogOut){
           finish();
           editor.putBoolean("Login",false).apply();
           startActivity(new Intent(MainActivity.this , LoginActivity.class));
        }


        try {
            if (fragmentclass != null){
                fragment = (Fragment) fragmentclass.newInstance();

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_in);
                transaction.replace(R.id.drawer_layout, fragment);
                transaction.commit();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    public void NewStudentBTN(final String GroupID, final int Code, AppCompatImageButton Add) {

        Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragmentManager;
                FragmentTransaction transaction;

                fragmentManager = getSupportFragmentManager();
                transaction = fragmentManager.beginTransaction();
                InputStudentFragment fragment = InputStudentFragment.newInstance(Code,GroupID,STUDENTCOUNTER);
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                transaction.replace(android.R.id.content,fragment, "s");
                transaction.addToBackStack("myscreen");
                transaction.commit();

                STUDENTCOUNTER++;


            }
        });

        }


    @Override
    public void Camera(final int Code , final AppCompatImageButton request , final CircleImageView profile, final LinearLayoutCompat btnparent , AppCompatImageButton camera
            , AppCompatImageButton gallery , final Fragment fragment) {

        btnparent.setVisibility(View.INVISIBLE);
        final Animation ani = AnimationUtils.loadAnimation(MainActivity.this,R.anim.cameragalleryout);

        viewMap.put(PROFILE_IMAGEVIEW,profile);
        viewMap.put(CAMERABUTTONS , btnparent);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OpenCameraButtons(btnparent, profile, ani);

            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(ContextCompat.checkSelfPermission(MainActivity.this , camraPermisson) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    CameraRequest(Code);
                }


            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MainActivity.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    if (Code == 0){
                        Ucrop.openGalletry(MainActivity.this, REQUEST_OPEN_GALLETY_MASTER);
                    }else {
                        Ucrop.openGalletry(MainActivity.this, REQUEST_OPEN_GALLETY_PROFILE);
                    }
                }

            }
        });

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String imagestring ;

                if (result.get(BITMAP) != null ){
                    if (!result.get(BITMAP).isEmpty() ){
                        imagestring = result.get(BITMAP);
                    }else {
                        imagestring = "0";
                    }
                }else{
                    imagestring = "0";
                }


                if (Code == 0){
                    String nationalid = result.get(KODEKEY);
                    String name = result.get(NAMEKEY);
                    String lastname = result.get(LASTNAMEKEY);
                    String education = result.get(EDUCATIONKEY);




                    if ( name != null || nationalid != null
                            || lastname!= null ||
                            education != null  ){

                        if (!name.isEmpty() && !nationalid.isEmpty()
                                &&!lastname.isEmpty() &&
                                !education.isEmpty()){

                            requestInputMaster(nationalid , name , lastname , education , imagestring , fragment);

                        }else {
                            TSnackbar snackbar = TSnackbar.make(findViewById(android.R.id.content), "لطفا تمامی فیلد ها را پر کنید.", TSnackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                            TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                            textView.setTextColor(Color.parseColor("#00BCD4"));
                            snackbar.show();
                        }
                    }else {
                        TSnackbar snackbar = TSnackbar.make(findViewById(android.R.id.content), "لطفا تمامی فیلد ها را پر کنید.", TSnackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                        textView.setTextColor(Color.parseColor("#00BCD4"));
                        snackbar.show();

                    }
                }else if (Code == 1){

                    String ManagName = result.get(MANAGENAME);
                    String ManagLastName = result.get(MANAGELASTNAME);

                    boolean code = false;
                    if (result.get(BITMAP) != null ){
                        if (!result.get(BITMAP).isEmpty() ){
                            imagestring = result.get(BITMAP);
                            code = true;
                        }else {
                            imagestring = Address;
                            code = false;
                        }
                    }

                    if (ManagName != null || ManagLastName != null){
                        if (ManagName.length() > 1 || ManagLastName.length() > 1){

                            String nationalID = files.read("N");
                            UpdateProfile(code , nationalID , ManagName , ManagLastName , imagestring , fragment);


                        }else {
                            TSnackbar snackbar = TSnackbar.make(findViewById(android.R.id.content), "لطفا تمامی فیلد ها را پر کنید.", TSnackbar.LENGTH_LONG);
                            View snackbarView = snackbar.getView();
                            snackbarView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                            TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                            textView.setTextColor(Color.parseColor("#00BCD4"));
                            snackbar.show();
                        }
                    }else {
                        TSnackbar snackbar = TSnackbar.make(findViewById(android.R.id.content), "لطفا تمامی فیلد ها را پر کنید.", TSnackbar.LENGTH_LONG);
                        View snackbarView = snackbar.getView();
                        snackbarView.setBackgroundColor(Color.parseColor("#FFFFFF"));
                        TextView textView = (TextView) snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
                        textView.setTextColor(Color.parseColor("#00BCD4"));
                        snackbar.show();
                    }
                }



            }
        });

    }
    public void reload() {
        Intent intent = getIntent();
        overridePendingTransition(0, 0);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        finish();
        overridePendingTransition(0, 0);
        startActivity(intent);
    }

    private String BitmapToString(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG , 100 , stream);
        byte[] bytes = stream.toByteArray();
        String temp = Base64.encodeToString(bytes , Base64.DEFAULT);

        return temp;
    }

    private void UpdateProfile(final boolean code , final String nationalid , final String name , final String lastname , final String image , final Fragment fragment){

        String url = "http://192.168.42.210/mysite/Rschool/updatemanage.php";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddyyyyHHmmss", Locale.ENGLISH);
        String timestamp = simpleDateFormat.format(new Date());
        final String imagename = timestamp + nationalid;


        done = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.trim().equals("OK")){

                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                    transaction.remove(fragment);
                    transaction.commit();

                    reload();

                    files.write("FirstName" , name);
                    files.write("LastName",lastname);
                    if (code){
                        files.write("IMG","Profiles/"+imagename);
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("fffffffffffffffffff",error.getLocalizedMessage());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("nationalID" , nationalid);
                map.put("firstname" , name);
                map.put("lastname" , lastname);
                map.put("photoname" , imagename);
                map.put("photo" , image);

                return map;
            }
        };

        done.setShouldCache(false);
        done.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(done);

    }

    private void requestInputMaster(final String nationalid , final String name , final String lastname , final String education, final String image , final Fragment fragment){

        String url = "http://192.168.42.210/mysite/Rschool/InsertMaster.php";

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddyyyyHHmmss", Locale.ENGLISH);
        String timestamp = simpleDateFormat.format(new Date());
        final String imagename = timestamp + nationalid;


        done = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                transaction.remove(fragment);
                transaction.commit();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               Log.e("fffffffffffffffffff",error.getLocalizedMessage());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("nationalID" , nationalid);
                map.put("firstname" , name);
                map.put("lastname" , lastname);
                map.put("Education" , education);
                map.put("photoname" , imagename);
                map.put("photo" , image);

                return map;
            }
        };

        done.setShouldCache(false);
        done.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(done);

    }

    @Override
    public void ContentCallBack(String Name, String nationalId, String LastName, String education) {
        result.put(NAMEKEY,Name);
        result.put(KODEKEY, nationalId);
        result.put(LASTNAMEKEY,LastName);
        result.put(EDUCATIONKEY,education);


    }

    @Override
    public void ProfilContent(String Name, String LastName) {
        result.put(MANAGENAME,Name);
        result.put(MANAGELASTNAME,LastName);


    }

    private void OpenCameraButtons(LinearLayoutCompat btnparent, CircleImageView profile, Animation ani) {
        Animation animation = AnimationUtils.loadAnimation(MainActivity.this,R.anim.cameragalleryin);
        btnparent.setAnimation(animation);
        btnparent.setVisibility(View.VISIBLE);
        profile.setAnimation(ani);
    }

    private void CloseCameraButtons(LinearLayoutCompat btnparent, CircleImageView profile) {
        Animation animation = AnimationUtils.loadAnimation(MainActivity.this,R.anim.cameragalleryout);
        btnparent.setAnimation(animation);
        btnparent.setVisibility(View.INVISIBLE);
        Animation anim = AnimationUtils.loadAnimation(MainActivity.this,R.anim.cameragalleryin);
        profile.setAnimation(anim);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 101 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

        }if (requestCode == 102 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

        }


        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Ucrop ucrop = new Ucrop(color);
        if (requestCode == REQUEST_IMAGE_MASTER && resultCode == RESULT_OK){
            ucrop.startCropActivity(MainActivity.this,uri,"profileMaster");
        }else if (requestCode == REQUEST_IMAGE_PROFILE && resultCode == RESULT_OK){
            ucrop.startCropActivity(MainActivity.this,uri,"profile");
        }else if (requestCode == REQUEST_OPEN_GALLETY_PROFILE && resultCode == RESULT_OK){
            uri = data.getData();
            ucrop.startCropActivity(MainActivity.this,uri,"profile");
        }else if (requestCode == REQUEST_OPEN_GALLETY_MASTER && resultCode == RESULT_OK){
            uri = data.getData();
            ucrop.startCropActivity(MainActivity.this,uri,"profileMaster");
        }else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            Bitmap bitmap = Ucrop.handleCropResult(MainActivity.this, data);
            result.put(BITMAP,BitmapToString(bitmap));
            CircleImageView profile = (CircleImageView) viewMap.get(PROFILE_IMAGEVIEW);
            profile.setImageBitmap(bitmap);
            CloseCameraButtons((LinearLayoutCompat) viewMap.get(CAMERABUTTONS),profile);

        }else if(requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_CANCELED){
            Toast.makeText(this, "cancel", Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void CameraRequest(int Code) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


        File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+"/School/Image/",
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        CamIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        CamIntent.putExtra("return-data",true);
        if (Code == 0){
            startActivityForResult(CamIntent, REQUEST_IMAGE_MASTER);
        }else {
            startActivityForResult(CamIntent, REQUEST_IMAGE_PROFILE);
        }


    }

    @Override
    public void HourCallBack( String time) {

        result.put(TIMEKEY,time);


        String cdays = result.get(DAYSKEY);
        String ctime = result.get(TIMEKEY);
        String cmaster = result.get(MASTERKEY);
        String cstudi = result.get(STUDIKEY);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        //DetailWeeklySchedule fragment = DetailWeeklySchedule.newInstance(cdays,ctime);
        //transaction.replace(android.R.id.content, fragment );
        transaction.commit();



    }

    @Override
    public void DayCallBack( String Day) {
        result.put(DAYSKEY,Day);


        String cdays = result.get(DAYSKEY);
        String ctime = result.get(TIMEKEY);
        String cmaster = result.get(MASTERKEY);
        String cstudi = result.get(STUDIKEY);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        //DetailWeeklySchedule fragment = DetailWeeklySchedule.newInstance(cdays,ctime);
        //transaction.replace(android.R.id.content, fragment );
        transaction.commit();



    }
    @Override
    public void MasterCallBack(String MasterName) {
        result.put(MASTERKEY,MasterName);

    }

    @Override
    public void StudiCallBack(String StudiName) {
        result.put(STUDIKEY,StudiName);
    }

    @Override
    public void StudentCallBack(ArrayList<String> names) {

    }


    public class AdapterPager extends FragmentPagerAdapter{

        public AdapterPager(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {

            switch (i){
                case 0 : return new MainStudentFragmnet();
                case 1 : return new MainMasterFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
