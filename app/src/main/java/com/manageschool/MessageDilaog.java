package com.manageschool;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;


public class MessageDilaog extends DialogFragment {

    AppCompatButton Student , Master;

    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_message_dilaog, container, false);

        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Student = (AppCompatButton) view.findViewById(R.id.Student);
        Master = (AppCompatButton) view.findViewById(R.id.Master);

        Master.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_in);
                LetterFragement fragment = LetterFragement.newInstance("1");
                transaction.replace(android.R.id.content, fragment);
                transaction.commit();
                MessageDilaog.this.dismiss();

            }
        });


        Student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_in);
                LetterFragement fragment = LetterFragement.newInstance("0");
                transaction.replace(android.R.id.content, fragment);
                transaction.commit();
                MessageDilaog.this.dismiss();
            }
        });


        return view;
    }

}
