package com.manageschool;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;


public class Master1 extends Fragment implements TextWatcher {

    private static String MID = "id";
    private static String MNAME = "Name";
    private static String MLASTNAME = "LastName";
    private static String MNATIONALID = "nationalID";
    private static String MPHOTO = "Photo";
    private static String MEDUCATION = "Education";


    String ID , name , lastname , nationalid ,education, photo ;
    AppCompatEditText Name , LastName , NationalId , Education;
    CircleImageView Profile;
    CameraBtn callBack;
    AppCompatImageButton Gallery , Camera , request;
    LinearLayoutCompat parent;
    SwitchCompat switchCompat;
    Animation in , out;
    String url = "http://192.168.42.210/mysite/Rschool/";
    ImageRequest imgRequest;
    Picasso picasso ;
    LruCache<Integer,Bitmap> imagecach;
    RequestQueue queue;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callBack = (CameraBtn) context;
    }

    public static Master1 newInstance(String ID , String Name , String LastName , String NationalID , String Education, String Photo) {

        Bundle args = new Bundle();

        Master1 fragment = new Master1();
        args.putString(MID , ID);
        args.putString(MNAME , Name);
        args.putString(MLASTNAME , LastName);
        args.putString(MNATIONALID , NationalID);
        args.putString(MPHOTO , Photo);
        args.putString(MEDUCATION,Education);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_master1, container, false);

        Name = (AppCompatEditText) view.findViewById(R.id.firstname);
        LastName = (AppCompatEditText) view.findViewById(R.id.lastname);
        NationalId = (AppCompatEditText) view.findViewById(R.id.kodmelli);
        Education = (AppCompatEditText) view.findViewById(R.id.education);
        Profile = (CircleImageView) view.findViewById(R.id.profile);
        parent = (LinearLayoutCompat) view.findViewById(R.id.btnparen);
        Gallery = (AppCompatImageButton) view.findViewById(R.id.gallery);
        Camera = (AppCompatImageButton) view.findViewById(R.id.camera);
        request = (AppCompatImageButton) view.findViewById(R.id.request);
        in = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryin);
        out = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryout);
        switchCompat = (SwitchCompat) view.findViewById(R.id.EditMode);

        switchCompat.setChecked(false);
        Name.setEnabled(false);
        LastName.setEnabled(false);
        NationalId.setEnabled(false);
        Education.setEnabled(false);
        Profile.setEnabled(false);
        request.setVisibility(View.INVISIBLE);
        picasso = Picasso.get();
        int maxsize = (int) Runtime.getRuntime().maxMemory();
        imagecach = new LruCache<>(maxsize / 8);

        queue = Volley.newRequestQueue(getContext());

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    Name.setEnabled(true);
                    LastName.setEnabled(true);
                    NationalId.setEnabled(true);
                    Education.setEnabled(true);
                    Profile.setEnabled(true);
                    request.setVisibility(View.VISIBLE);
                    request.startAnimation(in);
                }else {
                    Name.setEnabled(false);
                    LastName.setEnabled(false);
                    NationalId.setEnabled(false);
                    Education.setEnabled(false);
                    Profile.setEnabled(false);
                    request.setVisibility(View.INVISIBLE);
                    request.startAnimation(out);
                    if (parent.getVisibility() == View.VISIBLE){
                        parent.setVisibility(View.INVISIBLE);
                        parent.startAnimation(out);
                        Profile.setVisibility(View.VISIBLE);
                        Profile.startAnimation(in);
                    }

                }
            }
        });


        ID = getArguments().getString(MID);
        name = getArguments().getString(MNAME);
        lastname = getArguments().getString(MLASTNAME);
        nationalid = getArguments().getString(MNATIONALID);
        education = getArguments().getString(MEDUCATION);
        photo = getArguments().getString(MPHOTO);

        LoadProfile(photo);

        setupwidthAndHeight();

        Name.setText(name);
        LastName.setText(lastname);
        NationalId.setText(nationalid);
        Education.setText(education);

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        callBack.Camera(0,request , Profile , parent , Camera , Gallery , this);
        callBack.ContentCallBack(Name.getText().toString() ,
                NationalId.getText().toString() , LastName.getText().toString() , Education.getText().toString() );

        Name.addTextChangedListener(this);
        LastName.addTextChangedListener(this);
        NationalId.addTextChangedListener(this);
        Education.addTextChangedListener(this);


        return view;
    }
    private void setupwidthAndHeight(){

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        Profile.getLayoutParams().height = metrics.heightPixels/ 6;
        Profile.getLayoutParams().width = metrics.widthPixels/ 3;

        Name.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        LastName.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        NationalId.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);
        Education.getLayoutParams().width = (int) (metrics.widthPixels / 1.5);


    }

    @Override
    public void onResume() {
        super.onResume();

    }
    public void Lock(){
        Name.setEnabled(false);
        LastName.setEnabled(false);
        NationalId.setEnabled(false);
        Education.setEnabled(false);
        Profile.setEnabled(false);
        request.setVisibility(View.INVISIBLE);
        switchCompat.setChecked(false);
        request.startAnimation(out);
        if (parent.getVisibility() == View.VISIBLE){
            parent.setVisibility(View.INVISIBLE);
            parent.startAnimation(out);
            Profile.setVisibility(View.VISIBLE);
            Profile.startAnimation(in);
        }
    }

    private void LoadProfile(final String address){

        if (address.length() > 1 ){
            picasso.load(url+address)//
                    .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(Profile, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError(Exception e) {
                    imgRequest = new ImageRequest(url+address, new Response.Listener<Bitmap>() {
                        @Override
                        public void onResponse(Bitmap response) {
                            Profile.setImageBitmap(response);
                        }
                    }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.ARGB_8888,
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    imgRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                    queue.add(imgRequest);
                                }
                            });
                    queue = Volley.newRequestQueue(getContext());
                    imgRequest.setRetryPolicy(new DefaultRetryPolicy((int)TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    queue.add(imgRequest);

                }
            });

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Lock();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        callBack.ContentCallBack(Name.getText().toString() ,
                NationalId.getText().toString() , LastName.getText().toString() , Education.getText().toString() );
    }
}
