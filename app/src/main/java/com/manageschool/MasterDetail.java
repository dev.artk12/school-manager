package com.manageschool;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

public class MasterDetail extends AppCompatActivity implements CameraBtn{

    ViewPager viewPager;
    StringRequest request;
    RequestQueue queue;
    String LoadURL = "http://192.168.42.210/mysite/Rschool/getCurrentMaster.php";
    MasterPagerAdapter adapter;
    AppCompatTextView title;
    TabLayout tab;


    private static final int REQUEST_OPEN_GALLETY = 105 ;
    private static final int REQUEST_IMAGE = 106 ;
    private static final String PROFILE_IMAGEVIEW = "prof";
    private static final String CAMERABUTTONS = "btns";
    private static final String NAMEKEY = "NameKey" ;
    private static final String IDKEY = "IDkey" ;
    private static final String KODEKEY = "KodeKey";
    private static final String LASTNAMEKEY = "LastNameKey" ;
    private static final String EDUCATIONKEY = "EducationKey";
    private static final String STOCKKEY = "StockKey";
    private static final String EDUCCATIONNAMEKEY = "EducationName";
    private static final String STUDENTSSTOCKKEY = "StudentStock";
    private static final String OLDADDRESSKEY = "OldAddress";
    private final String BITMAP = "Bitmap";

    Uri uri;
    int color;

    Map<String , String> result;
    Map<String,View> viewMap;
    String [] permission = new String[]{Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE , Manifest.permission.READ_EXTERNAL_STORAGE , Manifest.permission.INTERNET};
    String camraPermisson = Manifest.permission.CAMERA;
    StringRequest done;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master_detail);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        title = (AppCompatTextView) findViewById(R.id.mastername);
        tab = (TabLayout) findViewById(R.id.tabs);

        String ID = getIntent().getExtras().getString("ID");
        queue = Volley.newRequestQueue(this);
        result = new HashMap<>();
        viewMap = new HashMap<>();
        tab.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab));

        if (ID != null){

            LoadStudentDetail(ID, new CallBackMaster() {
                @Override
                public void callback(Master master) {
                    if (master != null){
                        adapter = new MasterPagerAdapter(getSupportFragmentManager() , master);
                        viewPager.setAdapter(adapter);
                        title.setText("استاد"+master.getFirstName() +" "+master.getLastName());
                        result.put(IDKEY,master.getID());
                        result.put(OLDADDRESSKEY , master.getProfileAddress());
                    }
                }
            });

        }else {
            Toast.makeText(this, "Somthing Wrong", Toast.LENGTH_SHORT).show();
        }
    }

    private void LoadStudentDetail(final String ID , final CallBackMaster callBack) {

        request = new StringRequest(Request.Method.POST,LoadURL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject object = jsonArray.getJSONObject(0);
                    Master master = new Master();
                    master.setNationalID(object.getString("NationalID"));
                    master.setID(object.getString("ID"));
                    master.setLastName(object.getString("LastName"));
                    master.setEducation(object.getString("Education"));
                    master.setFirstName(object.getString("FirstName"));
                    master.setProfileAddress(object.getString("Photo"));

                    Log.e("ffffffffff",master.getFirstName());
                    callBack.callback(master);


                } catch (JSONException e) {
                    Log.e("eeeeee",e.getLocalizedMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("eeeeee",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String > map = new HashMap<>();
                map.put("ID",ID);
                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);
        queue.add(request);

    }

    public class MasterPagerAdapter extends FragmentPagerAdapter{

        String ID;
        String Name;
        String LastName;
        String NationalID;
        String Education;
        String Photo;


        public MasterPagerAdapter(FragmentManager fm , Master master) {
            super(fm);
            this.ID = master.getID();
            this.Name = master.getFirstName();
            this.LastName = master.getLastName();
            this.NationalID = master.getNationalID();
            this.Education = master.getEducation();
            this.Photo = master.getProfileAddress();
        }

        @Override
        public Fragment getItem(int i) {

            switch (i){
                case 0 : Master1 ms1 = Master1.newInstance(ID,Name,LastName,NationalID,Education,Photo); return ms1;
                case 1 : Master2 ms2 = Master2.newInstance(ID); return ms2;
                case 2 : return new Master3();
            }

            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }

    interface CallBackMaster{
        void callback(Master master);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void Camera(int code ,final AppCompatImageButton request , final CircleImageView profile, final LinearLayoutCompat btnparent , AppCompatImageButton camera
            , AppCompatImageButton gallery , final Fragment fragment) {

        btnparent.setVisibility(View.INVISIBLE);
        final Animation ani = AnimationUtils.loadAnimation(MasterDetail.this,R.anim.cameragalleryout);

        viewMap.put(PROFILE_IMAGEVIEW,profile);
        viewMap.put(CAMERABUTTONS , btnparent);

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OpenCameraButtons(btnparent, profile, ani);

            }
        });

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(ContextCompat.checkSelfPermission(MasterDetail.this , camraPermisson) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MasterDetail.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
                    CameraRequest();
                }


            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ContextCompat.checkSelfPermission(MasterDetail.this , Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(MasterDetail.this , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Ucrop.openGalletry(MasterDetail.this, REQUEST_OPEN_GALLETY);
                }

            }
        });

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String imagestring = "0";


                if (result.get(BITMAP) != null ){
                    if (!result.get(BITMAP).isEmpty() ){
                        imagestring = result.get(BITMAP);
                        Toast.makeText(MasterDetail.this, "1", Toast.LENGTH_SHORT).show();
                    }
                }else if (result.get(BITMAP) == null ){
                    BitmapDrawable drawabel = (BitmapDrawable) profile.getDrawable();
                    if(drawabel != null){
                        Bitmap bitmap = drawabel.getBitmap();
                        imagestring = BitmapToString(bitmap);
                    }else {
                        imagestring = "0";
                    }
                }else {
                    imagestring = "0";
                }





                String nationalid = result.get(KODEKEY);
                String name = result.get(NAMEKEY);
                String lastname = result.get(LASTNAMEKEY);
                String education = result.get(EDUCATIONKEY);



                if ( name != null || nationalid != null
                        || lastname!= null ||
                        education != null  ){

                    boolean check  = checkresult(name , lastname , education , nationalid);

                    if (check){
                        requestUpdateMaster(nationalid , name , lastname , education , imagestring , result.get(IDKEY) , result.get(OLDADDRESSKEY),fragment);
                    }
                }else {
                    Toast.makeText(MasterDetail.this, "لطفا تمامی فیلدها را بر کنید", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    private boolean checkresult(String Name , String LastName , String Education , String NationalID){

        if (Name.trim().length() < 3 ){
            Toast.makeText(this, "اسم باید بیش از سه کلمه باشد.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (LastName.trim().length() < 3){
            Toast.makeText(this, "نام خانوادگی باید بیش از سه کلمه باشد.", Toast.LENGTH_SHORT).show();
            return false;
        }else if (Education.trim().length() < 3 ){
            Toast.makeText(this, "مدرک تحصیلی باید بیش از سه کلمه باشد.", Toast.LENGTH_SHORT).show();
        }else if (NationalID.trim().length() != 10){
            Toast.makeText(this, "کد ملی باید 10 عدد باشد.", Toast.LENGTH_SHORT).show();
        }else {

            return true;
        }
        return false;
    }

    private String BitmapToString(Bitmap bitmap) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG , 100 , stream);
        byte[] bytes = stream.toByteArray();
        String temp = Base64.encodeToString(bytes , Base64.DEFAULT);

        return temp;
    }

    private void requestUpdateMaster(final String nationalid , final String name , final String lastname ,
                                     final String education, final String image , final String ID , final String oldaddress , final Fragment fragment){

        String url = "http://192.168.42.210/mysite/Rschool/UpdateMaster.php";

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMddyyyyHHmmss", Locale.ENGLISH);
            String timestamp = simpleDateFormat.format(new Date());
            final String imagename = timestamp + nationalid;

        done = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Master1 master1 = (Master1) fragment;
                master1.Lock();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("fffffffffffffffffff",error.getLocalizedMessage());

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("ID",ID);
                map.put("nationalID" , nationalid);
                map.put("oldadress",oldaddress);
                map.put("firstname" , name);
                map.put("lastname" , lastname);
                map.put("Education" , education);
                map.put("photoname" , imagename);
                map.put("photo" , image);

                return map;
            }
        };

        done.setShouldCache(false);
        done.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(done);

    }

    @Override
    public void ContentCallBack(String Name, String nationalId, String LastName, String education) {
        result.put(NAMEKEY,Name);
        result.put(KODEKEY, nationalId);
        result.put(LASTNAMEKEY,LastName);
        result.put(EDUCATIONKEY,education);



    }

    @Override
    public void ProfilContent(String Name, String LastName) {

    }

    private void OpenCameraButtons(LinearLayoutCompat btnparent, CircleImageView profile, Animation ani) {
        Animation animation = AnimationUtils.loadAnimation(MasterDetail.this,R.anim.cameragalleryin);
        btnparent.setAnimation(animation);
        btnparent.setVisibility(View.VISIBLE);
        profile.setAnimation(ani);
    }

    private void CloseCameraButtons(LinearLayoutCompat btnparent, CircleImageView profile) {
        Animation animation = AnimationUtils.loadAnimation(MasterDetail.this,R.anim.cameragalleryout);
        btnparent.setAnimation(animation);
        btnparent.setVisibility(View.INVISIBLE);
        Animation anim = AnimationUtils.loadAnimation(MasterDetail.this,R.anim.cameragalleryin);
        profile.setAnimation(anim);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == 101 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            CameraRequest();
        }if (requestCode == 102 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            Ucrop.openGalletry(MasterDetail.this,REQUEST_OPEN_GALLETY);
        }


        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Ucrop ucrop = new Ucrop(color);
        if (requestCode == REQUEST_IMAGE && resultCode == RESULT_OK){
            ucrop.startCropActivity(MasterDetail.this,uri,"profileMaster");
        }else if (requestCode == REQUEST_OPEN_GALLETY && resultCode == RESULT_OK){
            uri = data.getData();
            ucrop.startCropActivity(MasterDetail.this,uri,"profileMaster");
        }else if (requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_OK){
            Bitmap bitmap = Ucrop.handleCropResult(MasterDetail.this, data);
            result.put(BITMAP,BitmapToString(bitmap));
            CircleImageView profile = (CircleImageView) viewMap.get(PROFILE_IMAGEVIEW);
            profile.setImageBitmap(bitmap);
            CloseCameraButtons((LinearLayoutCompat) viewMap.get(CAMERABUTTONS),profile);

        }else if(requestCode == UCrop.REQUEST_CROP && resultCode == RESULT_CANCELED){
            Toast.makeText(this, "cancel", Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void CameraRequest() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent CamIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory().getAbsoluteFile()+"/school/Image/",
                "file"+String.valueOf(System.currentTimeMillis())+".jpg");
        uri = Uri.fromFile(file);
        CamIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
        CamIntent.putExtra("return-data",true);
        startActivityForResult(CamIntent,REQUEST_IMAGE);


    }



}
