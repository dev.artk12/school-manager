package com.manageschool;

import java.util.ArrayList;

public interface WeeklyScheduleCallBack {

    void HourCallBack( String time);
    void DayCallBack( String Day );
    void MasterCallBack(String MasterName);
    void StudiCallBack(String StudiName);
    void StudentCallBack(ArrayList<String> names);

}
