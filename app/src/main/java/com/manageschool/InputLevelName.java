package com.manageschool;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class InputLevelName extends DialogFragment implements TextWatcher {


    AppCompatEditText LevelName, ClassRoomName;
    AppCompatButton done;
    RequestQueue queue ;
    StringRequest request;

    @Override
    public void onStart() {
        super.onStart();

        Window window = getDialog().getWindow();
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.inpudlevelnamedialog , container , false);

        LevelName = (AppCompatEditText) view.findViewById(R.id.nameinput);
        ClassRoomName = (AppCompatEditText) view.findViewById(R.id.classroominput);
        done = (AppCompatButton) view.findViewById(R.id.done);

        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        ClassRoomName.addTextChangedListener(this);
        LevelName.addTextChangedListener(this);

        queue = Volley.newRequestQueue(getContext());

        return view;
    }


    private void UpLoad(final String Name , final String ClassRoomName){

        String url = "http://192.168.42.210/mysite/Rschool/InsertLevel.php";

        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("dddddddddd",response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String , String> map = new HashMap<>();
                map.put("Name" , Name);
                map.put("ClassRoomName", ClassRoomName);
                return map;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!ClassRoomName.getText().toString().isEmpty() && !LevelName.getText().toString().isEmpty() ){
                    UpLoad(LevelName.getText().toString() , ClassRoomName.getText().toString());
                    //Toast.makeText(getContext(), "تمامی فیلدها را پر است.", Toast.LENGTH_SHORT).show();
                    InputLevelName.this.dismiss();

                }else {
                    Toast.makeText(getContext(), "لطفا تمامی فیلدها را پر کنید.", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }
}
