package com.manageschool;

public class Master {


    private String ID;
    private String NationalID;
    private String FirstName;
    private String LastName;
    private String ProfileAddress;

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    private String Education;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getNationalID() {
        return NationalID;
    }

    public void setNationalID(String nationalID) {
        NationalID = nationalID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getProfileAddress() {
        return ProfileAddress;
    }

    public void setProfileAddress(String profileAddress) {
        ProfileAddress = profileAddress;
    }
}
