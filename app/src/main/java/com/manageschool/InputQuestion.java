package com.manageschool;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class InputQuestion extends Fragment {


    private static final String INSURL ="http://192.168.42.210/mysite/Rschool/InsertQuestion.php" ;
    private static final String Update ="http://192.168.42.210/mysite/Rschool/UpdateQuestion.php" ;

    RecyclerView groupsview;
    StringRequest grouprequest , request;
    RequestQueue queue;
    AppCompatTextView GroupName;
    String urlgroups = "http://192.168.42.210/mysite/Rschool/getGroups.php";
    String GroupID;
    AppCompatButton done , refresh;
    AppCompatEditText Question , First , Secend , Third , Four ;
    private static String EDITMODEKEY = "EditMode";
    private static String QUESTIONKEY = "QU";
    private static String FIRSTKEY = "FI";
    private static String SECENDKEY = "SE";
    private static String THIRDKEY = "TH";
    private static String FOURKEY = "FO";
    private static String LEVELNAMEKEY = "LevelName";
    private static String LEVELIDKEY = "LevelID";
    private static String IDKEY = "ID";


    boolean Edit;
    String question , first , secend , third , four , LevelName ,ID;
    SwitchCompat switchCompat;
    Animation in , out , Bin , Bout;



    public static InputQuestion newInstance( String ID, boolean EditMode , String Question , String First , String Secend , String Third , String Four , String LevelName , String LevelID) {

        Bundle args = new Bundle();

        InputQuestion fragment = new InputQuestion();
        args.putBoolean(EDITMODEKEY , EditMode);
        args.putString(QUESTIONKEY , Question);
        args.putString(FIRSTKEY , First);
        args.putString(SECENDKEY , Secend);
        args.putString(THIRDKEY , Third);
        args.putString(FOURKEY , Four);
        args.putString(LEVELNAMEKEY , LevelName);
        args.putString(LEVELIDKEY , LevelID);
        args.putString(IDKEY , ID);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_input_question, container, false);
        groupsview = (RecyclerView) view.findViewById(R.id.groups);
        GroupName = (AppCompatTextView) view.findViewById(R.id.group);
        done = (AppCompatButton) view.findViewById(R.id.done);
        Question = (AppCompatEditText) view.findViewById(R.id.question);
        First = (AppCompatEditText) view.findViewById(R.id.first);
        Secend = (AppCompatEditText) view.findViewById(R.id.secend);
        Third = (AppCompatEditText) view.findViewById(R.id.third);
        Four = (AppCompatEditText) view.findViewById(R.id.four);
        in = AnimationUtils.loadAnimation(getContext() , R.anim.slide_left_week);
        switchCompat = (SwitchCompat) view.findViewById(R.id.EditMode);
        out = AnimationUtils.loadAnimation(getContext() , R.anim.slide_right_week);

        Bin = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryin);
        Bout = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryout);


        queue = Volley.newRequestQueue(getContext());
        Edit = getArguments().getBoolean(EDITMODEKEY);




        if (Edit){


            switchCompat.setVisibility(View.VISIBLE);
            switchCompat.setChecked(!Edit);
            question = getArguments().getString(QUESTIONKEY);
            first = getArguments().getString(FIRSTKEY);
            secend = getArguments().getString(SECENDKEY);
            third = getArguments().getString(THIRDKEY);
            four = getArguments().getString(FOURKEY);
            GroupID = getArguments().getString(LEVELIDKEY);
            LevelName = getArguments().getString(LEVELNAMEKEY);
            GroupName.setText(LevelName);
            ID = getArguments().getString(IDKEY);

            Question.setText(question);
            First.setText(first);
            Secend.setText(secend);
            Third.setText(third);
            Four.setText(four);

            Question.setEnabled(false);
            First.setEnabled(false);
            Secend.setEnabled(false);
            Third.setEnabled(false);
            Four.setEnabled(false);
            groupsview.startAnimation(out);
            groupsview.setVisibility(View.INVISIBLE);
            done.startAnimation(Bout);
            done.setVisibility(View.INVISIBLE);

            switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                        Question.setEnabled(isChecked);
                        First.setEnabled(isChecked);
                        Secend.setEnabled(isChecked);
                        Third.setEnabled(isChecked);
                        Four.setEnabled(isChecked);
                    if (isChecked){
                        groupsview.startAnimation(in);
                        groupsview.setVisibility(View.VISIBLE);
                        done.startAnimation(Bin);
                        done.setVisibility(View.VISIBLE);
                    }else {
                        groupsview.startAnimation(out);
                        groupsview.setVisibility(View.INVISIBLE);
                        done.startAnimation(Bout);
                        done.setVisibility(View.INVISIBLE);

                    }
                }
            });

        }





        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (GroupID != null){
                    boolean check = chcek();
                    if (check){
                            InserRequest();
                    }
                }else {
                    Toast.makeText(getContext(), "گروهی انتخاب نشده است", Toast.LENGTH_SHORT).show();
                }
            }
        });

        setupgroups();


        return view;
    }
    private void setupgroups() {

        final ArrayList<Groups> names = new ArrayList<>();



        grouprequest = new StringRequest(Request.Method.POST, urlgroups , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("DFfffffffff",response);


                if (response != null){

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        Groups groups ;
                        for (int i = 0; i < jsonArray.length() ; i++) {
                            groups = new Groups();
                            JSONObject object = jsonArray.getJSONObject(i);
                            groups.setNames(object.getString("Name"));
                            groups.setColor(object.getString("Color"));
                            groups.setGroupsID(object.getString("GroupID"));
                            boolean lock ;
                            if (object.getString("Lock").equals("0")){
                                lock = false;
                            }else {
                                lock = true;
                            }if (groups.getGroupsID().equals("0") || groups.getGroupsID().equals("1")){
                                lock = true;
                            }

                            groups.setCheck(lock);
                            if (groups.getGroupsID().trim().equals("0")|| groups.getGroupsID().trim().equals("1")){

                            }else {
                                names.add(groups);
                            }

                        }

                        groupsview.setLayoutManager(new GridLayoutManager(getContext(),1));
                        groupsview.setItemAnimator(new DefaultItemAnimator());
                        GroupsRecyclerView adapter = new GroupsRecyclerView(false ,names,getContext(), (AppCompatActivity) getActivity());
                        groupsview.setAdapter(adapter);
                        groupsview.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                GroupName.setText(names.get(position).getNames());
                                GroupID = names.get(position).getGroupsID();
                            }
                        }));




                    } catch (JSONException e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }


                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ggggggggg" , error.getLocalizedMessage());
            }
        });

        grouprequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        grouprequest.setShouldCache(false);

        queue.add(grouprequest);
    }

    private void InserRequest(){

        String URL = "";
        if (Edit){
            URL = Update;
        }else {
            URL = INSURL;
        }

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("sssss",response);
                if (response.trim().equals("OK")){
                    getActivity().onBackPressed();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("Question",Question.getText().toString());
                map.put("First",First.getText().toString());
                map.put("Secend",Secend.getText().toString());
                map.put("Third",Third.getText().toString());
                map.put("Four",Four.getText().toString());
                map.put("GroupID",GroupID);
                if (Edit){
                    map.put("QuestionID",ID);
                }

                return map;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);

        queue.add(request);


    }

    private boolean chcek(){

        if (Question.length() < 10){
            Toast.makeText(getContext(), "پرسش معتبر نمیباشد", Toast.LENGTH_SHORT).show();
            return false;
        }else if (First.length() < 1){
            Toast.makeText(getContext(), "جواب اول  باید بیش از دو حرف باشد", Toast.LENGTH_SHORT).show();
            return false;
        }else if (Secend.length() < 1){
            Toast.makeText(getContext(), "جواب دوم  باید بیش از دو حرف باشد", Toast.LENGTH_SHORT).show();
            return false;
        }else if (Third.length() < 1){
            Toast.makeText(getContext(), "جواب سوم  باید بیش از دو حرف باشد", Toast.LENGTH_SHORT).show();
            return false;
        }else if (Four.length() < 1){
            Toast.makeText(getContext(), "جواب چهارم  باید بیش از دو حرف باشد", Toast.LENGTH_SHORT).show();
            return false;
        }else {
            return true;
        }

    }

}
