package com.manageschool;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class StudiDetail extends Fragment implements TextWatcher {

    AppCompatEditText EducationCode, EducationName;
    RequestQueue queue ;
    StringRequest request , levelrequest;
    RecyclerView recyclerView;
    AppCompatImageButton done;
    AppCompatTextView leveltxt;
    Animation in , out;
    String GETURL = "http://192.168.42.210/mysite/Rschool/getLevel.php";
    Map<String , String> map;
    private String IDKEY = "ID";
    private String STUDIIDKEY ="StudiID";
    private String STUDINAMEKEY ="StudiNAME";
    private String STUDICODEKEY ="StudiCODE";
    private String STUDILEVELKEY ="StudiLEVELID";

    SwitchCompat switchCompat;
    ArrayList<Level> levels;
    ArrayList<MasterClassRoom> masterClassRooms;




    public static StudiDetail newInstance(String ID , String Name ,String Code, String LevelID ) {

        Bundle args = new Bundle();

        StudiDetail fragment = new StudiDetail();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.inpudstudinamedialog , container , false);

        EducationCode = (AppCompatEditText) view.findViewById(R.id.studiCode);
        EducationName = (AppCompatEditText) view.findViewById(R.id.studiname);
        done = (AppCompatImageButton) view.findViewById(R.id.done);
        leveltxt = (AppCompatTextView) view.findViewById(R.id.leveltxt);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        in = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryin);
        out = AnimationUtils.loadAnimation(getContext() , R.anim.cameragalleryout);
        switchCompat = (SwitchCompat) view.findViewById(R.id.EditMode);
        map = new HashMap<>();

        switchCompat.setVisibility(View.VISIBLE);
        done.setVisibility(View.INVISIBLE);
        EducationCode.setEnabled(false);
        EducationName.setEnabled(false);
        leveltxt.setEnabled(false);

        EducationName.addTextChangedListener(this);
        EducationCode.addTextChangedListener(this);
        queue = Volley.newRequestQueue(getContext());

        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    done.setVisibility(View.VISIBLE);
                    done.startAnimation(in);
                    LoadLevels();
                    EducationCode.setEnabled(true);
                    EducationName.setEnabled(true);
                    leveltxt.setEnabled(true);
                }else {

                    done.setVisibility(View.INVISIBLE);
                    done.startAnimation(out);
                    //LoadLevels();
                    EducationCode.setEnabled(false);
                    EducationName.setEnabled(false);
                    leveltxt.setEnabled(false);
                }
            }
        });





        return view;
    }

    private void LoadLevels() {

        if (levels == null){
            levelrequest = new StringRequest(Request.Method.POST, GETURL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        Level level ;
                        levels = new ArrayList<>();
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            level = new Level();
                            JSONObject object = jsonArray.getJSONObject(i);
                            level.setID(object.getString("ID"));
                            level.setName(object.getString("Name"));
                            level.setClassRoomName(object.getString("ClassRoom"));

                            levels.add(level);
                        }
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        LevelAdapter adapter = new LevelAdapter(levels , getContext(),false);
                        recyclerView.setAdapter(adapter);
                        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                map.put(IDKEY,levels.get(position).getID());
                                leveltxt.setText(levels.get(position).getName());
                            }
                        }));

                    }catch (JSONException e){
                        Log.e("eeeeee",e.getLocalizedMessage());
                    }



                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });

            levelrequest.setShouldCache(false);
            levelrequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(levelrequest);
        }else {
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
            LevelAdapter adapter = new LevelAdapter(levels , getContext(), false);
            recyclerView.setAdapter(adapter);
            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    map.put(IDKEY,levels.get(position).getID());
                    leveltxt.setText(levels.get(position).getName());
                }
            }));
        }



    }
    private void LoadClassrooms(){
        
    }


    private void Download (final String Code , final String Name , final String level){

        if (Code.length() < 1){
            Toast.makeText(getContext(), "کد معتبر نمیاشد", Toast.LENGTH_SHORT).show();
        }else if (Name.length() < 1){
            Toast.makeText(getContext(), "اسم درس معتبر نمیاشد", Toast.LENGTH_SHORT).show();
        }else if (level.length() < 1){
            Toast.makeText(getContext(), "سطحی انتخاب نشده است.", Toast.LENGTH_SHORT).show();
        }else {
            String url = "http://192.168.42.210/mysite/Rschool/InsertLesson.php";

            request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("dddddddddd",response);
                    getActivity().onBackPressed();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String , String> map = new HashMap<>();
                    map.put("Code" , Code);
                    map.put("Name", Name);
                    map.put("LevelID",level);
                    return map;
                }
            };

            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            queue.add(request);

        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!EducationName.getText().toString().isEmpty() && !EducationCode.getText().toString().isEmpty() ){
                    Download(EducationCode.getText().toString() , EducationName.getText().toString() , map.get(IDKEY));


                }else {
                    Toast.makeText(getContext(), "لطفا تمامی فیلدها را پر کنید.", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }
}
