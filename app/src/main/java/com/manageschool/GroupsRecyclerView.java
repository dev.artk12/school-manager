package com.manageschool;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class GroupsRecyclerView extends RecyclerView.Adapter<GroupsRecyclerView.ViewHolder> {

    ArrayList<Groups> names;
    Context context;
    int blue , red , green , yellow;
    AppCompatActivity activity;
    boolean check;


    public GroupsRecyclerView(boolean check , ArrayList<Groups> names , Context context , AppCompatActivity activity){

        if (names == null){
            names = new ArrayList<>();
        }
        this.names = names;
        this.context = context;
        this.activity = activity;
        this.check = check;

        blue = ContextCompat.getColor(context , R.color.bluecheckbox);
        red = ContextCompat.getColor(context , R.color.redcheckbox);
        green = ContextCompat.getColor(context , R.color.greencheckbox);
        yellow = ContextCompat.getColor(context , R.color.yellowcheckbox);


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.grupitem , viewGroup , false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {

        viewHolder.name.setText(names.get(i).getNames());

        if (names.get(i).isCheck()){
            viewHolder.bot.setVisibility(View.VISIBLE);
        }else {
            viewHolder.bot.setVisibility(View.GONE);
        }
        switch (Integer.valueOf(names.get(i).getColor())){
            case 0 : viewHolder.name.setTextColor(blue); break;
            case 1 : viewHolder.name.setTextColor(green); break;
            case 2 : viewHolder.name.setTextColor(red); break;
            case 3 : viewHolder.name.setTextColor(yellow); break;
        }

        viewHolder.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check){
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.addToBackStack("myscreen");
                    transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_in);
                    ShowGroupContent fragment = ShowGroupContent.newInstance(names.get(i).getNames(),names.get(i).getGroupsID() , names.get(i).isCheck());
                    transaction.replace(android.R.id.content, fragment);
                    transaction.commit();

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return names.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView name;
        AppCompatImageView bot;

        public ViewHolder(View itemView){
            super(itemView);
            name = (AppCompatTextView) itemView.findViewById(R.id.names);
            bot = (AppCompatImageView) itemView.findViewById(R.id.bot);
        }

    }
}
