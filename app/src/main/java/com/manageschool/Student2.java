package com.manageschool;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Student2 extends Fragment {


    RecyclerView recycler1View;
    private static String IDKEY = "ID";
    String ID;
    StringRequest request;
    RequestQueue queue;
    String URL = "http://192.168.42.210/mysite/Rschool/getCurrentStudentClassRoom.php";

    public static Student2 newInstance(String ID) {

        Bundle args = new Bundle();

        Student2 fragment = new Student2();
        fragment.setArguments(args);
        args.putString(IDKEY,ID);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_student2, container, false);

        queue = Volley.newRequestQueue(getContext());
        ID = getArguments().getString(IDKEY);
        recycler1View = (RecyclerView) view.findViewById(R.id.classrooms);

        LoadClassRooms(ID, new student2volleycallback() {
            @Override
            public void callback(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    MasterClassRoom classRoom;
                    JSONObject object;
                    ArrayList<MasterClassRoom> classRooms = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length() ; i++) {

                        object = jsonArray.getJSONObject(i);
                        classRoom = new MasterClassRoom();

                        classRoom.setID(object.getString("ID"));
                        classRoom.setLesson(object.getString("LessonID"));
                        classRoom.setMaster(object.getString("MasterID"));
                        classRoom.setStartHour(object.getString("StartHour"));
                        classRoom.setEndHour(object.getString("EndHour"));
                        classRoom.setWeekDay(object.getString("WeekDay"));
                        classRoom.setLevelName(object.getString("LevelName"));
                        classRoom.setClassRoomName(object.getString("LevelClassName"));
                        classRoom.setStartQuran(object.getString("StartQuran"));
                        classRoom.setEndQuran(object.getString("EndQuran"));


                        classRooms.add(classRoom);
                    }
                    recycler1View.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                    recycler1View.setItemAnimator(new DefaultItemAnimator());
                    WeeklyscheduleAdapter adapter = new WeeklyscheduleAdapter(0,false,classRooms , getContext());
                    recycler1View.setAdapter(adapter);

                } catch (JSONException e) {
                    Log.e("errrrrrrror",e.getLocalizedMessage());
                }
            }

        });
        return view;
    }
    private void LoadClassRooms (final String ID , final student2volleycallback callback){

        request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.callback(response);
                Log.e("FFFFFF",response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ddddddddddd",error.getLocalizedMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<>();
                map.put("StudentID",ID);
                return map;
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    interface student2volleycallback{
        void callback(String response);
    }



}
