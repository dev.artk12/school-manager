package com.manageschool;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;


public class MenuProfile extends Fragment implements TextWatcher {

    AppCompatEditText Name , nationalID, Lastname ;
    AppCompatImageButton gallery , camera ;
    CircleImageView profile;
    CameraBtn callback;
    LinearLayoutCompat parent;
    AppCompatImageButton request;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Output_files files;


    String url = "";
    Picasso picasso;
    LruCache<Integer,Bitmap> imagecach;
    ImageRequest imgRequest;
    RequestQueue queue;
    Resources resources;
    Drawable drawable;
    boolean check ;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callback = (CameraBtn) context;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu_profil, container, false);

        Name = (AppCompatEditText) view.findViewById(R.id.firstname);
        Lastname = (AppCompatEditText) view.findViewById(R.id.lastname);
        nationalID = (AppCompatEditText) view.findViewById(R.id.kodmelli);
        profile = (CircleImageView) view.findViewById(R.id.profile);
        parent = (LinearLayoutCompat) view.findViewById(R.id.btnparen);
        gallery = (AppCompatImageButton) view.findViewById(R.id.gallery);
        camera = (AppCompatImageButton) view.findViewById(R.id.camera);
        request = (AppCompatImageButton) view.findViewById(R.id.request);
        files = new Output_files(getContext());

        callback.Camera(1,request,profile , parent , camera , gallery , this);
        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        editor = preferences.edit();

        url = "http://192.168.42.210/mysite/Rschool/";
        this.check = check;
        picasso = Picasso.get();
        int maxsize = (int) Runtime.getRuntime().maxMemory();
        imagecach = new LruCache<>(maxsize / 8);


        String national = files.read("N");
        String name = files.read("FirstName" );
        String lastname = files.read("LastName" );
        String Address = files.read("IMG");

        nationalID.setText(national);
        nationalID.setEnabled(false);
        Name.setText(name);
        Lastname.setText(lastname);
        Name.addTextChangedListener(this);
        Lastname.addTextChangedListener(this);
        //Log.e("Address" ,Address);
        if (Address.equals("0")){
            profile.setImageResource(R.drawable.ic_person_24dp);
        }else {
            LoadProfile(profile , Address);
        }

        callback.ProfilContent(Name.getText().toString() , Lastname.getText().toString());

        return view;
    }

    private void LoadProfile(final CircleImageView imageView , final String address){

        picasso.load(url+address)//
                .memoryPolicy(MemoryPolicy.NO_CACHE).networkPolicy(NetworkPolicy.NO_CACHE).into(imageView , new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
                imgRequest = new ImageRequest(url+address, new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        imageView.setImageBitmap(response);
                    }
                }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.ARGB_8888,
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                imgRequest.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                                queue.add(imgRequest);
                            }
                        });
                queue = Volley.newRequestQueue(getContext());
                imgRequest.setRetryPolicy(new DefaultRetryPolicy((int)TimeUnit.SECONDS.toMillis(2),DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(imgRequest);

            }
        });



    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        callback.ProfilContent(Name.getText().toString() , Lastname.getText().toString());

    }
}
