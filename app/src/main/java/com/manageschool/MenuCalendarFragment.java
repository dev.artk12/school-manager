package com.manageschool;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import saman.zamani.persiandate.PersianDate;
import saman.zamani.persiandate.PersianDateFormat;


public class MenuCalendarFragment extends Fragment {

    InputCalendarName dialog;
    RequestQueue queue;
    StringRequest request , download;
    String url = "http://192.168.42.210/mysite/Rschool/checkCalendar.php";
    RecyclerView recyclerView;
    String dlurl = "http://192.168.42.210/mysite/Rschool/getStartEnd.php";
    AppCompatImageButton refresh;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_menu_calendar, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.calendars);
        queue = Volley.newRequestQueue(getContext());
        refresh = (AppCompatImageButton) view.findViewById(R.id.refresh);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        CheckCalendar();
        //Download();

    }

    private void Download(final VolleyCallBack callBack) {

        download = new StringRequest(Request.Method.POST, dlurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.trim().equals("null")){
                    FragmentManager fm = getFragmentManager();

                    InputCalendarName dialog = new InputCalendarName();
                    dialog.show(fm,"MyDialog");

                    fm.executePendingTransactions();
                    dialog.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            onResume();
                        }
                    });
                }else {
                    callBack.CallBack(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("aaaaaaaaaaaaaaa",error.getMessage());

            }
        });

        download.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        download.setShouldCache(false);

        queue.add(download);

    }



    interface VolleyCallBack{
        void CallBack(String response);
    }

    void CheckCalendar(){

        request = new StringRequest(Request.Method.POST,url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() <= 0 ){
                        dialog = new InputCalendarName();
                        dialog.setCancelable(false);
                        dialog.show(getFragmentManager() , "a");
                    }else{
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        String calendar = jsonObject.getString("Calendar");
                        String ID = jsonObject.getString("ID");

                        CompareCalensarsCheck(calendar , response);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Errrorrrrr" , error.getMessage());
            }
        });


        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }

    private void CompareCalensarsCheck(String lastcalendar , String response) {
        lastcalendar = lastcalendar.replace("-" , "");
        lastcalendar = lastcalendar.replace("/" , "");
        lastcalendar = lastcalendar.replace("." , "");
        //lastcalendar = lastcalendar.replace("0" ,"");

        int start = Integer.valueOf(lastcalendar);

        SimpleDateFormat Year = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        String yearString = Year.format(new Date());

        SimpleDateFormat month = new SimpleDateFormat("MM", Locale.ENGLISH);
        String monthString = month.format(new Date());


        SimpleDateFormat day = new SimpleDateFormat("dd", Locale.ENGLISH);
        String dayString = day.format(new Date());

        PersianDate pdate = new PersianDate();
        PersianDateFormat format = new PersianDateFormat();
        format.format(pdate);

        int[] jalili = pdate.toJalali(Integer.valueOf(yearString) , Integer.valueOf(monthString) , Integer.valueOf(dayString));

        int yearint = jalili[0];
        int monthint = jalili[1];
        int dayint = jalili[2];

        String monthstr = String.valueOf(monthint);
        String daystr = String.valueOf(dayint);
        String yearstr = String.valueOf(yearint);
        if (monthstr.length() == 1){
            monthstr = "0"+monthstr;
        }if (daystr.length() == 1){
            daystr = "0"+daystr;
        }if (yearstr.length() == 2){
            yearstr = "13"+yearstr;
        }


        String index = yearstr+"/"+monthstr+"/"+daystr;
        index = index.replace("/","");

        int today = Integer.valueOf(index);
        Log.e("fffffffffffffffffff" , start +"                   "+today);


        if (start > today){

            Download(new VolleyCallBack() {
                @Override
                public void CallBack(String response) {
                    try {

                        JSONArray jsonArray = new JSONArray(response);
                        if (jsonArray.length() < 0 ){
                            Toast.makeText(getContext(), "null", Toast.LENGTH_SHORT).show();
                        }
                        Calendar calendar;
                        final ArrayList<Calendar> calendars = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length() ; i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            calendar = new Calendar();
                            Log.e("ddddddddddd",object.toString());
                            calendar.setEnd(object.getString("End"));
                            calendar.setStart(object.getString("Start"));
                            calendar.setID(object.getString("ID"));

                            calendars.add(calendar);
                        }

                        CalendarAdapter adapter = new CalendarAdapter(getContext(),calendars);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
                        recyclerView.setAdapter(adapter);
                        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                                transaction.addToBackStack("myscreen");
                                CalendarWeeklyScheduleFragment fragment = CalendarWeeklyScheduleFragment.newInstance(calendars.get(position).getID());
                                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                                transaction.replace(android.R.id.content, fragment);
                                transaction.commit();
                            }
                        }));

                    } catch (JSONException e) {
                        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });


        }else if (start < today){
            FragmentManager fm = getFragmentManager();

            InputCalendarName dialog = new InputCalendarName();
            dialog.show(fm,"MyDialog");

            fm.executePendingTransactions();
            dialog.getDialog().setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    onResume();
                }
            });

            Toast.makeText(getContext(), "تاریخ ترم تمام شده.", Toast.LENGTH_SHORT).show();

        }

    }


}
