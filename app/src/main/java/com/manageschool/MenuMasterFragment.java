package com.manageschool;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MenuMasterFragment extends Fragment {


    FloatingActionButton add;
    AppCompatImageButton refresh;
    RecyclerView recyclerView;
    RequestQueue queue;
    StringRequest request;
    String url = "http://192.168.42.210/mysite/Rschool/getMaster.php";




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.master_fragemnt , container , false);
        add = (FloatingActionButton) view.findViewById(R.id.floatingbtn);
        refresh = (AppCompatImageButton) view.findViewById(R.id.refresh);
        recyclerView = (RecyclerView) view.findViewById(R.id.masters);
        queue = Volley.newRequestQueue(getContext());


        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onResume();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.addToBackStack("myscreen");
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                transaction.replace(android.R.id.content, new InputMasterFragmnet());
                transaction.commit();
            }
        });



        return view;
    }


    private  void Download(final VolleyCallback callback){

        request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                callback.callback(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return super.getParams();
            }
        };

        request.setShouldCache(false);
        request.setRetryPolicy(new DefaultRetryPolicy((int) TimeUnit.SECONDS.toMillis(15) , DefaultRetryPolicy.DEFAULT_MAX_RETRIES , DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }

    public interface VolleyCallback{
        void callback(String response);
    }

    @Override
    public void onResume() {

        super.onResume();

        Download(new VolleyCallback() {
            @Override
            public void callback(String response) {
                try {
                    Master master;
                    final ArrayList<Master> masters = new ArrayList<>();
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length() ; i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        master = new Master();

                        master.setID(object.getString("ID"));
                        master.setFirstName(object.getString("FirstName"));
                        master.setLastName(object.getString("LastName"));
                        master.setProfileAddress(object.getString("Photo"));
                        masters.add(master);
                    }

                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL , false));
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    Drawable drawable = getResources().getDrawable(R.drawable.camerabg );
                    MasterItemAdapter adapter = new MasterItemAdapter(masters,getContext(),getResources() ,drawable , true);
                    recyclerView.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
