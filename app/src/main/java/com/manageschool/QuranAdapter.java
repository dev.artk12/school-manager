package com.manageschool;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class QuranAdapter extends RecyclerView.Adapter<QuranAdapter.ViewHolder>{

    ArrayList<Quran> qurans;
    Context context;

    public QuranAdapter (Context context , ArrayList<Quran> qurans ){

        if (qurans == null){
            qurans = new ArrayList<>();
        }
        this.qurans = qurans;
        this.context = context;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.quranitem , viewGroup , false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        viewHolder.Index.setText("-");
        viewHolder.Index.append(qurans.get(i).getSura());
        viewHolder.Sura.setText(qurans.get(i).getSuraName());
    }

    @Override
    public int getItemCount() {
        return qurans.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder{

        AppCompatTextView Sura , Index;

        public ViewHolder (View itemView){
            super(itemView);
            Sura = (AppCompatTextView) itemView.findViewById(R.id.suraname);
            Index = (AppCompatTextView) itemView.findViewById(R.id.index);

        }

    }
}
